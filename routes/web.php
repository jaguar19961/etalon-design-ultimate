<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('sitemap.xml', function () {
//    return view('sitemap');
//});


//front end routes-----------------------------------

use Illuminate\Support\Facades\Session;

Route::get('/', 'FrontController@first');
Route::get('intro', 'FrontController@intro')->name('intro');
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::group(['prefix' => '{w_id}', 'middleware' => 'isCity'], function () {
            Route::get('/', 'FrontController@index')->name('acasa');
            Route::get('/about', 'FrontController@about')->name('about');
            Route::get('/article', 'FrontController@article')->name('articole');
            Route::get('/blog', 'FrontController@blog')->name('blog');
            Route::get('/category/{slug}/{id}', 'FrontController@category');
            Route::get('/catalog', 'FrontController@catalog');
            Route::get('/toate-colectiile', 'FrontController@allcolections');
            Route::get('/category/serie/{slug?}/{id?}', 'FrontController@serie')->name('series');
            Route::get('/contact', 'FrontController@contact')->name('contact');
            Route::get('/offers', 'FrontController@offers')->name('oferte');
            Route::get('/product/{serie}/{product}', 'FrontController@product')->name('produs');
            Route::get('/blog/article/{slug}/{id}', 'FrontController@showBlog')->name('blogArticle');
            Route::get('/offers/article/{slug}/{id}', 'FrontController@showOffer')->name('oferteArticol');
//sisteme de deschidere
            Route::get('/sisteme-de-deschidere', 'FrontController@openSystem');
            Route::get('/sisteme-de-deschidere/afisare/{name}/{id}', 'FrontController@openSystemShow');
//        parteneri-----------
            Route::get('/parteneri', 'FrontController@partners');
            Route::get('/galerie/{name}/{id}', 'FrontController@gallery');
//        politica-------
            Route::get('/termeni&conditii', 'FrontController@terms');
            Route::get('/politica-de-confidentialitate', 'FrontController@confident');
            Route::get('/multumim', 'FrontController@multumim');
            Route::put('/multumim-pentru-mesaj', 'FrontController@getOffer1');
            Route::put('/multumim-pentru-colaborare', 'FrontController@getColaboration');
            Route::put('/multumim-pentru-abonare-la-newsletter', 'FrontController@getNewsletter');
            Route::put('/multumim-pentru-interes-fata-de-oferta', 'FrontController@getPhone');

//        filtrare dupa o categorie oarecare
//            Route::get('/colectia/usi-cu-geam', 'FilterController@usiCuGeam')->name('series');

        });
    });
Route::get('/set-city/{value}', 'FrontController@setCity')->name('setCity');
Route::post('/submit_request_door', 'FrontController@getOffer');
//api axios

Route::get('/door-filter/{serie_id?}', 'FrontController@DoorFilter');


Route::post('/api-routes/save-data', 'ApiController@SaveEdit');
Route::post('/api-routes/add-single-product-gallery/{id}/{section}', 'ApiController@saveImage');
Route::post('/api-routes/add-product-gallery/{id}/{section}', 'ApiController@saveGallery');
Route::post('/api-routes/del-img-gall', 'ApiController@delImg');


//end front end routes-----------

//admin routes------------------------------------------------------------------------
Route::group(['prefix' => 'admin'], function(){
    Auth::routes(
        ['register' => false]
    );
});
Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/home', 'AdminController@index');
});
Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'AdminController@index');
    Route::get('/product_color/{id}', 'AdminController@productColor');
    Route::get('/add_product_color/{id}', 'AdminController@addProductColor');
    Route::get('/add_product_window/{product_id}/{color_id}', 'AdminController@addProductWindow');
    Route::get('/edit_home_page', 'AdminController@EditHomePage');
    Route::get('/edit_product', 'AdminController@editProduct');
    Route::get('/edit_serie', 'AdminController@editSerie');
    Route::get('/edit_category/serie/{id}', 'AdminController@editCatSerie');
    Route::get('/edit_molding', 'AdminController@editMolding');
    Route::get('/edit_furniture', 'AdminController@editFurniture');
    Route::get('/parchet', 'AdminController@parchet');

//editare serie
    Route::put('/edit_serie/update', 'AdminController@updateSerie');
    Route::put('/edit_serie/addNewSerie', 'AdminController@addNewSerie');

//    sectiunea produse
    Route::get('/edit_product/delete/{id}', 'AdminController@editProductDelete');
    Route::put('/edit_product/editProductDescription', 'AdminController@editProductDescription');
    Route::put('/edit_product/productPrice', 'AdminController@productPrice');

    //sectiunea culori
    Route::get('/edit_color', 'AdminController@editColor');
    Route::get('/edit_color/delete/{id}', 'AdminController@editColorDelete');
    Route::put('/edit_color/colorImage', 'AdminController@addColorImage');
    Route::put('/edit_color/new', 'AdminController@addNewColor');

//    sectiunea ferestre-----------------------------------------
    Route::get('/edit_window', 'AdminController@editWindow');
    Route::put('/edit_window/new', 'AdminController@addNewWindow');
    Route::put('/edit_window/windowImage', 'AdminController@addWindowImage');

//    --------------------------------------------------------------------
//    sectiunea categorii
    Route::get('/category/{id}', 'AdminController@selectCategory');
    Route::get('/category/series/{category}/{id}', 'AdminController@selectSeries');
//------------------------------------------------------------------------
    //postare

    //stergere culoare de la un produs
    Route::get('/add_product_color/delete/{id}', 'AdminController@addProductColorDelete');

//    stergere geam produs
    Route::get('/delete-window-product/{id}', 'AdminController@addProductWindowDelete');

    //adaugare culori noi pentru produse
    Route::put('add-product-color-color', 'AdminController@addProductColorColor');

//    adaugare sticla noua pentru produse
    Route::put('add-product-window-window', 'AdminController@addProductWindowWindow');

//    adugare furnitura
    Route::put('add-product-furniture-furniture', 'AdminController@addProductFurnitureFurniture');
    Route::put('add-product-plane-plane', 'AdminController@addProductPlanePlane');
    Route::get('/furniture-delete/{id}', 'AdminController@furDel');


    Route::get('/category/sub/{id}', 'AdminController@getSub');
    Route::get('/category/color/{id}', 'AdminController@getColor');
//    baner pagina principala
    Route::put('addNewBanner', 'AdminController@addNewBanner');
    Route::put('editBanner', 'AdminController@editBanner');
    Route::get('deleteBanner/{id}', 'AdminController@deleteBanner');
//    adugare oferta
    Route::put('addNewOffer', 'AdminController@addNewOffer');
    Route::get('OfferActive', 'AdminController@activeOffer');
    Route::get('OfferInactive', 'AdminController@inctiveOffer');

    Route::put('addNewCatalog', 'AdminController@addNewCatalog');
    Route::put('updateNewCatalog', 'AdminController@updateNewCatalog');
    Route::put('addNewCatalogv2', 'AdminController@addNewCatalogv2');
    Route::get('deleteCatalog/{id}', 'AdminController@deleteCatalog');

    Route::get('editProductSelected/{id}', 'AdminController@editProductSelected');
    Route::put('addProduct', 'AdminController@addProduct');
    Route::put('addProductColorImage', 'AdminController@addProductColorImage');
    Route::put('addProductWindowImage', 'AdminController@addProductWindowImage');

    //blog-----------
    Route::get('blog', 'AdminController@blog');
    Route::get('add_blog', 'AdminController@addBlog');
    Route::put('add_new_blog', 'AdminController@addNewBlog');
    Route::get('delete_blog/{id}', 'AdminController@deleteBlog');

    Route::get('edit_blog/{id}', 'AdminController@editBlog');
    Route::put('put_edit_blog', 'AdminController@putEditBlog');
//    blog publication
    Route::get('blog-publication/{stare}/{id}', 'AdminController@blogPublication');
    Route::get('blog-toMenu/{stare}/{id}', 'AdminController@blogToMenu');


    //offer----------
    Route::get('offer', 'AdminController@offer');
    Route::get('add_offer', 'AdminController@addOffer');
    Route::put('add_new_offer', 'AdminController@addNewOffers');
    Route::put('put-edit-offer', 'AdminController@putEditOffer');
    Route::get('delete_offer/{id}', 'AdminController@deleteOffer');
    Route::put('offer-form', 'AdminController@selectForm');

//    FORMULARE
    Route::get('forms', 'AdminController@forms');
    Route::put('update-form', 'AdminController@updateForm');

//    offer publication
    Route::get('offer-publication/{stare}/{id}', 'AdminController@offerPublication');
    Route::get('edit-offer/{id}', 'AdminController@editOffer');

    //seo
    Route::get('seo', 'AdminController@seo');
    Route::put('seo/key', 'SeoController@seoKey');
    Route::put('seo/info', 'SeoController@seoInfo');
    Route::put('seo/about', 'SeoController@addSiteAbout');


//    adaugare stergere din produse populare sau noi
    Route::get('product/add-to-new/{id}', 'ProductController@productAddToNew');
    Route::get('product/delete-to-new/{id}', 'ProductController@productDeleteToNew');
    Route::get('product/add-to-popular/{id}', 'ProductController@productAddToPopular');
    Route::get('product/delete-to-popular/{id}', 'ProductController@productDeleteToPopular');

    //adugare editare descriere dupa serie de usi
//    Route::put('serie/desc', 'ProductController@seriDesc');
    Route::put('plane/plane/p', 'AdminController@editProductPlanePlane');

//    sisteme de deschidere-------------------------------------------------------------
    Route::get('/open-system', 'AdminController@openSystem');
    Route::get('/add-open-system', 'AdminController@addOpenSystem');
    Route::get('/delete-open-system/{id}', 'AdminController@deleteOpenSystem');
    Route::put('/put-open-system', 'AdminController@putOpenSystem');
    Route::put('/edit-open-system', 'AdminController@editOpenSystem');
    Route::get('/open-edit-open-system/{id}', 'AdminController@openEditOpenSystem');
//    gelerie ------------------------
    Route::get('/gallery', 'AdminController@gallery');
    Route::put('/gallery-save', 'AdminController@saveGallery');

//    editare home page-------------------------
    Route::put('/fun-1', 'AdminController@fun1');
    Route::put('/fun-2', 'AdminController@fun2');
    Route::put('/fun-3', 'AdminController@fun3');
    Route::put('/fun-4', 'AdminController@fun4');
    Route::put('/fun-5', 'AdminController@fun5');
    Route::put('/fun-6', 'AdminController@fun6');
    Route::put('/fun-7', 'AdminController@fun7');
    Route::put('/fun-8', 'AdminController@fun8');
    Route::put('/fun-9', 'AdminController@fun9');

//    editare meniu
    Route::get('/menu-editor', 'MenuEditorController@menuEditor');
    Route::put('/submenu-save', 'MenuEditorController@submenuSave');

});




