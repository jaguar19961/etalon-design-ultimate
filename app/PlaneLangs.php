<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaneLangs extends Model
{
    protected $table = 'plane_langs';

    public function category(){
        return $this->belongsTo(Planes::class, 'id');
    }
}
