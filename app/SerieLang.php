<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SerieLang extends Model
{
    protected $table = 'serie_langs';
}
