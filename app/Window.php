<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Window extends Model
{
    protected $table = 'windows';


    public function products(){
        return $this->belongsToMany('App\Products', 'product_windows', 'window_id', 'product_id');
    }
}
