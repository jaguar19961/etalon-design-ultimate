<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPopular extends Model
{
    protected $table = 'product_populars';

    public function getPro(){
        return $this->hasOne(Products::class,'id', 'product_id');
    }
}
