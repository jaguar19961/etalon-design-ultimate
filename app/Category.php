<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';


    public function lang(){
        return $this->hasOne(CategoryLang::class, 'article_id')->where('lang_id',app()->getLocale());
    }

    public function langs(){
        return $this->hasOne(CategoryLang::class, 'article_id', 'id');
    }


//    relatii pentru admin

    public function products(){
        return $this->hasMany(Products::class,'category_id');
    }

    public function dropdowns(){
        return $this->hasMany(CategoryDropdown::class,'category_id');
    }

    public function series(){
        return $this->hasOne(Serie::class,'category_id','id');
    }
}
