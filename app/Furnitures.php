<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Furnitures extends Model
{
    protected $table = 'furnitures';


    public function lang(){
        return $this->hasOne(FurnitureLangs::class, 'article_id')->where('lang_id',app()->getLocale());
    }

    public function checkexists(){
        return $this->hasMany(ProductFurniture::class, 'furniture_id', 'article_id');
    }
}
