<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteInfo extends Model
{
    protected $table = 'site_infos';

    public function lang(){
        return $this->hasOne(ArticleLang::class, 'article_id')->where('lang_id',app()->getLocale());
    }

    public function langs(){
        return $this->hasOne(ArticleLang::class, 'article_id', 'id');
    }
}
