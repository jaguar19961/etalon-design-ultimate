<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Serie extends Model
{
    protected $table = 'series';

    public function cat(){
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function lang(){
        return $this->hasOne(SerieLang::class, 'article_id')->where('lang_id',app()->getLocale());
    }

    public function langs(){
        return $this->hasOne(SerieLang::class, 'article_id', 'id');
    }

    public function furnitur(){
        return $this->hasMany(SerieFurniture::class, 'serie_id', 'id');
    }
}
