<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductLangs extends Model
{
    protected $table = 'product_langs';

    public function category(){
        return $this->belongsTo(Products::class, 'id');
    }
}
