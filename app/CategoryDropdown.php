<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryDropdown extends Model
{
    protected $table = 'category_dropdown';
}
