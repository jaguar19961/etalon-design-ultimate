<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colors extends Model
{
    protected $table = 'colors';


    public function lang(){
        return $this->hasOne(ColorLangs::class, 'article_id')->where('lang_id',app()->getLocale());
    }


    public function products(){
        return $this->belongsToMany('App\Products', 'product_colors', 'color_id', 'product_id');
    }


}
