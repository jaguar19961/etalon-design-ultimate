<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';

    public function lang(){
        return $this->hasOne(BlogLang::class, 'article_id')->where('lang_id',app()->getLocale());
    }

    public function langs(){
        return $this->hasOne(BlogLang::class, 'article_id', 'id');
    }
}
