<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Blog;
use App\BlogLang;
use App\Catalog;
use App\Category;
use App\Colors;
use App\Content;
use App\DoorType;
use App\Form;
use App\FurnitureLangs;
use App\Furnitures;
use App\Gallery;
use App\Offer;
use App\OfferLang;
use App\OneOffer;
use App\OpSystem;
use App\PlaneLangs;
use App\Planes;
use App\ProductColor;
use App\ProductFurniture;
use App\ProductLangs;
use App\ProductNew;
use App\ProductPlane;
use App\ProductPopular;
use App\Products;
use App\ProductWindow;
use App\Seo;
use App\Serie;
use App\SerieFurniture;
use App\SerieLang;
use App\SiteInfo;
use App\Style;
use App\Window;
use App\OpenSystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Image;

class AdminController extends Controller
{
    public function index()
    {

        $categories = Category::take(2)->get();
        $colors = Colors::get();
        $furniture = Furnitures::get();
        $molding = Planes::get();
        $windows = Window::get();
        $blog = Blog::take(3)->get();
        $styles = Style::get();
        $types = DoorType::get();
        return view('admin.partials.product', compact('categories', 'colors', 'furniture', 'molding', 'windows', 'blog',
            'styles', 'types'));
    }

    public function parchet()
    {
        $categories = Category::skip(2)->take(1)->get();
        $colors = Colors::get();
        $furniture = Furnitures::get();
        $molding = Planes::get();
        $windows = Window::get();
        $blog = Blog::take(3)->get();
        return view('admin.partials.parchet', compact('categories', 'colors', 'furniture', 'molding', 'windows', 'blog'));
    }

    public function editHomePage()
    {

        $catalogs = Catalog::get();
        $oneOffer = OneOffer::find(1);
        $categories = Category::get();
        $banners = Banner::get();
        $newProduct = ProductNew::get();
        $popularProduct = ProductPopular::get();
        $about = SiteInfo::find(3);
//        new variables-------------------------------

        $content = Content::get();


//        endnew variables
        return view('admin.partials.edit_home_page', compact('categories', 'banners', 'oneOffer',
            'catalogs', 'newProduct', 'popularProduct', 'about', 'content'));
    }

    public function editProduct()
    {

        $categories = Products::get();
        $category = Category::take(3)->get();
        $styles = Style::get();
        $types = DoorType::get();
        return view('admin.partials.edit_product', compact('categories', 'category', 'styles', 'types'));
    }

    public function editProductDescription(Request $request)
    {

//        dd($request->all());
//        $desc = Products::find($request->id);
        $desc = Products::where('serie_id', $request->serie_id);
        $desc->description = $request->description;
        $desc->complect_description = $request->complect_description;
        $desc->keywords = $request->keywords;
        $desc->desc = $request->desc;
        $desc->save();

        return redirect()->back();
    }

    public function editMolding()
    {

        $moldings = Planes::get();
        return view('admin.partials.edit_molding', compact('moldings'));
    }

    public function editFurniture()
    {

        $furnitures = Furnitures::get();
        return view('admin.partials.edit_furniture', compact('furnitures'));
    }

    public function editColor()
    {
        $colors = Colors::get();
        return view('admin.partials.edit_color', compact('colors'));
    }

    public function editWindow()
    {
        $windows = Window::get();
        return view('admin.partials.edit_window', compact('windows'));
    }

    public function selectCategory($id)
    {

        $categories = Products::where('category_id', $id)->get();
        $category = Category::take(3)->get();
        $getSeries = Serie::where('category_id', $id)->get();
        $styles = Style::get();
        $types = DoorType::get();
        return view('admin.partials.edit_product', compact('categories', 'category', 'getSeries',
            'styles', 'types'));
    }

    public function selectSeries($cat, $id)
    {

        $categories = Products::where('serie_id', $id)->get();
        $category = Category::get();
        $getSeries = Serie::where('category_id', $cat)->get();
        $styles = Style::get();
        $types = DoorType::get();
        return view('admin.partials.edit_product', compact('categories', 'category', 'getSeries', 'styles', 'types'));
    }

    //postare

//    adugare furnitura
    public function addProductFurnitureFurniture(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $new = new Furnitures();
        if ($request->hasFile('image')) {
            $dir = 'images/catalog/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $fileName;
        }
        $new->save();

        foreach ($request->name as $key => $lang) {

            $productNameTranslation = new FurnitureLangs();
            $productNameTranslation->article_id = $new->id;
            $productNameTranslation->name = $request['name'][$key];
            $productNameTranslation->slug = str_slug($request['name'][$key]);
            $productNameTranslation->lang_id = $key;
            $productNameTranslation->save();


        }
        return redirect()->back();
    }

//    fur del

    public function furDel($id)
    {

        $furnam = FurnitureLangs::where('article_id', $id)->delete();
        $furn = Furnitures::find($id)->delete();
        return redirect()->back();
    }
//    adugare moldinguri
//    adugare furnitura
    public function addProductPlanePlane(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $new = new Planes();
        if ($request->hasFile('image')) {
            $dir = 'images/accesories/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $new->image = $fileName;
        }
        $new->save();

        foreach ($request->name as $key => $lang) {

            $productNameTranslation = new PlaneLangs();
            $productNameTranslation->article_id = $new->id;
            $productNameTranslation->name = $request['name'][$key];
            $productNameTranslation->slug = str_slug($request['name'][$key]);
            $productNameTranslation->lang_id = $key;
            $productNameTranslation->save();


        }
        return redirect()->back();
    }

    public function addNewBanner(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);

        $banner = new Banner();
        $banner->name = $request->name;
        $banner->video_link = $request->video_link;
        if ($request->hasFile('image')) {
            $dir = 'images/scroll/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $banner->image = $fileName;
        }
        $banner->save();
        return redirect()->back();
    }

    public function editBanner(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $banner = Banner::find($request->id);
        $banner->name = $request->name;
        $banner->video_link = $request->video_link;
        if ($request->hasFile('image')) {
            $dir = 'images/scroll/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $banner->image = $fileName;
        }
        $banner->save();
        return redirect()->back();
    }

    public function deleteBanner($id)
    {

        $banner = Banner::find($id);
        $path = public_path('/images' . '/category' . '/' . $banner->image);
        File::delete($path);
        $banner->delete();
        return redirect()->back();
    }

    public function addNewOffer(Request $request)
    {

        $offer = OneOffer::find(1);
        $offer->name = $request->name;
        $offer->description = $request->content;
        $offer->valability = $request->valability;
        $offer->link = $request->link;
        $offer->save();

        return redirect()->back();
    }

    public function activeOffer()
    {
        $active = OneOffer::find(1);
        $active->activity = 1;
        $active->save();
        return redirect()->back();
    }

    public function inctiveOffer()
    {
        $active = OneOffer::find(1);
        $active->activity = 0;
        $active->save();
        return redirect()->back();
    }

    public function addNewCatalog(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $catalog = new Catalog();
        $catalog->name = $request->name;
        $catalog->link = $request->link;
        if ($request->hasFile('image')) {
            $dir = 'images/catalog-offer/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $catalog->image = $fileName;
        }
        $catalog->save();
        return redirect()->back();
    }

    public function updateNewCatalog(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $catalog = Catalog::find($request->id);
        $catalog->name = $request->name;
        $catalog->link = $request->link;

        if ($request->hasFile('image')) {
            $path = public_path('/images' . '/catalog-offer' . '/' . $catalog->image);
            File::delete($path);
            $dir = 'images/catalog-offer/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $catalog->image = $fileName;
        }
        $catalog->save();
        return redirect()->back();
    }

    public function addNewCatalogv2(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $catalog = new Catalog();
        $catalog->name = $request->name;
        $catalog->link = $request->link;
        if ($request->hasFile('image')) {
            $dir = 'images/catalog-offer/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $catalog->image = $fileName;
        }
        $catalog->save();
        return redirect()->back();
    }

    public function deleteCatalog($id)
    {
        $catalog = Catalog::find($id);
        $path = public_path('/images' . '/catalog-offer' . '/' . $catalog->image);
        File::delete($path);
        $catalog->delete();

        return redirect()->back();
    }

//    adaugare produse-----------------------------------------------------------------------------------------------

    public function addProduct(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        if ($request->category == 1 || $request->category == 2) {
            //        dd($request);
            $product = new Products();
            $product->category_id = $request->category;
            $product->serie_id = $request->serie_id;
            $product->name = $request->name;
            $product->description = $request->content1;
            $product->complect_description = $request->content2;
            $product->price = $request->price;
            $product->type_id = $request->type_id;
            $product->style_id = $request->style_id;
            $product->keywords = $request->keywords;
            $product->desc = $request->desc;


            if ($request->hasFile('image')) {
                $dir = 'images/catalog/';
                $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
                $fileName = str_random() . '.' . $extension; // rename image
//            $request->file('image')->move(public_path($dir), $fileName);
                //Resize image here
                $thumbnailpath = public_path('images/catalog/' . $fileName);
                $img = Image::make($request->file('image'), $thumbnailpath)->resize(null, 1500, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($thumbnailpath);
                $product->image = $fileName;
            }
            $product->save();
//        salvare culoare
            foreach ($request->colors as $color) {
                $culoare = new ProductColor();
                $culoare->product_id = $product->id;
                $culoare->color_id = $color;
                $culoare->save();
            }
//        salvare furniture
            foreach ($request->furnitures as $furnituress) {
                $furniture = new ProductFurniture();
                $furniture->product_id = $product->id;
                $furniture->furniture_id = $furnituress;
                $furniture->save();
            }

//        salvare moldinguri

            foreach ($request->planes as $planess) {
                $plane = new ProductPlane();
                $plane->product_id = $product->id;
                $plane->plane_id = $planess;
                $plane->save();
            }

            //salvare sticla
            foreach ($request->windows as $windowss) {
                $windows = new ProductWindow();
                $windows->product_id = $product->id;
                $windows->window_id = $windowss;
                $windows->save();
            }
            return redirect(url('/admin/product_color/' . $product->id));
        } else {
            $product = new Products();
            $product->category_id = $request->category;
            $product->serie_id = $request->serie_id;
            $product->name = $request->name;
            $product->description = $request->content1;
            $product->complect_description = $request->content2;
            $product->price = $request->price;
            $product->keywords = $request->keywords;
            $product->desc = $request->desc;

            if ($request->hasFile('image')) {
                $dir = 'images/catalog/';
                $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
                $fileName = str_random() . '.' . $extension; // rename image
//            $request->file('image')->move(public_path($dir), $fileName);
                //Resize image here
                $thumbnailpath = public_path('images/catalog/' . $fileName);
                $img = Image::make($request->file('image'), $thumbnailpath)->resize(null, 1500, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($thumbnailpath);
                $product->image = $fileName;
            }
            $product->save();
        }

        return redirect()->back();
    }

    public function productPrice(Request $request)
    {
        if ($request->cat_id == 1 || $request->cat_id == 2) {
            $product = Products::find($request->id);
            $product->style_id = $request->style_id;
            $product->type_id = $request->type_id;
            $product->price = $request->price;
            $product->save();
        } else {
            $product = Products::find($request->id);
            $product->price = $request->price;
            $product->save();
        }


        return redirect()->back();
    }

    public function editProductSelected($id)
    {
        $categories = Category::get();
        $colors = Colors::get();
        $furniture = Furnitures::get();
        $molding = Planes::get();
        $windows = Window::get();
        $select = Products::find($id);
        $styles = Style::get();
        $types = DoorType::get();
        return view('admin.partials.edit_product_selected', compact('select', 'categories', 'colors',
            'furniture', 'molding', 'windows', 'styles', 'types'));
    }


    public function editProductDelete($id)
    {

        //        stergere plane
        $productPlane = ProductPlane::where('product_id', $id)->delete();

        //stergere furnitura
        $productFurniture = ProductFurniture::where('product_id', $id)->delete();

        //    stergere sticla de la produse
        $productWindow = ProductWindow::where('product_id', $id)->delete();

//         stergere imagini culori
        $productColors = ProductColor::where('product_id', $id)->get();
        foreach ($productColors as $key => $color) {
            if (empty($color->image)) {

            } else {

                $path1 = public_path('/images' . '/catalog' . '/' . $color->image);
                File::delete($path1);

            }
            $colorData = ProductColor::find($color->id)->delete();
        }

//        stergere produs
        $product = Products::find($id);
        $path = public_path('/images' . '/catalog' . '/' . $product->image);
        File::delete($path);
        $product->delete();

        return redirect()->back();
    }

    public function productColor($id)
    {
        $idProduct = $id;
        $colors = Colors::get();
        $productColor = ProductColor::where('product_id', $id)->get();
        return view('admin.partials.product_color', compact('productColor', 'colors', 'idProduct'));
    }

    public function addProductColor($id)
    {
        $idProduct = $id;
        $colors = Colors::get();
        $productColor = ProductColor::where('product_id', $id)->get();
        return view('admin.partials.product_color', compact('productColor', 'colors', 'idProduct'));
    }

    public function addProductWindow($product_id, $color_id)
    {
        $idColor = $color_id;
        $idProduct = $product_id;
        $product = Products::find($product_id);
        $color = Colors::find($color_id);
        $windows = Window::get();
        $getImageColorDoor = ProductColor::find($color_id);
        $productWindows = ProductWindow::where('product_id', $product_id)->where('color_id', $color_id)->get();
        return view('admin.partials.product_window', compact('windows', 'productWindows', 'idProduct',
            'idColor', 'product', 'color', 'getImageColorDoor'));
    }


    public function getSub($category_id)
    {
        $sub = Serie::where('category_id', $category_id)->get();
        $list = '<option selected disabled>Alege model</option>';
        foreach ($sub as $subs) {
            $list .= "<option value='" . $subs->id . "'>" . $subs->name . "</option>";
        }
        return $list;
    }

//adugare imagine pentru culoare

    public function addProductColorImage(Request $request)
    {

        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $new = ProductColor::find($request->id);
        $path = public_path('/images' . '/catalog' . '/' . $new->image);
        File::delete($path);
        if ($request->hasFile('image')) {
            $dir = 'images/catalog/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
//            $request->file('image')->move(public_path($dir), $fileName);
            //Resize image here
            $thumbnailpath = public_path('images/catalog/' . $fileName);
            $img = Image::make($request->file('image'), $thumbnailpath)->resize(null, 1500, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $new->image = $fileName;
        }
        $new->save();


        return redirect()->back();
    }

//    adaugare imagine pentru produs sticla
    public function addProductWindowImage(Request $request)
    {

        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $new = ProductWindow::find($request->id);
        $path = public_path('/images' . '/catalog' . '/' . $new->image);
        File::delete($path);
        if ($request->hasFile('image')) {
            $dir = 'images/catalog/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
//            $request->file('image')->move(public_path($dir), $fileName);
            //Resize image here
            $thumbnailpath = public_path('images/catalog/' . $fileName);
            $img = Image::make($request->file('image'), $thumbnailpath)->resize(null, 1500, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $new->image = $fileName;
        }
        $new->save();


        return redirect()->back();
    }


    //stergere culori si imagine de la produse
    public function addProductColorDelete($id)
    {

        $color = ProductColor::find($id);
        $path = public_path('/images' . '/catalog' . '/' . $color->image);
        File::delete($path);
        $color->delete();
        return redirect()->back();
    }

    //stergere ferestre si imagine de la produse
    public function addProductWindowDelete($id)
    {

        $window = ProductWindow::find($id);

        $path = public_path('/images' . '/catalog' . '/' . $window->image);
        File::delete($path);
        $window->delete();
        return redirect()->back();
    }

    //adugare culoare si imagine pentru culori

    public function addColorImage(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);

        $new = Colors::find($request->id);
        $path = public_path('/images' . '/catalog' . '/' . $new->image);
        File::delete($path);
        if ($request->hasFile('image')) {
            $dir = 'images/catalog/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
//            $request->file('image')->move(public_path($dir), $fileName);
            //Resize image here
            $thumbnailpath = public_path('images/catalog/' . $fileName);
            $img = Image::make($request->file('image'), $thumbnailpath)->resize(335, 800, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $new->image = $fileName;
        }
        $new->code = $request->name;
        $new->save();


        return redirect()->back();
    }


    public function addWindowImage(Request $request)
    {

        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $new = Window::find($request->id);
        $path = public_path('/images' . '/catalog' . '/' . $new->image);
        File::delete($path);
        if ($request->hasFile('image')) {
            $dir = 'images/catalog/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
//            $request->file('image')->move(public_path($dir), $fileName);
            //Resize image here
            $thumbnailpath = public_path('images/catalog/' . $fileName);
            $img = Image::make($request->file('image'), $thumbnailpath)->resize(335, 800, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $new->image = $fileName;
        }
        $new->save();


        return redirect()->back();
    }

//    adaugare culoare noua

    public function addNewColor(Request $request)
    {

        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $new = new Colors();

        if ($request->hasFile('image')) {
            $dir = 'images/catalog/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
//            $request->file('image')->move(public_path($dir), $fileName);
            //Resize image here
            $thumbnailpath = public_path('images/catalog/' . $fileName);
            $img = Image::make($request->file('image'), $thumbnailpath)->resize(335, 800, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $new->image = $fileName;
        }
        $new->name = $request->name;
        $new->code = $request->code;
        $new->save();


        return redirect()->back();
    }

//    adugare sticla noua
    public function addNewWindow(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);

        $new = new Window();

        if ($request->hasFile('image')) {
            $dir = 'images/catalog/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
//            $request->file('image')->move(public_path($dir), $fileName);
            //Resize image here
            $thumbnailpath = public_path('images/catalog/' . $fileName);
            $img = Image::make($request->file('image'), $thumbnailpath)->resize(335, 800, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $new->image = $fileName;
        }
        $new->name = $request->name;
        $new->save();


        return redirect()->back();
    }

    public function editColorDelete($id)
    {

        $color = Colors::find($id);
        $path = public_path('/images' . '/catalog' . '/' . $color->image);
        File::delete($path);
        $color->delete();
        return redirect()->back();
    }

//    adugare culori noi pentru produse
    public function addProductColorColor(Request $request)
    {

        foreach ($request->colors as $color) {
            $culoare = new ProductColor();
            $culoare->product_id = $request->id;
            $culoare->color_id = $color;
            $culoare->save();
        }


        return redirect()->back();
    }

//    adaugare sticla noua pentru produs
    public function addProductWindowWindow(Request $request)
    {

        foreach ($request->window_id as $window) {
            $sticla = new ProductWindow();
            $sticla->product_id = $request->id;
            $sticla->window_id = $window;
            $sticla->color_id = $request->color_id;
            $sticla->save();
        }


        return redirect()->back();
    }

//sectiunea blog---------------------------------------------------------------

    public function blog()
    {

        $blogs = Blog::get();
        return view('admin.partials.blog', compact('blogs'));
    }

    public function addBlog()
    {

        return view('admin.partials.add_blog');
    }

    public function addNewBlog(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $blog = new Blog();
        if ($request->hasFile('image')) {
            $dir = 'images/article/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $blog->image = $fileName;
        }

        $blog->save();
        foreach ($request->name as $key => $lang) {

            $productNameTranslation = new BlogLang();
            $productNameTranslation->article_id = $blog->id;
            $productNameTranslation->name = $request['name'][$key];
            $productNameTranslation->slug = str_slug($request['name'][$key]);
            $productNameTranslation->description = $request['content'][$key];
            $productNameTranslation->keywords = $request['keywords'][$key];
            $productNameTranslation->descr = $request['descr'][$key];
            $productNameTranslation->lang_id = $key;
            $productNameTranslation->save();


        }


        return redirect()->back();
    }

    public function putEditBlog(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $blog = Blog::find($request->id);
        if ($request->hasFile('image')) {
            $dir = 'images/article/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $blog->image = $fileName;
        }
        $blog->position = $request->position;
        $blog->save();
        foreach ($request->name as $key => $lang) {

            $productNameTranslation = BlogLang::where('article_id', $request->id)->where('lang_id', $key)->first();
            $productNameTranslation->article_id = $blog->id;
            $productNameTranslation->name = $request['name'][$key];
            $productNameTranslation->slug = str_slug($request['name'][$key]);
            $productNameTranslation->description = $request['content'][$key];
            $productNameTranslation->keywords = $request['keywords'][$key];
            $productNameTranslation->descr = $request['descr'][$key];
            $productNameTranslation->lang_id = $key;
            $productNameTranslation->save();


        }


        return redirect()->back();
    }

    public function deleteBlog($id)
    {

        $blog = Blog::find($id);
        $path = public_path('/images' . '/article' . '/' . $blog->image);
        File::delete($path);
        $blogTranslate = BlogLang::where('article_id', $id)->delete();
        $blog->delete();

        return redirect()->back();
    }

    public function editBlog($id)
    {

        $blog = Blog::find($id);
        return view('admin/partials/edit_blog', compact('blog'));
    }

    public function blogPublication($stare, $id)
    {
//        dd($stare);
        $blog = Blog::find($id);
        $blog->publicata = $stare;
        $blog->save();

        return redirect()->back();
    }

    public function blogToMenu($stare, $id)
    {
//        dd($stare);
        $blog = Blog::find($id);
        $blog->to_menu = $stare;
        $blog->save();

        return redirect()->back();
    }


//sectiunea oferte-------------------------------------------------------------

    public function selectForm(Request $request)
    {
        $offer = Offer::find($request->offer_id);
        $offer->form_id = $request->form;
        $offer->save();
        return redirect()->back();
    }

    public function updateForm(Request $request)
    {
        $form = Form::find($request->form_id);
        $form->name = $request->name;
        $form->save();
        return redirect()->back();
    }

    public function forms()
    {
        $forms = Form::get();
        return view('admin.partials.forms', compact('forms'));
    }

    public function offer()
    {
        $offers = Offer::get();
        $forms = Form::get();
        return view('admin.partials.offer', compact('offers', 'forms'));
    }

    public function addOffer()
    {

        return view('admin.partials.add_offer');
    }

    public function addNewOffers(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $blog = new Offer();
        if ($request->hasFile('image')) {
            $dir = 'images/article/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $blog->image = $fileName;
        }

        if ($request->hasFile('image_offer')) {
            $dir = 'images/article/';
            $extension = strtolower($request->file('image_offer')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image_offer')->move(public_path($dir), $fileName);
            $blog->image_offer = $fileName;
        }

        $blog->save();
        foreach ($request->name as $key => $lang) {

            $productNameTranslation = new OfferLang();
            $productNameTranslation->article_id = $blog->id;
            $productNameTranslation->name = $request['name'][$key];
            $productNameTranslation->slug = str_slug($request['name'][$key]);
            $productNameTranslation->description = $request['content'][$key];
            $productNameTranslation->keywords = $request['keywords'][$key];
            $productNameTranslation->descr = $request['descr'][$key];
            $productNameTranslation->lang_id = $key;
            $productNameTranslation->save();
        }
        return redirect()->back();
    }

    public function deleteOffer($id)
    {
        $offerTranslate = OfferLang::where('article_id', $id)->delete();
        $offer = Offer::find($id);
        $path = public_path('/images' . '/article' . '/' . $offer->image);
        File::delete($path);
        $offer->delete();
        return redirect()->back();
    }

    public function offerPublication($stare, $id)
    {

        $blog = Offer::find($id);
        $blog->publicata = $stare;
        $blog->save();

        return redirect()->back();
    }

    public function editOffer($id)
    {

        $offer = Offer::find($id);
        return view('admin/partials/edit_offer', compact('offer'));
    }

    public function putEditOffer(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $offer = Offer::find($request->id);
        if ($request->hasFile('image')) {
            $dir = 'images/article/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $offer->image = $fileName;
            $offer->save();
        }

        if ($request->hasFile('image_offer')) {
            $dir = 'images/article/';
            $extension = strtolower($request->file('image_offer')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image_offer')->move(public_path($dir), $fileName);
            $offer->image_offer = $fileName;
            $offer->save();
        }


        $offerLang = OfferLang::where('article_id', $request->id)->first();
        $offerLang->name = $request->name;
        $offerLang->slug = str_slug($request->name);
        $offerLang->description = $request->content;
        $offerLang->keywords = $request->keywords;
        $offerLang->descr = $request->descr;
        $offerLang->save();

        return redirect()->back();
    }

    //seo--------------------------
    public function seo()
    {
        $seos = Seo::get();
        $info = SiteInfo::find(1);
        return view('admin.partials.seo', compact('seos', 'info'));
    }

    public function editSerie()
    {
        $moldings = Planes::get();
        $category = Category::take(3)->get();
        $categories = Serie::get();
        $furni = SerieFurniture::get();
        return view('admin.partials.edit_serie', compact('category', 'categories', 'moldings', 'furni'));
    }

    public function editCatSerie($id)
    {
        $furni = SerieFurniture::get();
        $moldings = Planes::get();
        $category = Category::get();
        $categories = Serie::where('category_id', $id)->get();
        return view('admin.partials.edit_serie', compact('category', 'categories', 'moldings', 'furni'));
    }


    public function updateSerie(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $serie = Serie::find($request->id);
        $path = public_path('/images' . '/usi-de-interior' . '/' . $serie->image);
        File::delete($path);
        if ($request->hasFile('image')) {
            $dir = 'images/usi-de-interior/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $serie->images = $fileName;
        }
        $serie->name = $request->name;
        $serie->save();

        foreach ($request->content as $key => $lang) {
            if (SerieLang::where('article_id', $request->id)->where('lang_id', $key)->exists()) {
                $productNameTranslation = SerieLang::where('article_id', $request->id)->where('lang_id', $key)->first();
                $productNameTranslation->article_id = $request->id;
                $productNameTranslation->name = $request['content'][$key];
                $productNameTranslation->slug = $request['content1'][$key];
                $productNameTranslation->lang_id = $key;
                $productNameTranslation->save();
            } else {
                $productNameTranslation = new SerieLang();
                $productNameTranslation->article_id = $request->id;
                $productNameTranslation->name = $request['content'][$key];
                $productNameTranslation->slug = $request['content1'][$key];
                $productNameTranslation->lang_id = $key;
                $productNameTranslation->save();
            }
        }
        if (!empty($request->input('furnitures'))) {
            $cus = SerieFurniture::where('serie_id', $request->id)->delete();
            foreach ($request->furnitures as $item) {
                if (SerieFurniture::where('serie_id', $request->id)->where('furniture_id', $item)->exists()) {

                } else {
                    $fur = new SerieFurniture();
                    $fur->serie_id = $request->id;
                    $fur->furniture_id = $item;
                    $fur->save();
                }

            }
        } else {
            $cus = SerieFurniture::where('serie_id', $request->id)->delete();

        }
        return redirect()->back();
    }

    public function addNewSerie(Request $request)
    {
        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $serie = new Serie();

        if ($request->hasFile('image')) {
            $dir = 'images/usi-de-interior/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $serie->images = $fileName;
        }
        $serie->category_id = $request->category;
        $serie->name = $request->name;
        $serie->save();


        return redirect()->back();
    }

    public function editProductPlanePlane(Request $request)
    {

        $request->validate([
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $serie = Planes::find($request->id);

        if ($request->hasFile('image')) {
            $path = public_path('/images' . '/accesories' . '/' . $serie->image);
            File::delete($path);
            $dir = 'images/accesories/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
//            $request->file('image')->move(public_path($dir), $fileName);
            //Resize image here
            $thumbnailpath = public_path('images/accesories/' . $fileName);
            $img = Image::make($request->file('image'), $thumbnailpath)->resize(261, 175, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $serie->image = $fileName;
        }
        $serie->save();

        foreach ($request->name as $key => $lang) {
            if (PlaneLangs::where('article_id', $request->id)->where('lang_id', $key)->exists()) {
                $productNameTranslation = PlaneLangs::where('lang_id', $key)->where('article_id', $request->id)->first();
                $productNameTranslation->article_id = $request->id;
                $productNameTranslation->name = $request['name'][$key];
                $productNameTranslation->slug = 'me';
                $productNameTranslation->lang_id = $key;
                $productNameTranslation->save();
            } else {
                $productNameTranslation = new PlaneLangs();
                $productNameTranslation->article_id = $request->id;
                $productNameTranslation->name = $request['name'][$key];
                $productNameTranslation->slug = 'me';
                $productNameTranslation->lang_id = $key;
                $productNameTranslation->save();
            }


        }
        return redirect()->back();
    }

//    sisteme de deschidere--------------------------------------------------------------
    public function openSystem()
    {
        $systems = OpSystem::get();
        return view('admin/partials/open_system', compact('systems'));
    }

    public function addOpenSystem()
    {

        return view('admin/partials/add_open_system');
    }

    public function openEditOpenSystem($id)
    {

        $openSystem = OpSystem::find($id);
        return view('admin/partials/edit_open_system', compact('openSystem'));
    }

    public function putOpenSystem(Request $request)
    {
//        dd($request->created_at);
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'content' => 'required|string',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
        ]);
        $system = new OpSystem();
        $system->name = $request->name;
        $system->slug = str_slug($request['name']);
        $system->description = $request->description;
        $system->content = $request->content;
        if ($request->hasFile('image')) {
            $dir = 'images/systems/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $system->image = $fileName;
        }
        $system->save();

        return redirect()->back();
    }

    public function editOpenSystem(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'content' => 'required|string',
            'image' => 'mimes:jpeg,jpg,png,gif',
        ]);

        $system = OpSystem::find($request->id);
        $system->name = $request->name;
        $system->slug = str_slug($request['name']);
        $system->description = $request->description;
        $system->content = $request->content;
        if ($request->hasFile('image')) {
            $dir = 'images/systems/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $system->image = $fileName;
        }
        $system->save();

        return redirect()->back();
    }

    public function deleteOpenSystem($id)
    {
        $system = OpSystem::find($id)->delete();

        return redirect()->back();
    }

    public function gallery()
    {

        $cont = Gallery::get();
        return view('admin/partials/gallery', compact('cont'));
    }

    public function saveGallery(Request $request)
    {
        $gallery = Gallery::find($request->id);
        $gallery->content = $request->content;
        $gallery->save();

        return redirect()->back();
    }


//    editare home page-----------------------------------------------------

    public function fun1(Request $request)
    {
        $request->validate([
            'txt1' => 'mimes:jpeg,jpg,png,gif',
            'txt4' => 'mimes:jpeg,jpg,png,gif',
            'txt7' => 'mimes:jpeg,jpg,png,gif',
            'txt10' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $cont = Content::find(1);
        if ($request->hasFile('txt1')) {
            $dir = 'images/homePage/';
            $extension = strtolower($request->file('txt1')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('txt1')->move(public_path($dir), $fileName);
            $cont->txt1 = $fileName;
        }

        $cont->txt2 = $request->txt2;
        $cont->txt3 = $request->txt3;
        if ($request->hasFile('txt4')) {
            $dir = 'images/homePage/';
            $extension = strtolower($request->file('txt4')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('txt4')->move(public_path($dir), $fileName);
            $cont->txt4 = $fileName;
        }

        $cont->txt5 = $request->txt5;
        $cont->txt6 = $request->txt6;
        if ($request->hasFile('txt7')) {
            $dir = 'images/homePage/';
            $extension = strtolower($request->file('txt7')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('txt7')->move(public_path($dir), $fileName);
            $cont->txt7 = $fileName;
        }

        $cont->txt8 = $request->txt8;
        $cont->txt9 = $request->txt9;
        if ($request->hasFile('txt10')) {
            $dir = 'images/homePage/';
            $extension = strtolower($request->file('txt10')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('txt10')->move(public_path($dir), $fileName);
            $cont->txt10 = $fileName;
        }

        $cont->save();
        return redirect()->back();
    }

    public function fun2(Request $request)
    {
        $cont = Content::find(2);
        $cont->txt1 = $request->txt1;
        $cont->txt2 = $request->txt2;
        $cont->txt3 = $request->txt3;
        $cont->txt4 = $request->txt4;
        $cont->txt5 = $request->txt5;
        $cont->txt6 = $request->txt6;
        $cont->txt7 = $request->txt7;
        $cont->txt8 = $request->txt8;
        $cont->txt9 = $request->txt9;
        $cont->txt10 = $request->txt10;
        $cont->save();

        return redirect()->back();
    }

    public function fun3(Request $request)
    {
        $cont = Content::find(3);
        $cont->txt1 = $request->txt1;
        $cont->txt2 = $request->txt2;
        $cont->txt3 = $request->txt3;
        $cont->txt4 = $request->txt4;
        $cont->txt5 = $request->txt5;
        $cont->txt6 = $request->txt6;
        $cont->txt7 = $request->txt7;
        $cont->txt8 = $request->txt8;
        $cont->save();
        return redirect()->back();
    }

    public function fun4(Request $request)
    {
        $request->validate([
            'txt3' => 'mimes:jpeg,jpg,png,gif',
            'txt6' => 'mimes:jpeg,jpg,png,gif',
            'txt9' => 'mimes:jpeg,jpg,png,gif',
            'txt12' => 'mimes:jpeg,jpg,png,gif',
            'txt15' => 'mimes:jpeg,jpg,png,gif',
            'txt18' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $cont = Content::find(4);
        $cont->txt1 = $request->txt1;
        $cont->txt2 = $request->txt2;


        if ($request->hasFile('txt3')) {
            $dir = 'images/homePage/';
            $extension = strtolower($request->file('txt3')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('txt3')->move(public_path($dir), $fileName);
            $cont->txt3 = $fileName;
        }
        $cont->txt4 = $request->txt4;
        $cont->txt5 = $request->txt5;
        $cont->txt21 = $request->txt21;


        if ($request->hasFile('txt6')) {
            $dir = 'images/homePage/';
            $extension = strtolower($request->file('txt6')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('txt6')->move(public_path($dir), $fileName);
            $cont->txt6 = $fileName;
        }
        $cont->txt7 = $request->txt7;
        $cont->txt8 = $request->txt8;
        $cont->txt22 = $request->txt22;


        if ($request->hasFile('txt9')) {
            $dir = 'images/homePage/';
            $extension = strtolower($request->file('txt9')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('txt9')->move(public_path($dir), $fileName);
            $cont->txt9 = $fileName;
        }
        $cont->txt10 = $request->txt10;
        $cont->txt11 = $request->txt11;
        $cont->txt23 = $request->txt23;

        if ($request->hasFile('txt12')) {
            $dir = 'images/homePage/';
            $extension = strtolower($request->file('txt12')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('txt12')->move(public_path($dir), $fileName);
            $cont->txt12 = $fileName;
        }
        $cont->txt13 = $request->txt13;
        $cont->txt14 = $request->txt14;
        $cont->txt24 = $request->txt24;

        if ($request->hasFile('txt15')) {
            $dir = 'images/homePage/';
            $extension = strtolower($request->file('txt15')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('txt15')->move(public_path($dir), $fileName);
            $cont->txt15 = $fileName;
        }
        $cont->txt16 = $request->txt16;
        $cont->txt17 = $request->txt17;
        $cont->txt25 = $request->txt25;


        if ($request->hasFile('txt18')) {
            $dir = 'images/homePage/';
            $extension = strtolower($request->file('txt18')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('txt18')->move(public_path($dir), $fileName);
            $cont->txt18 = $fileName;
        }
        $cont->txt19 = $request->txt19;
        $cont->txt20 = $request->txt20;
        $cont->txt26 = $request->txt26;

        $cont->save();
        return redirect()->back();
    }

    public function fun5(Request $request)
    {
        $cont = Content::find(5);
        $cont->txt1 = $request->txt1;
        $cont->txt2 = $request->txt2;
        $cont->save();
        return redirect()->back();
    }

    public function fun6(Request $request)
    {
        $request->validate([
            'txt4' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $cont = Content::find(6);
        $cont->txt1 = $request->txt1;
        $cont->txt2 = $request->txt2;
        $cont->txt3 = $request->txt3;
        if ($request->hasFile('txt4')) {
            $dir = 'images/homePage/';
            $extension = strtolower($request->file('txt4')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('txt4')->move(public_path($dir), $fileName);
            $cont->txt4 = $fileName;
        }
        $cont->save();
        return redirect()->back();
    }

    public function fun7(Request $request)
    {
        $request->validate([
            'txt4' => 'mimes:jpeg,jpg,png,gif',
        ]);
        $cont = Content::find(7);
        $cont->txt1 = $request->txt1;
        $cont->txt2 = $request->txt2;
        $cont->txt3 = $request->txt3;
        if ($request->hasFile('txt4')) {
            $dir = 'images/homePage/';
            $extension = strtolower($request->file('txt4')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('txt4')->move(public_path($dir), $fileName);
            $cont->txt4 = $fileName;
        }
        $cont->save();
        return redirect()->back();
    }

    public function fun8(Request $request)
    {
        $cont = Content::find(8);

        return redirect()->back();
    }

    public function fun9(Request $request)
    {


        return redirect()->back();
    }
}
