<?php

namespace App\Http\Controllers;

use App\ArticleLang;
use App\Seo;
use App\SeoLang;
use App\SiteInfo;
use Illuminate\Http\Request;

class SeoController extends Controller
{

    public function seoKey(Request $request)
    {
        $article = Seo::find($request->id);
        foreach ($request['title'] as $key => $lang) {

            $limba = $article->langs->where('article_id', $request->id)->where('lang_id', $key)->first();
            $limba->title = $lang;
            $limba->description = $request['description'][$key];
            $limba->keywords = $request['keywords'][$key];
            $limba->slug = $request['keywords'][$key];
            $limba->update();

        }
        return redirect()->back();
    }

    public function seoInfo(Request $request)
    {

        $info = SiteInfo::find(1);
        $info->phone = $request->phone;
        $info->phone1 = $request->phone1;
        $info->address = $request->address;
        $info->work = $request->work;
        $info->email = $request->email;
        $info->fb = $request->fb;
        $info->go = $request->go;
        if ($request->hasFile('image')) {
            $dir = 'images/seo/';
            $extension = strtolower($request->file('image')->getClientOriginalExtension()); // get image extension
            $fileName = str_random() . '.' . $extension; // rename image
            $request->file('image')->move(public_path($dir), $fileName);
            $info->logo = $fileName;
        }
        $info->save();

        return redirect()->back();
    }

    public function addSiteAbout(Request $request){

        foreach ($request->content as $key => $lang) {
            if(ArticleLang::where('article_id', $request->id)->where('lang_id', $key)->exists()){
                $productNameTranslation = ArticleLang::where('lang_id', $key)->first();
                $productNameTranslation->article_id = $request->id;
                $productNameTranslation->name = $request['content'][$key];
                $productNameTranslation->slug = 'best';
                $productNameTranslation->lang_id = $key;
                $productNameTranslation->save();
            }else{
                $newDesc = new SiteInfo();
                $newDesc->go = 'best';
                $newDesc->save();
                $productNameTranslation = new ArticleLang();
                $productNameTranslation->article_id = $newDesc->id;
                $productNameTranslation->name = $request['content'][$key];
                $productNameTranslation->slug = 'best';
                $productNameTranslation->lang_id = $key;
                $productNameTranslation->save();
            }



        }

        return redirect()->back();
    }
}
