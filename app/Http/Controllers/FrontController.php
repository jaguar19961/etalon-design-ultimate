<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Blog;
use App\Catalog;
use App\Category;
use App\Colors;
use App\Content;
use App\DoorType;
use App\Furnitures;
use App\Mail\Cerere;
use App\Mail\CerereMesaj;
use App\Mail\Colaboram;
use App\Offer;
use App\OneOffer;
use App\OpSystem;
use App\ProductColor;
use App\ProductFurniture;
use App\ProductNew;
use App\ProductPopular;
use App\Products;
use App\Seo;
use App\Serie;
use App\SiteInfo;
use App\Style;
use App\Window;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class FrontController extends Controller
{
    public function first()
    {
        return redirect()->route('intro');
    }

    public function intro()
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $city = Session::get('city_id');
        return view('front.partials.intro', compact('infos', 'seo', 'categories', 'city'));
    }

    public function setCity($value)
    {
        Session::put('city_id', $value);
        return redirect()->route('acasa',  Session::get('city_id'));
    }

    public function index()
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $banners = Banner::get();
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $getProducts = ProductNew::inRandomOrder()->take(50)->get();
        $newProduct = ProductPopular::inRandomOrder()->take(50)->get();
        $products = Serie::get();
        $getOffers = OneOffer::find(1);
        $offers = Offer::inRandomOrder()->take(10)->get();
        $catalogOffer = Catalog::get();
        $cat1 = Catalog::find(6);
        $cat2 = Catalog::find(7);
        $cat3 = Catalog::find(8);
        $cat4 = Catalog::find(9);
        $cat5 = Catalog::find(10);
        $cat6 = Catalog::find(11);
        $blog = Blog::where('publicata', 1)->orderBy('position', 'asc')->take(6)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        $content = Content::get();
        $openSystem = OpSystem::get();
//        return view('front.partials.index', compact('categories', 'getProducts', 'newProduct',
//            'getOffers', 'banners', 'offers', 'catalogOffer', 'infos', 'seo', 'products',
//            'cat1', 'cat2', 'cat3', 'cat4', 'cat5', 'cat6'));

        return view('front.index', compact('categories', 'seo', 'infos', 'blog', 'products', 'content',
            'openSystem', 'toMenu'));
    }

    public function openSystemShow($slug, $id, $idd)
    {
        $infos = SiteInfo::find(1);
        $about = SiteInfo::find(3);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $content = OpSystem::find($idd);
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        return view('front/partials/opsystem-show', compact('content', 'infos', 'seo', 'categories', 'about', 'toMenu'));
    }

    public function openSystem()
    {

        $infos = SiteInfo::find(1);
        $about = SiteInfo::find(3);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        $openSystem = OpSystem::get();
        return view('front/partials/opensystem', compact('openSystem', 'infos', 'about', 'seo', 'categories', 'toMenu'));
    }

    public function about()
    {

        $infos = SiteInfo::find(1);
        $about = SiteInfo::find(3);
        $seo = Seo::find(6);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        return view('front.partials.about', compact('categories', 'infos', 'seo', 'about', 'toMenu'));
    }

    public function article()
    {

        $infos = SiteInfo::find(1);
        $seo = Seo::find(8);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        return view('front.partials.article', compact('categories', 'infos', 'seo', 'toMenu'));
    }

    public function blog()
    {

        $infos = SiteInfo::find(1);
        $seo = Seo::find(7);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $blogs = Blog::where('publicata', 1)->orderBy('created_at', 'desc')->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        return view('front.partials.blog', compact('categories', 'blogs', 'infos', 'seo', 'toMenu'));
    }

    public function category($slug, $id, $idd)
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $thisCategory = Category::find($idd);
        if ($idd == 1 || $idd == 2) {
            $showserie = Serie::where('category_id', $idd)->where('active', 1)->get();
        } elseif ($idd == 3 || $idd == 4) {
            $showserie = Products::where('category_id', $idd)->get();
        }

        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        return view('front.partials.category', compact('categories', 'showserie', 'thisCategory', 'infos', 'seo', 'toMenu'));
    }

//    toate colectiile
    public function allcolections()
    {
        $series = Serie::get(['id', 'name']);

        // dd($series);
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
//        $thisCategory = Serie::find($id);
        $showProductSeries = Products::get();
        $colors = Colors::get();
        $min = Products::min('price');
        $max = Products::max('price');

        //filtrare culori dupa serie
        $cat = $showProductSeries->pluck('id')->toArray();

        $groupcolors = Colors::whereHas('products', function ($query) use ($cat) {
            $query->whereIn('products.id', $cat);
        })->distinct()->orderBy('type_id')->get()->groupBy('type_id');

//        filtrare dupa insertii
        $groupwindows = Window::whereHas('products', function ($query) use ($cat) {
            $query->whereIn('products.id', $cat);
        })->distinct()->get();

//        dd($groupwindows);
//        afisare tip de usa din modelul DoorType
        $doorType = DoorType::get();
        $doorStyle = Style::get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();

        return view('front.partials.allseries', compact('categories', 'colors', 'showProductSeries', 'infos', 'seo', 'series', 'min', 'max',
            'groupcolors', 'doorType', 'doorStyle', 'groupwindows', 'toMenu'));
    }

    public function serie(Request $request, $slug = null, $id = null)
    {
//        dd(array_filter([null,5,4,3]));
        $series = Serie::get(['id', 'name']);
//        dd($id);
//        dd($request->all());

        // dd($series);
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();

        if ($id != null) {
            $thisCategory = Serie::find($id);
            //dd($thisCategory);
            $showProductSeries = Products::where('serie_id', $id)->get();

            $min = Products::where('serie_id', $id)->min('price');
            $max = Products::where('serie_id', $id)->max('price');

            //filtrare culori dupa serie
            $cat = $showProductSeries->pluck('id')->toArray();

            $groupcolors = Colors::whereHas('products', function ($query) use ($cat) {
                $query->whereIn('products.id', $cat);
            })->distinct()->orderBy('type_id')->get()->groupBy('type_id');

//        filtrare dupa insertii
            $groupwindows = Window::whereHas('products', function ($query) use ($cat) {
                $query->whereIn('products.id', $cat);
            })->distinct()->get();
        } else {
//           daca nu are slug
//            dd($request->all());
            $thisCategory = null;

            $cat = Products::get('id')->pluck('id')->toArray();
            $showProductSeries = [];

            // $colors = Colors::get();

            $min = Products::min('price');
            $max = Products::max('price');


            $groupcolors = Colors::whereHas('products', function ($query) use ($cat) {
                $query->whereIn('products.id', $cat);
            })->distinct()->orderBy('type_id')->get()->groupBy('type_id');

//        filtrare dupa insertii
            $groupwindows = Window::whereHas('products', function ($query) use ($cat) {
                $query->whereIn('products.id', $cat);
            })->distinct()->get();
        }
//        afisare tip de usa din modelul DoorType
        $doorType = DoorType::get();
        $doorStyle = Style::get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();

        $door_type = $request->has('door_type') ? $request->door_type : 'no';
        $styles = $request->has('styles') ? $request->styles : 'no';
        $color_id = $request->has('color_id') ? $request->color_id : 'no';
        $window_id = $request->has('window_id') ? $request->window_id : 'no';


        return view('front.partials.serie', compact('categories', 'showProductSeries', 'thisCategory', 'infos', 'seo', 'series', 'min', 'max',
            'groupcolors', 'doorType', 'doorStyle', 'groupwindows', 'toMenu', 'door_type', 'styles', 'color_id', 'window_id'));
    }

    public function contact()
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(9);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        return view('front.partials.contact', compact('categories', 'infos', 'seo', 'toMenu'));
    }

    public function offers()
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(8);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $offers = Offer::where('publicata', 1)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        return view('front.partials.offers', compact('categories', 'offers', 'infos', 'seo', 'toMenu'));
    }

    public function catalog()
    {
        $seo = Seo::find(1);
        $infos = SiteInfo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        return view('front.partials.catalog', compact('seo', 'infos', 'categories'));
    }

    public function product($serie, $produc, $product)
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        $modelId = $product;

        $getProduct = Products::with('colors')->with('colors.windows')->with('colors.windows.window')->with('colors.color')->find($product);
//        $thisCategory = Serie::find($getProduct->serie_id);
//        $thisFurniture = ProductFurniture::where('product_id', $product)->get();
//        $furnitures = Furnitures::get();
        $showProductSeries = [];
        if (!empty($getProduct)) {
            $showProductSeries = Products::where('serie_id', $getProduct->serie_id)->inRandomOrder()->take(10)->get();
        }

//        $series = Serie::get();
        return view('front.partials.product',
            compact('categories',
                'infos',
                'seo',
                'toMenu',
                'modelId',
                'getProduct', 'showProductSeries'));
    }

    public function showBlog($slug, $id, $idd)
    {
        $infos = SiteInfo::find(1);
        $seo = Blog::find($idd);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $item = Blog::find($idd);
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        $is_blog = true;
        return view('front.partials.article', compact('categories', 'item', 'infos', 'seo', 'toMenu', 'is_blog'));
    }

    public function showOffer($slug, $id, $idd)
    {

        $infos = SiteInfo::find(1);
        $seo = Offer::find($idd);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $item = Offer::find($idd);
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        $is_blog = false;
        return view('front.partials.article', compact('categories', 'item', 'infos', 'seo', 'toMenu', 'is_blog'));


    }

    public function DoorFilter(Request $request, $serie_id = null)
    {
//        dd($serie_id);
        $dors = Products::where(function ($query) use ($request, $serie_id) {

            if ($serie_id != null) {
                $query->where('serie_id', $serie_id);
            }

            $price = json_decode($request->price);
            $query->whereBetween('price', $price);

            if ($request->color_id != null) {
                $color_id = $request->color_id;
                $query->whereHas('colors', function ($query) use ($color_id) {
                    $query->where('color_id', $color_id);
                });
            }

//            filtrare dupa insertii
            if ($request->window_id != null) {
                $window_id = $request->window_id;
                $query->whereHas('windows', function ($query) use ($window_id) {
                    $query->where('window_id', $window_id);
                });
            }

            if ($request->styles != null) {

                $styles = json_decode($request->styles);
                $query->whereIn('style_id', $styles);
            }

            if ($request->door_type != null) {
                $query->where('type_id', $request->door_type);
            }

        })->with('colors')->paginate(8);

        return response($dors);
    }

    public function terms()
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        return view('front.partials.termeni', compact('infos', 'seo', 'categories', 'toMenu'));
    }

    public function confident()
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        return view('front.partials.cinfidet', compact('infos', 'seo', 'categories', 'toMenu'));
    }

    public function multumim()
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        return view('front/partials/multumim', compact('infos', 'seo', 'categories', 'toMenu'));
    }

    public function getOffer(Request $request)
    {
        $door_name = $request->door_name;
        $name = $request->name;
        $phone = $request->phone;
        $size_door = $request->size_door;
        $door_numbers = $request->door_numbers;
        $color = $request->color;
        Mail::send(new Cerere($door_name, $name, $phone, $size_door, $door_numbers, $color));
        return response(['status' => 200]);
    }

    public function getOffer1(Request $request)
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();

        $name = $request->name;
        $phone = $request->phone;
        $mesaj = $request->mesaj;
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        Mail::send(new CerereMesaj($name, $phone, $mesaj));
        return view('front/partials/multumim', compact('infos', 'seo', 'categories', 'toMenu'));
    }

    public function getColaboration(Request $request)
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        Mail::send('front/mail/colaboram', $request->all(), function ($query) {
            $query->from('support@etalon.ro', 'Etalon');
            $query->to('info@etalon.ro');
            $query->subject('Colaborare');
        });
        return view('front/partials/multumim', compact('infos', 'seo', 'categories', 'toMenu'));
    }

    public function getNewsletter(Request $request)
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        Mail::send('front/mail/newsletter', $request->all(), function ($query) {
            $query->from('support@etalon.ro', 'Etalon');
            $query->to('info@etalon.ro');
            $query->subject('Newsletter');
        });
        return view('front/partials/multumim', compact('infos', 'seo', 'categories', 'toMenu'));
    }

    public function getPhone(Request $request)
    {
        $infos = SiteInfo::find(1);
        $seo = Seo::find(1);
        $categories = Category::orderBy('position', 'asc')->where('activity', 1)->get();
        $toMenu = Blog::where('publicata', 1)->where('to_menu', 1)->get();
        Mail::send('front/mail/phone', $request->all(), function ($query) {
            $query->from('support@etalon.ro', 'Etalon');
            $query->to('info@etalon.ro');
            $query->subject('Newsletter');
        });
        return view('front/partials/multumim', compact('infos', 'seo', 'categories', 'toMenu'));
    }

}
