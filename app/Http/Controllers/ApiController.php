<?php

namespace App\Http\Controllers;

use App\OpSystem;
use App\OpGallery;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Image;

class ApiController extends Controller
{
    public function SaveEdit(Request $request)
    {
//        dd($request->all());
        $data = OpSystem::findOrFail($request->op_id);
        $data->sc1_title = $request->sc1_title;
        $data->sc1_desc = $request->sc1_desc;
        $data->sc1_text = $request->sc1_text;
        $data->sc2_title = $request->sc2_title;
        $data->sc2_desc = $request->sc2_desc;
        $data->sc2_text = $request->sc2_text;
        $data->save();

        return response(['status' => 'success']);
    }

    public function saveImage(Request $request, $id, $section)
    {
//        dd($id);
        $gallery = OpSystem::findOrFail($id);

        $filename = time() . Carbon::now()->microsecond . '.' . $request->file('file')->getClientOriginalExtension();
        $path = public_path('images/site/' . $filename);
        $img = Image::make($request->file('file'), $path);
        $img->save($path);
        if ($section == 1) {
            $gallery->sc1_img = $filename;
        } elseif ($section == 2) {
            $gallery->sc2_img = $filename;
        }

        $gallery->save();
        return response(['status' => 'success']);
    }

    public function saveGallery(Request $request, $id, $section)
    {
//        dd($id);
        $gallery = new OpGallery();

        $filename = time() . Carbon::now()->microsecond . '.' . $request->file('file')->getClientOriginalExtension();
        $path = public_path('images/site/' . $filename);

        $img = Image::make($request->file('file'), $path);
        $img->save($path);
        $gallery->op_id = $id;
        $gallery->name = $filename;
        $gallery->url = 'https://etalon.ro/images/site/' . $filename;
        $gallery->position = $section;

        $gallery->save();
        return response(['status' => 'success']);
    }

    public function delImg(Request $request)
    {
//        dd($request->all());
        $gallery = OpGallery::findOrFail($request->id);
        $path = public_path('/images' . '/site' . '/' . $gallery->image);
        File::delete($path);
        $gallery->delete();

        $oldGallery = OpGallery::where('op_id', $request->op_id)->get();
        return response(['status' => 'success', 'gallery' => $oldGallery]);
    }
}
