<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPlane extends Model
{
    protected $table = 'product_planes';
}
