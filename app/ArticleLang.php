<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleLang extends Model
{
    protected $table = 'article_langs';
}
