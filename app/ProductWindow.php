<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductWindow extends Model
{
    protected $table = 'product_windows';

    protected $with = ['window'];

    public function window(){
        return $this->hasOne(Window::class,'id', 'window_id');
    }
}
