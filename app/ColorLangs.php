<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ColorLangs extends Model
{
    protected $table = 'color_langs';

    public function category(){
        return $this->belongsTo(Colors::class, 'id');
    }
}
