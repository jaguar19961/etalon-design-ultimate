<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Cerere extends Mailable
{
    use Queueable, SerializesModels;

    public $door_name;
    public $name;
    public $phone;
    public $size_door;
    public $door_numbers;
    public $color;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($door_name, $name, $phone, $size_door, $door_numbers, $color)
    {
        $this->door_name = $door_name;
        $this->name = $name;
        $this->phone = $phone;
        $this->size_door = $size_door;
        $this->door_numbers = $door_numbers;
        $this->color = $color;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('mail@etalon.ro')
            ->to('info@etalon.ro')
            ->view('front.mail.cerere');
    }
}
