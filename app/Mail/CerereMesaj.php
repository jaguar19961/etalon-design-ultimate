<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CerereMesaj extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $phone;
    public $mesaj;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $phone, $mesaj)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->mesaj = $mesaj;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('mail@etalon.ro')
            ->to('info@etalon.ro')
            ->view('front.mail.cerere1');
    }
}
