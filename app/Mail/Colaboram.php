<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Colaboram extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $phone;
    public $email;
    public $subject1;
    public $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $phone, $email, $subject1 , $message)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->email = $email;
        $this->subject1 = $subject1;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('mail@etalon.ro')
            ->to('anghel_vadim@mail.ru')
            ->view('front.mail.colaboram');
    }
}
