
window.onscroll = function() {scrollFunction(); myFunction(); myFunction2();};

function myFunction() {
    var navbar = document.getElementById("navbar");
// Get the offset position of the navbar
    if(navbar != null){
        var sticky = navbar.offsetTop;

        if (window.pageYOffset >= sticky+60) {
            navbar.classList.add("is-fixed-top");
            navbar.classList.remove("is-transparent");
            navbar.classList.add("is-white");
        } else {
            navbar.classList.remove("is-fixed-top");
            navbar.classList.add("is-transparent");
            navbar.classList.remove("is-white");
        }
    }

}

function myFunction2() {
    var navbar = document.getElementById("navbar2");
// Get the offset position of the navbar
    if(navbar != null){
        var sticky = navbar.offsetTop;

        if (window.pageYOffset >= sticky+60) {
            navbar.classList.add("is-fixed-top");
        } else {
            navbar.classList.remove("is-fixed-top");
        }
    }

}

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("to_top").style.opacity = 1;
    } else {
        document.getElementById("to_top").style.opacity = 0;
    }
}

function loadData() {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, 2000);
    })
}

loadData()
  .then(() => {
      let preloaderEl = document.getElementById('preloader');
      preloaderEl.classList.add('hidden');
      preloaderEl.classList.remove('visible');
  });

require('./bulma-extensions');

document.addEventListener('DOMContentLoaded', function () {
    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                let target = $el.dataset.target;
                let $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});

require('./bootstrap');

window.Vue = require('vue');



import './theme/element-variables.scss';
import {
    Dropdown,
    DropdownMenu,
    DropdownItem,
    Menu,
    Submenu,
    MenuItem,
    MenuItemGroup,
    Slider,
    Radio,
    RadioGroup,
    RadioButton,
    Checkbox,
    CheckboxButton,
    CheckboxGroup,
    Button,
    Dialog,
    ButtonGroup,
    Upload} from 'element-ui';

Vue.use(Dropdown);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Menu);
Vue.use(Submenu);
Vue.use(MenuItem);
Vue.use(MenuItemGroup);
Vue.use(Slider);
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(RadioButton);
Vue.use(Checkbox);
Vue.use(CheckboxButton);
Vue.use(CheckboxGroup);
Vue.use(Button);
Vue.use(ButtonGroup);
Vue.use(Dialog);
Vue.use(Upload);

Vue.component('pagination', require('laravel-vue-pagination'));

import InfiniteLoading from 'vue-infinite-loading';

import CookieLaw from 'vue-cookie-law'
Vue.component('cookie-law', require('vue-cookie-law'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app',
    components: {
        InfiniteLoading,
        CookieLaw
    },
    data: {
        activeTab: 'complect',
        serch: false,
        preloader: true,
        min: 0,
        max: 100,
        serie_id: null,
        group_colors: [],
        group_windows: [],
        door_styles: [],
        door_types: [],

        laravelData: {},

        price: [0,100],
        color_id: '',
        window_id: '',
        styles: [],
        door_type: '',
        city_name: 'iasi',

        doors: [],
        loading: true,
        page: 1,
        infiniteId: +new Date(),
        show_more: false,
        series: [],
        tags: [],
    },
    computed:{
      textShow(){
          if(this.show_more == false){
              return 'Află mai multe detalii despre această serie'
          }else{
              return 'Ascunde informatiile adaugatoare'
          }
      }
    },

    created() {
        if(window.door_tp != null){
            this.styles.push(parseInt(window.door_tp))
        }
        this.getSeries();
    },
    methods: {
        topFunction(){
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        },
        getSeries() {
            axios.get('https://order.profildoors.ro/api/series/')
                .then(response => {
                    this.series = response.data;
                    this.getColors();
                    this.getWindows();
                    this.getTags();
                });
        },
        getTags() {
            axios.get('https://order.profildoors.ro/api/tags/')
                .then(response => {
                    this.tags = response.data.data;
                });
        },


        getResults(page = 1) {
           var styles = this.styles.length > 0 ? JSON.stringify(this.styles) : '';

            // axios.get('/door-filter/'+this.serie_id+'?page='+page+'&price='+JSON.stringify(this.price)+'&color_id='+this.color_id+'&window_id='+this.window_id+'&styles='+styles+'&door_type='+this.door_type)
            axios.get('https://order.profildoors.ro/api/product-series/'+this.serie_id+'?page='+page+'&price='+JSON.stringify(this.price)+'&color_id='+this.color_id+'&window_id='+this.window_id+'&styles='+styles+'&door_type='+this.door_type)
              .then(response => {
                  this.laravelData = response.data;
                  this.doors = response.data.data;
                  if(response.data.data.length>0){

                      //this.loading = true
                  }else{
                      //this.loading = false
                  }

              });
        },
        infiniteHandler($state) {
            var styles = this.styles.length > 0 ? JSON.stringify(this.styles) : '';
            axios.get('https://order.profildoors.ro/api/product-series/' + this.serie_id, {
                // axios.get('/door-filter/'+this.serie_id, {
                params: {
                    page: this.page,
                    price: JSON.stringify(this.price),
                    color_id: this.color_id,
                    window_id: this.window_id,
                    styles: styles,
                    door_type: this.door_type
                }
            }).then(response => {
                if (response.data.data.length) {
                    this.page += 1;
                    this.doors.push(...response.data.data);
                    $state.loaded();
                } else {
                    $state.complete();
                }

            });
        },
        getColors() {
            axios.get('https://order.profildoors.ro/api/colors/' + this.serie_id)
                .then(response => {
                    this.group_colors = response.data.data;
                });
        },

        getWindows() {
            axios.get('https://order.profildoors.ro/api/windows/' + this.serie_id)
                .then(response => {
                    this.group_windows = response.data.data;
                });
        },

        GetDors(){
            this.page = 1;
            this.doors = [];
            this.infiniteId += 1;
            window.scrollTo({
                top: this.serie_id == '' ? 220 : 700,
                behavior: "smooth"
            });
        },
        resetFilter(){
          var p = [parseInt(min_value), parseInt(max_value)];
          this.price = p;
          this.color_id = '';
          this.window_id = '';
          this.styles = [];
          this.door_type = '';
          this.GetDors()
        },
        getImage(door) {
            var img = 'no-image.png';
            if (this.color_id == '' && this.window_id == '') {
                if (door.colors.length > 0) {
                    // img = door.colors[0].image_original != null ? door.colors[0].image_original : 'no-image.png'
                    img = door.colors[0].image_original ? '/catalog-img-original/' + door.colors[0].image_original : '/catalog/' + door.colors[0].image
                }
            } else if (this.color_id != '' && this.window_id == '') {
                if (door.colors.length > 0) {
                    let color = door.colors.find(color => color.color_id == this.color_id);
                    // img = color.image_original != null ? color.image_original : 'no-image.png'
                    img = color.image_original ? '/catalog-img-original/' + color.image_original : '/catalog/' + color.image
                }
            } else if (this.color_id == '' && this.window_id != '') {
                var vind = []
                if (door.colors.length > 0) {
                    door.colors.forEach((el) => {
                        if (el.windows.length > 0) {
                            el.windows.forEach((e) => {
                                vind.push(e)
                            })
                        }
                    })
                }

                if (vind.length > 0) {
                    let w = vind.find(w => w.window_id == this.window_id);
                    img = w.image_original ? '/catalog-img-original/' + w.image_original : '/catalog/' + w.image
                }
            } else if (this.color_id != '' && this.window_id != '') {
                if (door.colors.length > 0) {
                    let color = door.colors.find(color => color.color_id == this.color_id);

                    if (color.windows.length > 0) {
                        let w = color.windows.find(w => w.window_id == this.window_id);
                        img = w.image_original ? '/catalog-img-original/' + w.image_original : '/catalog/' + w.image
                    }
                }

            }
            return img
        },
    },
    mounted(){
        this.min = parseInt(min_value)
        this.max = parseInt(max_value)
        var p = [parseInt(min_value), parseInt(max_value)]
        this.price = p
        this.serie_id = serie_id
        // this.group_colors = groupcolors
        // this.group_windows = groupwindows
        this.door_styles = doorStyles
        this.door_types = doorTypes
        this.city_name = city_name

        // this.$watch(vm => [
        //     vm.price,
        //     vm.color_id,
        //     vm.window_id,
        //     vm.styles,
        //     vm.door_type], val => {
        //     this.GetDors();
        // }, {
        //     immediate: true
        // })
        //
        // setTimeout(() => {
        //     this.preloader = false
        // }, 1000);

    }
});


// Bulma NavBar Burger Script

