require('./bootstrap');

window.onscroll = function() {scrollFunction(); myFunction()};

function myFunction() {
    var navbar = document.getElementById("navbar");
// Get the offset position of the navbar
    var sticky = navbar.offsetTop;

    if (window.pageYOffset >= sticky+60) {
        navbar.classList.add("is-fixed-top");
        navbar.classList.remove("is-transparent");
        navbar.classList.add("is-white");
    } else {
        navbar.classList.remove("is-fixed-top");
        navbar.classList.add("is-transparent");
        navbar.classList.remove("is-white");
    }
}

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("to_top").style.opacity = 1;
    } else {
        document.getElementById("to_top").style.opacity = 0;
    }
}

function loadData() {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, 2000);
    })
}

loadData()
    .then(() => {
        let preloaderEl = document.getElementById('preloader');
        preloaderEl.classList.add('hidden');
        preloaderEl.classList.remove('visible');
    });

require('./bulma-extensions');

document.addEventListener('DOMContentLoaded', function () {
    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                let target = $el.dataset.target;
                let $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});

$(document).ready(function() {
    /* Tabs */
    $('.tabs a').each(function() {
        $(this).click(function(e) {
            e.preventDefault();
            var target = $(this).data('target');
            $('.tabs a').removeClass('is_active');
            $(this).addClass('is_active');
            $('.tab_panel').removeClass('is_active');
            $(target).addClass('is_active');
        })
    })
});