require('./bootstrap');

window.onscroll = function() {scrollFunction(); myFunction(); myFunction2();};

function myFunction() {
    var navbar = document.getElementById("navbar");
// Get the offset position of the navbar
    if(navbar != null){
        var sticky = navbar.offsetTop;

        if (window.pageYOffset >= sticky+60) {
            navbar.classList.add("is-fixed-top");
            navbar.classList.remove("is-transparent");
            navbar.classList.add("is-white");
        } else {
            navbar.classList.remove("is-fixed-top");
            navbar.classList.add("is-transparent");
            navbar.classList.remove("is-white");
        }
    }

}

function myFunction2() {
    var navbar = document.getElementById("navbar2");
// Get the offset position of the navbar
    if(navbar != null){
        var sticky = navbar.offsetTop;

        if (window.pageYOffset >= sticky+60) {
            navbar.classList.add("is-fixed-top");
        } else {
            navbar.classList.remove("is-fixed-top");
        }
    }

}

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("to_top").style.opacity = 1;
    } else {
        document.getElementById("to_top").style.opacity = 0;
    }
}

function loadData() {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, 2000);
    })
}

loadData()
  // .then(() => {
  //     let preloaderEl = document.getElementById('preloader');
  //     preloaderEl.classList.add('hidden');
  //     preloaderEl.classList.remove('visible');
  // });

require('./bulma-extensions');

document.addEventListener('DOMContentLoaded', function () {
    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                let target = $el.dataset.target;
                let $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});


require('./jquery-modal-video');
require('./main');



window.Vue = require('vue');

import CookieLaw from 'vue-cookie-law'

import VueAwesomeSwiper from 'vue-awesome-swiper'
import carousel from 'vue-owl-carousel'
Vue.component('carousel', require('vue-owl-carousel'));
// require styles
import 'swiper/dist/css/swiper.css'
import './theme/element-variables.scss';
import {
    Dialog,
    Dropdown,
    DropdownMenu,
    DropdownItem,
    Menu,
    Submenu,
    MenuItem,
    MenuItemGroup,
    Slider,
    Radio,
    RadioGroup,
    RadioButton,
    Checkbox,
    CheckboxButton,
    CheckboxGroup,
    Button,
    ButtonGroup,
    Upload} from 'element-ui';


Vue.use(Dropdown);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Menu);
Vue.use(Submenu);
Vue.use(MenuItem);
Vue.use(MenuItemGroup);
Vue.use(Slider);
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(RadioButton);
Vue.use(Checkbox);
Vue.use(CheckboxButton);
Vue.use(CheckboxGroup);
Vue.use(Button);
Vue.use(ButtonGroup);
Vue.use(Upload);
Vue.use(Dialog);

Vue.component('pagination', require('laravel-vue-pagination'));


Vue.use(VueAwesomeSwiper, /* { default global options } */)


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('carousel-sistem', require('./components/CarouselSistem.vue').default);
Vue.component('collection-edit', require('./components/admin/CollectionEdit.vue').default);
Vue.component('main-slider', require('./components/MainSlider.vue').default);
Vue.component('catalog-series', require('./components/CatalogSeries.vue').default);
Vue.component('products-catalog', require('./components/catalog/ProductsCatalog.vue').default);
// Vue.component('product-series', require('./components/ProductSeries.vue').default);

const app = new Vue({
    el: '#app',
    components: { CookieLaw },
    data: {
        activeTab: 'complect',
        infodialog: true,
        serch: false,
        swiperOption:{
            slidesPerView: 6,
            spaceBetween: 30,
            freeMode: true,
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
                waitForTransition: false,
            },
            navigation: {
                nextEl: '.sw-button-next',
                prevEl: '.sw-button-prev'
            },
            breakpoints:{
                1408: {
                    slidesPerView: 6,
                    spaceBetween: 40
                },
                1216: {
                    slidesPerView: 5,
                    spaceBetween: 30
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 30
                },
                769: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                }
            }
        },
        swiperOptionBlog: {
            slidesPerView: 4,
            spaceBetween: 15,
            freeMode: true,
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
                waitForTransition: false,
            },
            navigation: {
                nextEl: '.sw-btn-next',
                prevEl: '.sw-btn-prev'
            },
            breakpoints:{
                1408: {
                    slidesPerView: 4,
                    spaceBetween: 15
                },
                1216: {
                    slidesPerView: 4,
                    spaceBetween: 15
                },
                1024: {
                    slidesPerView: 2,
                    spaceBetween: 15
                },
                769: {
                    slidesPerView: 1,
                    spaceBetween: 15
                },
                320: {
                    slidesPerView: 1,
                    spaceBetween: 15
                }
            }
        },
        preloader: true,
        windows: [],
        responsive: {
            0: {
                items: 1
            },
            576: {
                items: 1
            },
            768: {
                items: 1
            },
            992: {
                items: 1
            },
            1200:{
                items: 1
            }
        },
        swiperOptionSistem: {
            effect: 'coverflow',
            grabCursor: true,
            slidesPerView: 3,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true
            },
            navigation: {
                nextEl: '.sw-button-sistem-next',
                prevEl: '.sw-button-sistem-prev'
            },
        },
        swiperOptionSistem2: {
            effect: 'coverflow',
            grabCursor: true,
            slidesPerView: 3,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true
            },
            navigation: {
                nextEl: '.sistem-next',
                prevEl: '.sistem-prev'
            },
        }
    },
    methods: {
        handleClose(done) {
            console.log('inchis');
            var poppy = localStorage.getItem('myPopup');
            if(!poppy) {
                localStorage.setItem('infodialog','true');

                done();
            }else{
                this.infodialog = false
            }


        },

        topFunction(){
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        },
        getWindows(windows){
            this.windows = windows
        }
    },

    created(){
        var poppy = localStorage.getItem('infodialog');
        if(poppy) {
            this.infodialog = false
        }
    },

    mounted(){
        setTimeout(() => {
            this.preloader = false
        }, 1000);

        if (document.getElementById('wind') !=null){
            var wind = document.getElementById('wind').value;
            this.windows = JSON.parse(wind)
        }

    },

});


// Bulma NavBar Burger Script

