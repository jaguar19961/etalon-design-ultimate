@extends('admin.layouts.app')
@section('content')
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Editare Insertii (Sticle pentru usi)</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                {{ Form::open(array('method' => 'put','action' => 'AdminController@addNewWindow','files'=>true)) }}
                {{ csrf_field() }}

                <div class="row" style="padding-bottom: 20px">
                    <div class="col-md-6">
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Denumire</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="name" required>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Adauga</button>
                    </div>
                </div>
                {{Form::close()}}
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Denumire Sticla</th>
                        <th>Imagine Sticla</th>
                        <th>Salveaza</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($windows))
                        @foreach($windows as $item)
                            {{ Form::open(array('method' => 'put','action' => 'AdminController@addWindowImage','files'=>true)) }}
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$item->id}}" name="id">
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->name}}</td>
                                <td>
                                    @if(empty($item->image))
                                    <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                    </div>
                                        @else
                                        <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                                            <span class="fileinput-filename"></span>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                        </div>
                                        <img src="{{asset('images/catalog/'.$item->image)}}" style="max-width: 100px;">
                                    @endif
                                </td>
                                <td style="width: 20%;">

                                    <button type="submit" class="btn btn-primary">Salveaza</button>
                                </td>
                            </tr>
                            {{Form::close()}}
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection