@extends('admin.layouts.app')
@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Adaugare sistema de deschidere noua</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            {{--<li><a href="#" class="dropdown-item">Config option 1</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#" class="dropdown-item">Config option 2</a>--}}
                            {{--</li>--}}
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {{ Form::open(array('method' => 'put','action' => 'AdminController@putOpenSystem','files'=>true)) }}
                    {{ csrf_field() }}
                    <div class="row" style="padding-bottom: 20px">
                        <div class="col-md-8">
                                <div class="input-group m-b">
                                    <div class="input-group-prepend">
                                        <span class="input-group-addon">Nume</span>
                                    </div>
                                    <input type="text" placeholder="" class="form-control" name="name" required>
                                </div>
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Descriere </span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="description" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image" required/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">Adauga</button>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-md-12">
                                <h3>Content</h3>
                                <textarea class="form-control my-editor" name="content" id="content" rows="20"></textarea>
                            </div>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>



@endsection