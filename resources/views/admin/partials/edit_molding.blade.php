@extends('admin.layouts.app')
@section('content')
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Editare Furnitura</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                {{ Form::open(array('method' => 'put','action' => 'AdminController@addProductPlanePlane','files'=>true)) }}
                {{ csrf_field() }}

                <div class="row" style="padding-bottom: 20px">
                    <div class="col-md-8">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Denumire {{$localeCode}}</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="name[{{$localeCode}}]">
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-2">
                        <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Adauga</button>
                    </div>
                </div>
                {{Form::close()}}
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Denumire Furnitura</th>
                        <th>Imagine </th>
                        <th>Actiune</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($moldings))
                        @foreach($moldings as $item)
                            {{ Form::open(array('method' => 'put','action' => 'AdminController@editProductPlanePlane','files'=>true)) }}
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$item->id}}" name="id">
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <div class="input-group m-b">
                                            <div class="input-group-prepend">
                                                <span class="input-group-addon">Denumire {{$localeCode}}</span>
                                            </div>
                                            <input type="text" placeholder="" class="form-control" name="name[{{$localeCode}}]" value="@if(isset($item->langs)){{$item->langs->where('lang_id', $localeCode)->where('article_id', $item->id)->first()->name}}@else @endif">
                                        </div>
                                @endforeach
                                </td>
                                <td>

                                        <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                                            <span class="fileinput-filename"></span>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                        </div>

                                        <img src="{{asset('images/accesories/'. $item->image)}}" style="max-height: 100px">

                                </td>
                                <td style="width: 20%;">

                                        <button type="submit" class="btn btn-primary">Resetare</button>

                            </tr>
                            {{Form::close()}}
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection