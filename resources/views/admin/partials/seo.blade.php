@extends('admin.layouts.app')
@section('content')

    <div class="row">
        <div class="col-md-4">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Contacte</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {{ Form::open(array('method' => 'put','action' => 'SeoController@seoInfo','files'=>true)) }}
                    {{ csrf_field() }}
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Facebook Block</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="fb" value="{{$info->fb}}">
                    </div>
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Google Maps</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="go" value="{{$info->go}}">
                    </div>
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Nr. telefon</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="phone" value="{{$info->phone}}">
                    </div>
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Nr. telefon</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="phone1" value="{{$info->phone1}}">
                    </div>
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Adresa</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="address" value="{{$info->address}}">
                    </div>
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Ore de lucru</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="work" value="{{$info->work}}">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Email</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="email" value="{{$info->email}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Logo</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                            <img src="{{asset('images/seo/'.$info->logo)}}" style="max-width: 100px;">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Salveaza modificari</button>
                    {{Form::close()}}
                </div>

            </div>
        </div>
        @foreach($seos as $seo)
        <div class="col-md-4">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Cuvinte cheie {{$seo->parent}}</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">

                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {{ Form::open(array('method' => 'put','action' => 'SeoController@seoKey','files'=>true)) }}
                    {{ csrf_field() }}

                        <input type="hidden" name="id" value="{{$seo->id}}">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <h4>Seo {{$seo->parent}} {{$localeCode}}</h4>
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Titlu site </span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="title[{{$localeCode}}]" value="{{$seo->lang->title}}">
                    </div>
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Descriere</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="description[{{$localeCode}}]" value="{{$seo->langs->description}}">
                    </div>
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">KeyWords</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="keywords[{{$localeCode}}]" value="{{$seo->langs->keywords}}">
                    </div>
                        <hr>
                        @endforeach

                        <button type="submit" class="btn btn-primary">Salveaza modificari</button>
                    {{Form::close()}}
                </div>

            </div>
        </div>
        @endforeach



    </div>


@endsection