@extends('admin.layouts.app')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Editare serie</h2>
        </div>
        <div class="col-lg-2 p-4">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Adauga serie noua
            </button>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Adaugare serie noua</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{ Form::open(array('method' => 'put','action' => 'AdminController@addNewSerie','files'=>true)) }}
                            {{ csrf_field() }}
                            <select class="form-control m-b" name="category" required>
                                <option disabled selected>Alege Categorie</option>
                                @foreach($category as $categor)
                                    <option value="{{$categor->id}}">{{$categor->langs->name}}</option>
                                @endforeach
                            </select>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon">Denumire serie</span>
                                        </div>
                                        <input type="text" placeholder="" class="form-control" name="name"
                                               autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image" required/></span>
                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                           style="float: none">×</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">


                @foreach($category as $item)
                    <a href="{{url('admin/edit_category/serie/'.$item->id)}}"
                       class="btn btn-primary">{{$item->langs->name}}</a>
                @endforeach
                <br>
                <br>
                @if(!empty($getSeries))
                    @foreach($getSeries as $series)
                        {{--<a href="{{url('admin/category/series/'.$series->category_id. '/'.$series->id)}}"--}}
                        {{--class="btn btn-primary">{{$series->name}}</a>--}}
                    @endforeach
                @endif
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Categorie</th>
                        <th>Serie</th>
                        <th>Furnitura</th>
                        <th>Descriere</th>
                        <th>Imagine</th>
                        <th>Actiune</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $product)
                        {{ Form::open(array('method' => 'put','action' => 'AdminController@updateSerie','files'=>true)) }}
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$product->id}}">
                        <tr>
                            <td style="width: 30%">{{$product->cat->langs->name}}</td>
                            <td style="width: 10%;">
                                <div class="input-group m-b">
                                    <div class="input-group-prepend">

                                    </div>
                                    <input type="text" placeholder="" class="form-control" name="name"
                                           value="{{$product->name}}" autocomplete="off">
                                </div>
                            </td>
                            <td>

                                <select multiple class="form-control" name="furnitures[]" id="id43_{{$product->id}}">
                                    <option disabled>Alege...</option>
                                    @foreach($moldings as $type)
                                        <option value="{{$type->id}}"
                                                @foreach($product->furnitur as $value) @if($type->id == $value->furniture_id) selected="selected" @else @endif @endforeach>{{$type->lang->name}}</option>
                                    @endforeach

                                </select>
                                <script>
                                    $(document).ready(function () {
                                        $("#id43_{{$product->id}}").select2({
                                            width: '100%',
                                            closeOnSelect: false,
                                            placeholder: 'Alege furnitura',
                                        });
                                    });
                                </script>
                            </td>
                            <td>

                                <a href="#" class="btn btn-xs btn-primary" data-toggle="modal"
                                   data-target="#exampleModaldesc{{$product->id}}">Descriere</a>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModaldesc{{$product->id}}" tabindex="-1"
                                     role="dialog" aria-labelledby="exampleModalLabeldesc{{$product->id}}"
                                     aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabeldesc{{$product->id}}">
                                                    Seria {{$product->name}}</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                                    <h3>Descriere scurta Serie limba {{$localeCode}}</h3>
                                                    <textarea class="form-control my-editor"
                                                              name="content[{{$localeCode}}]" id="content"
                                                              rows="20">@if(isset($product->langs)) {{$product->langs->where('lang_id', $localeCode)->where('article_id', $product->id)->first()->name}}  @else @endif</textarea>
                                                    <h3>Descriere Vezi mai mult limba {{$localeCode}}</h3>
                                                    <textarea class="form-control my-editor"
                                                              name="content1[{{$localeCode}}]" id="content"
                                                              rows="20">@if(isset($product->langs)) {{$product->langs->where('lang_id', $localeCode)->where('article_id', $product->id)->first()->slug}}  @else @endif</textarea>
                                                    <hr>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td style="width: 10%;">
                                <img src="{{asset('images/usi-de-interior/'.$product->images)}}"
                                     style="max-width: 100px;">
                                <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Modifica</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image"/></span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                       style="float: none">×</a>
                                </div>
                            </td>
                            <td style="width: 20%">
                                <button type="submit"
                                        class="btn btn-info btn-xs">Salveaza modificari
                                </button>
                            </td>
                        </tr>
                        {{Form::close()}}
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection