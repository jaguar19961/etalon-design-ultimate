@extends('admin.layouts.app')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>3D vizualizare</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                {{ Form::open(array('method' => 'put','action' => 'AdminController@saveGallery','files'=>true)) }}
                {{ csrf_field() }}
                <input type="hidden" value="{{$cont->find(1)->id}}" name="id">
                <div class="col-md-12">
                    <h3>Content</h3>
                    <textarea class="form-control my-editor" name="content" id="content" rows="20">{{$cont->find(1)->content}}</textarea>
                </div>
                <div class="form-group p-5">
                    <button type="submit" class="btn btn-xs btn-primary">Salveaza modificari</button>
                </div>
                {{Form::close()}}

            </div>

        </div>
    </div>
    <div class="col-md-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Proiecte incheiate</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                {{ Form::open(array('method' => 'put','action' => 'AdminController@saveGallery','files'=>true)) }}
                {{ csrf_field() }}
                <input type="hidden" value="{{$cont->find(2)->id}}" name="id">
                <div class="col-md-12">
                    <h3>Content</h3>
                    <textarea class="form-control my-editor" name="content" id="content" rows="20">{{$cont->find(2)->content}}</textarea>
                </div>
                <div class="form-group p-5">
                    <button type="submit" class="btn btn-xs btn-primary">Salveaza modificari</button>
                </div>
                {{Form::close()}}
            </div>

        </div>
    </div>
    <div class="col-md-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Video</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                {{ Form::open(array('method' => 'put','action' => 'AdminController@saveGallery','files'=>true)) }}
                {{ csrf_field() }}
                <input type="hidden" value="{{$cont->find(3)->id}}" name="id">
                <div class="col-md-12">
                    <h3>Content</h3>
                    <textarea class="form-control my-editor" name="content" id="content" rows="20">{{$cont->find(3)->content}}</textarea>
                </div>
                <div class="form-group p-5">
                    <button type="submit" class="btn btn-xs btn-primary">Salveaza modificari</button>
                </div>
                {{Form::close()}}
            </div>

        </div>
    </div>
</div>


@endsection