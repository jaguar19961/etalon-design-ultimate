@extends('admin.layouts.app')
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Editare despre noi</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    {{ Form::open(array('method' => 'put','action' => 'SeoController@addSiteAbout','files'=>true)) }}
                    {{ csrf_field() }}
                    <input type="hidden" value="{{$about->id}}" name="id">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <span>Descriere {{$localeCode}}</span>
                        <textarea class="form-control my-editor" name="content[{{$localeCode}}]"
                                  id="content">{{$about->langs->where('lang_id', $localeCode)->first()->name}}</textarea>
                    @endforeach
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Salveaza</button>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Section Header</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    {{ Form::open(array('method' => 'put','action' => 'AdminController@fun1','files'=>true)) }}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-2">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine Slider</span>
    <span class="fileinput-exists">Change</span><input type="file" name="txt1"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                            <img src="{{asset('images/homePage/' .$content->find(1)->txt1)}}" alt="" style="max-height: 100px">
                        </div>
                        {{--imagine 3 --}}
                        <div class="col-md-2">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Titlu</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt2" required value="{{$content->find(1)->txt2}}">
                            </div>
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Link</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt3" required value="{{$content->find(1)->txt3}}">
                            </div>
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine Card 1</span>
    <span class="fileinput-exists">Change</span><input type="file" name="txt4"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                            <img src="{{asset('images/homePage/' .$content->find(1)->txt4)}}" alt="" style="max-height: 100px">
                        </div>
                        <div class="col-md-2">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Titlu</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt5" required value="{{$content->find(1)->txt5}}">
                            </div>
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Link</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt6" required value="{{$content->find(1)->txt6}}">
                            </div>
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine Card 2</span>
    <span class="fileinput-exists">Change</span><input type="file" name="txt7"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                            <img src="{{asset('images/homePage/' .$content->find(1)->txt7)}}" alt="" style="max-height: 100px">
                        </div>
                        <div class="col-md-2">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Titlu</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt8" required value="{{$content->find(1)->txt8}}">
                            </div>
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Link</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt9" required value="{{$content->find(1)->txt9}}">
                            </div>
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine Card 3</span>
    <span class="fileinput-exists">Change</span><input type="file" name="txt10"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                            </div>
                            <img src="{{asset('images/homePage/' .$content->find(1)->txt10)}}" alt="" style="max-height: 100px">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Salveaza</button>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Sectiunea despre companie</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    {{ Form::open(array('method' => 'put','action' => 'AdminController@fun2','files'=>true)) }}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Titlu sectiune</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt1" required value="{{$content->find(2)->txt1}}">
                        </div>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Link video</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt2" required value="{{$content->find(2)->txt2}}">
                        </div>
                        <hr>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Titlu 1</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt3" required value="{{$content->find(2)->txt3}}">
                        </div>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Descriere 1</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt4" required value="{{$content->find(2)->txt4}}">
                        </div>
                        <hr>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Titlu 2</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt5" required value="{{$content->find(2)->txt5}}">
                        </div>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Descriere 2</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt6" required value="{{$content->find(2)->txt6}}">
                        </div>
                        <hr>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Titlu 3</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt7" required value="{{$content->find(2)->txt7}}">
                        </div>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Descriere 3</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt8" required value="{{$content->find(2)->txt8}}">
                        </div>
                        <hr>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Titlu buton</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt9" required  value="{{$content->find(2)->txt9}}">
                        </div>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Link buton</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt10" required value="{{$content->find(2)->txt10}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Salveaza</button>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Sectiunea facilitati</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    {{ Form::open(array('method' => 'put','action' => 'AdminController@fun3','files'=>true)) }}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Titlu 1</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt1" required  value="{{$content->find(3)->txt1}}">
                        </div>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Descriere 1</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt2" required  value="{{$content->find(3)->txt2}}">
                        </div>
                        <hr>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Titlu 2</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt3" required  value="{{$content->find(3)->txt3}}">
                        </div>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Descriere 2</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt4" required  value="{{$content->find(3)->txt4}}">
                        </div>
                        <hr>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Titlu 3</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt5" required  value="{{$content->find(3)->txt5}}">
                        </div>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Descriere 3</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt6" required  value="{{$content->find(3)->txt6}}">
                        </div>
                        <hr>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Titlu 4</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt7" required  value="{{$content->find(3)->txt7}}">
                        </div>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Descriere 4</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt8" required  value="{{$content->find(3)->txt8}}">
                        </div>
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Salveaza</button>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Sectiunea usi premium</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    {{ Form::open(array('method' => 'put','action' => 'AdminController@fun4','files'=>true)) }}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Titlu sectiune</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt1" required value="{{$content->find(4)->txt1}}">
                        </div>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Descriere sectiune</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt2" required value="{{$content->find(4)->txt2}}">
                        </div>
                    </div>
                    <h3>Editare </h3>
                    <div class="row">
                        {{--element 1--}}
                        <div class="col-md-3">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="txt3"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                <img src="{{asset('images/homePage/' .$content->find(4)->txt3)}}" alt="" style="max-height: 100px">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Titlu</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt4" required value="{{$content->find(4)->txt4}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Descriere</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt5" value="{{$content->find(4)->txt5}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Link</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt21" value="{{$content->find(4)->txt21}}">
                            </div>
                        </div>
                        {{--elelement 2--}}
                        <div class="col-md-3">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="txt6"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                <img src="{{asset('images/homePage/' .$content->find(4)->txt6)}}" alt="" style="max-height: 100px">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Titlu</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt7" required value="{{$content->find(4)->txt7}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Descriere</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt8" value="{{$content->find(4)->txt8}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Link</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt22" value="{{$content->find(4)->txt22}}">
                            </div>
                        </div>
                        {{--elelement 2--}}
                        <div class="col-md-3">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="txt9"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                <img src="{{asset('images/homePage/' .$content->find(4)->txt9)}}" alt="" style="max-height: 100px">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Titlu</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt10" required value="{{$content->find(4)->txt10}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Descriere</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt11"  value="{{$content->find(4)->txt11}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Link</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt23" value="{{$content->find(4)->txt23}}">
                            </div>
                        </div>
                        {{--elelement 2--}}
                        <div class="col-md-3">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="txt12"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                <img src="{{asset('images/homePage/' .$content->find(4)->txt12)}}" alt="" style="max-height: 100px">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Titlu</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt13" required value="{{$content->find(4)->txt13}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Descriere</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt14" value="{{$content->find(4)->txt14}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Link</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt24" value="{{$content->find(4)->txt24}}">
                            </div>
                        </div>
                        {{--elelement 3--}}
                        <div class="col-md-3">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="txt15"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                <img src="{{asset('images/homePage/' .$content->find(4)->txt15)}}" alt="" style="max-height: 100px">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Titlu</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt16" required value="{{$content->find(4)->txt16}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Descriere</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt17"  value="{{$content->find(4)->txt17}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Link</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt25" value="{{$content->find(4)->txt25}}">
                            </div>
                        </div>
                        {{--elelement 4--}}
                        <div class="col-md-3">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="txt18"/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                <img src="{{asset('images/homePage/' .$content->find(4)->txt18)}}" alt="" style="max-height: 100px">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Titlu</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt19" required  value="{{$content->find(4)->txt19}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Descriere</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt20"  value="{{$content->find(4)->txt20}}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Link</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt26" value="{{$content->find(4)->txt26}}">
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Salveaza</button>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Sectiunea colectii</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    {{ Form::open(array('method' => 'put','action' => 'AdminController@fun5','files'=>true)) }}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Titlu</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt1" required  value="{{$content->find(5)->txt1}}">
                        </div>
                        <div class="input-group m-b">
                            <div class="input-group-prepend">
                                <span class="input-group-addon">Descriere</span>
                            </div>
                            <input type="text" placeholder="" class="form-control" name="txt2" required  value="{{$content->find(5)->txt2}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Salveaza</button>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Sectiunea Sa incepem un proiect</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    {{ Form::open(array('method' => 'put','action' => 'AdminController@fun7','files'=>true)) }}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Titlu sectiune</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt1" required  value="{{$content->find(7)->txt1}}">

                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Descriere</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt2" required  value="{{$content->find(7)->txt2}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group m-b">
                                <div class="input-group-prepend">
                                    <span class="input-group-addon">Titlu callToAction</span>
                                </div>
                                <input type="text" placeholder="" class="form-control" name="txt3" required value="{{$content->find(7)->txt3}}">

                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="txt4" required/></span>
                                <span class="fileinput-filename"></span>
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                <img src="{{asset('images/homePage/' .$content->find(7)->txt4)}}" alt="" style="max-height: 100px;">
                            </div>
                            <button type="submit" class="btn btn-primary">Salveaza modificari</button>
                        </div>


                    </div>

                    {{Form::close()}}
                </div>
            </div>
        </div>


    </div>



@endsection