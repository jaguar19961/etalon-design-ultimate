@extends('admin.layouts.app')
@section('content')
    <style>
        .product-box {
            padding: 0;
            border: 1px solid #e7eaec;
            width: 302px !important;
            height: 337px !important;
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            margin-top: 0;
            padding: 0;
            max-width: 300px !important;
        }
    </style>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Blog</h2>

        </div>
        <div class="col-lg-2">
            <div class="row" style="margin-top: 20px">
                <div class="col-md-12">
                    <a href="{{url('admin/add_blog')}}" class="btn btn-primary">Adaugare postare</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row p-t-3">
        @foreach($blogs as $blog)
            <div class="col-md-3">
                <div class="ibox">
                    <div class="ibox-content">
                        <a href="{{url('/blog/article/'.$blog->lang->slug .'/'.$blog->id)}}" class="btn-link">
                            <img src="{{asset('images/article/'.$blog->image)}}" style="max-width: 300px;
    width: 100%;
    height: 163px;">
                            <h4>
                                {{$blog->lang->name}}
                            </h4>
                        </a>
                        <div class="small m-b-xs">
                            <strong>Etalon.ro</strong> <span class="text-muted"><i
                                        class="fa fa-clock-o"></i> {{$blog->created_at->format('M d, Y')}}</span>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {{--<h5>Tags: Usi de interior, Usi</h5>--}}
                                <a href="{{url('/blog/article/'.$blog->lang->slug .'/'.$blog->id)}}"
                                   class="btn btn-primary btn-xs">Articol</a>
                                <a href="{{url('admin/edit_blog/'.$blog->id)}}"
                                   class="btn btn-primary btn-xs">Editare</a>
                                <a href="{{url('admin/delete_blog/'. $blog->id)}}"
                                   class="btn btn-danger btn-xs">Sterge</a>
                                <span class="btn btn-xs btn-yellow">Pozitia {{$blog->publicata}}</span>
                                <hr>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px">
                                @if($blog->publicata == 0)
                                <a href="{{url('admin/blog-publication/'. 1 . '/'.$blog->id)}}" class="btn btn-xs btn-success">Publica</a>
                                @else
                                <a href="{{url('admin/blog-publication/'. 0 . '/'.$blog->id)}}" class="btn btn-xs btn-info">Opreste publicarea</a>
                                    @endif
                                    @if($blog->to_menu == 0)
                                        <a href="{{url('admin/blog-toMenu/'. 1 . '/'.$blog->id)}}" class="btn btn-xs btn-success">La meniu</a>
                                    @else
                                        <a href="{{url('admin/blog-toMenu/'. 0 . '/'.$blog->id)}}" class="btn btn-xs btn-info">Scoate din meniu</a>
                                    @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>



@endsection