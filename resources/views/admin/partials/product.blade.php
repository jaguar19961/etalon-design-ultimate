@extends('admin.layouts.app')
@section('content')
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Adaugare produse</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                {{ Form::open(array('method' => 'put','action' => 'AdminController@addProduct','files'=>true)) }}
                {{ csrf_field() }}
            <div class="row">
                <div class="col-md-2">
                    <select class="form-control m-b" name="category" id="category_id" required>
                        <option disabled selected>Alege Categorie</option>
                       @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->lang->name}}</option>
                           @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <select class="form-control m-b" name="serie_id" id="subcategory_id">
                    </select>
                </div>
                <div class="col-md-2">
                    <div class="fileinput fileinput-new m-b" data-provides="fileinput">
    <span class="btn btn-default btn-file"><span class="fileinput-new">Imagine</span>
    <span class="fileinput-exists">Change</span><input type="file" name="image" required></span>
                        <span class="fileinput-filename"></span>
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                    </div>
                </div>
                <div class="col-md-3">
                    <select multiple class="form-control"  name="colors[]" id="id1_1" required>
                        <option disabled>Alege...</option>
                        @foreach($colors as $color)
                            <option value="{{$color->id}}">{{$color->name}}</option>
                        @endforeach

                    </select>
                </div>
                <div class="col-md-3">
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Denumire</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="name" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Seo Keywords</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="keywords" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group m-b">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Seo Description</span>
                        </div>
                        <input type="text" placeholder="" class="form-control" name="desc" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="stil">Stil</label>
                    <select id="stil" class="form-control m-b" name="style_id" required>
                        <option selected disabled>Alege stil usa</option>
                        @foreach($styles as $style)
                        <option value="{{$style->id}}">{{$style->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="tip">Tip usa</label>
                    <select id="tip" class="form-control m-b" name="type_id">
                        <option selected disabled>Alege tip usa</option>
                        @foreach($types as $type)
                        <option value="{{$type->id}}">{{$type->type}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="price">Pret produs</label>
                    <div class="input-group m-b" id="price">
                        <div class="input-group-prepend">
                            <span class="input-group-addon">Pret usă</span>
                        </div>
                        <input type="text" placeholder="Username" class="form-control" name="price" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <select multiple class="form-control"  name="furnitures[]" id="id1_2" required>
                        <option disabled>Alege...</option>
                        @foreach($furniture as $type)

                            <option value="{{$type->id}}">@if(!empty($type->lang)){{$type->lang->name}}@endif</option>
                        @endforeach

                    </select>
                </div>
                <div class="col-md-3">
                    <select multiple class="form-control"  name="planes[]" id="id1_3">
                        <option disabled>Alege...</option>
                        @foreach($molding as $moldings)
                            <option value="{{$moldings->id}}">{{$moldings->lang->name}}</option>
                        @endforeach

                    </select>
                </div>
                <div class="col-md-3">
                    <select multiple class="form-control"  name="windows[]" id="id1_4">
                        <option disabled>Alege...</option>
                        @foreach($windows as $window)
                            <option value="{{$window->id}}">{{$window->name}}</option>
                        @endforeach

                    </select>
                </div>


            </div>
                <div class="row" style="margin-top: 20px">


                    <div class="col-md-6">
                        <textarea class="form-control my-editor" name="content1" id="content"></textarea>
                    </div>
                    <div class="col-md-6">
                        <textarea class="form-control my-editor" name="content2" id="content"></textarea>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Adauga produs</button>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>

    <script>
        $('#category_id').on('change', function () {
            getProducts($(this).val());
        });
        function getProducts(category_id) {
            $.get("{{url('/admin/category/sub')}}/" + category_id, function (data) {
                $("#subcategory_id").html(data);
            });
        }
        $(document).ready(function () {
            getProducts($('#category_id').val());
        });
    </script>



@endsection
