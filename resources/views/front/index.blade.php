@extends('front.layouts.new')

@section('content')
    <!-- SECTIUNEA CU IMAGINE -->
    <section class="section" id="section_header">
        <img src="{{asset('images/homePage/'.$content->find(1)->txt1)}}" alt="home" class="desktop-img">
        <img src="{{asset('images/homePage/'.$content->find(1)->txt1)}}" alt="home" class="mobile-img">

        <div class="section-header-content">
            <div class="container header-content">
                {{--            <h1>Salon de uși premium</br>de interior</h1>--}}
                <h1>SALON DE UȘI DE INTERIOR</br>ȘI PARDOSELI ÎN BUCUREȘTI ȘI IAȘI</h1>
                {{--            <p>Uși moderne de interior, cea mai variată </br> gamă de modele.</p>--}}
                <p>Peste 2500 de modele de uși personalizate</br>sau de pe stoc și parchet, laminat sau vinil.</p>
                <a href="#" class="button is-primary btn-primary">MAI DETALIAT</a>
            </div>
        </div>
    </section>

    <!--END SECTIUNEA CU IMAGINE --><!-- first content row -->
    <div class="container grit-container" id="first_content">
        <div class="col">
            <div class="card card-first">
                <a title="link" href="/{{Session::get('city_id')}}/category/usi-de-interior/1">
                    <div class="card-image">
                        <figure class="image">
                            <img src="{{asset('images/homePage/'.$content->find(1)->txt4)}}" alt="Placeholder image">
                        </figure>
                    </div>
                    <div class="card-content">
                        <a title="link" href="/{{Session::get('city_id')}}/category/usi-de-interior/1">
                            <span>{{$content->find(1)->txt2}}</span>
                        </a>
                    </div>
                </a>
            </div>
        </div>
        <div class="col">
            <div class="card card-first">
                <a title="link" href="{{url('/'.Session::get('city_id') .'/sisteme-de-deschidere/afisare/pereti-glisanti/13')}}">
                    <div class="card-image">
                        <figure class="image">
                            <img src="{{asset('images/homePage/'.$content->find(1)->txt7)}}" alt="Placeholder image">
                        </figure>
                    </div>
                    <div class="card-content">
                        <a title="link" href="{{url('/'.Session::get('city_id') .'/sisteme-de-deschidere/afisare/pereti-glisanti/13')}}">
                            <span>{{$content->find(1)->txt5}}</span>
                        </a>
                    </div>
                </a>
            </div>
        </div>
        <div class="col">
            <div class="card card-first">
                <a title="link" href="/{{Session::get('city_id') . '/'}}category/parchet/3">
                    <div class="card-image">
                        <figure class="image">
                            <img src="{{asset('images/homePage/'.$content->find(1)->txt10)}}" alt="Placeholder image">
                        </figure>
                    </div>
                    <div class="card-content">
                        <a title="link" href="/{{Session::get('city_id')}}/category/parchet/3">
                            <span>{{$content->find(1)->txt8}}</span>
                        </a>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <!--Colecții uși de interior -->
    <section id="colection_intern">
        <div class="section-name">
            <h1 class="section-sub-title">{!! $content->find(5)->txt1 !!}</h1>
            <p>{{$content->find(5)->txt2}}</p>
        </div>
        <main-slider :city_name="{{json_encode(Session::get('city_id'))}}"></main-slider>
    </section>

    <!--PRODUSELE NOASTRE -->{{--<section id="our_products">--}} {{--<div class="section-name">--}} {{--<h3 class="section-title">PRODUSELE NOASTRE</h3>--}} {{--<h1 class="section-sub-title">Calitatea de <span>primă clasă</span> poate fi găsită în inima ușilor de interior și de exterior <span>ETALON</span></h1>--}} {{--<p>Căutați un magazin de uși? Calitatea este nu numai un factor important pentru dvs., ci o necesitate absolută? În acest caz, ETALON este partenerul ideal. </p>--}} {{--</div>--}} {{--<div class="section-content">--}} {{--<div class="col">--}} {{--<div class="card-category">--}} {{--<img src="/img/prod-cat/1.png" class="card-image" alt="">--}} {{--<div class="conent-card">--}} {{--<h3 class="card-title">Ușide interior</h3>--}} {{--<p>Sunt dolor dolore quis pariatur. Aliqua labore tempor esse et eiusmod eiusmod irure ex rep veniam deserunt ea aliqua. Adipisicing fugiat consectetur ipsum sit reprehen etm dolor ese incididunt do officia mollit.</p>--}} {{--</div>--}} {{--<div class="card-action">--}} {{--<a title="link" href=""><span>Vezi Produsele</span><img src="/img/arrow.png" alt=""></a>--}} {{--</div>--}} {{--</div>--}} {{--</div>--}} {{--<div class="col">--}} {{--<div class="card-category">--}} {{--<img src="/img/prod-cat/2.png" class="card-image" alt="">--}} {{--<div class="conent-card">--}} {{--<h3 class="card-title">Uși de exterior</h3>--}} {{--<p>Sunt dolor dolore quis pariatur. Aliqua labore tempor esse et eiusmod eiusmod irure ex rep veniam deserunt ea aliqua. Adipisicing fugiat consectetur ipsum sit reprehen etm dolor ese incididunt do officia mollit.</p>--}} {{--</div>--}} {{--<div class="card-action">--}} {{--<a title="link" href=""><span>Vezi Produsele</span><img src="/img/arrow.png" alt=""></a>--}} {{--</div>--}} {{--</div>--}} {{--</div>--}} {{--<div class="col">--}} {{--<div class="card-category">--}} {{--<img src="/img/prod-cat/3.png" class="card-image" alt="">--}} {{--<div class="conent-card">--}} {{--<h3 class="card-title">Parchet</h3>--}} {{--<p>Sunt dolor dolore quis pariatur. Aliqua labore tempor esse et eiusmod eiusmod irure ex rep veniam deserunt ea aliqua. Adipisicing fugiat consectetur ipsum sit reprehen etm dolor ese incididunt do officia mollit.</p>--}} {{--</div>--}} {{--<div class="card-action">--}} {{--<a title="link" href=""><span>Vezi Produsele</span><img src="/img/arrow.png" alt=""></a>--}} {{--</div>--}} {{--</div>--}} {{--</div>--}} {{--<div class="col">--}} {{--<div class="card-category">--}} {{--<img src="/img/prod-cat/4.png" class="card-image" alt="">--}} {{--<div class="conent-card">--}} {{--<h3 class="card-title">Laminat</h3>--}} {{--<p>Sunt dolor dolore quis pariatur. Aliqua labore tempor esse et eiusmod eiusmod irure ex rep veniam deserunt ea aliqua. Adipisicing fugiat consectetur ipsum sit reprehen etm dolor ese incididunt do officia mollit.</p>--}} {{--</div>--}} {{--<div class="card-action">--}} {{--<a title="link" href=""><span>Vezi Produsele</span><img src="/img/arrow.png" alt=""></a>--}} {{--</div>--}} {{--</div>--}} {{--</div>--}} {{--</div>--}}{{--</section>--}}<!--END PRODUSELE NOASTRE --><!--Uși Premium de interior -->{{--<section id="premium_dor">--}} {{--<div class="section-name">--}} {{--<h1 class="section-sub-title">{!! $content->find(4)->txt1 !!}</h1>--}} {{--<p>{{$content->find(4)->txt2}}</p>--}} {{--</div>--}} {{--<div class="section-content container">--}} {{--<div class="col">--}} {{--<div class="card-icon-bg">--}} {{--<img src="{{asset('images/homePage/'.$content->find(4)->txt3)}}" alt="" class="bg-card">--}} {{--<div class="card-content">--}} {{--<div class="icon-card">--}} {{--<img src="/img/card-icons_bg/1.png" alt="">--}} {{--</div>--}} {{--<h3 class="card-title">{!! $content->find(4)->txt4 !!}</h3>--}} {{--<p>{{$content->find(4)->txt5}}</p>--}} {{--<a title="link" href="#" class="action-card">--}} {{--<span>Mai multe</span>--}} {{--<img src="/img/arrow-white.png" alt="">--}} {{--</a>--}} {{--</div>--}} {{--</div>--}} {{--</div>--}} {{--<div class="col">--}} {{--<div class="card-icon-bg">--}} {{--<img src="{{asset('images/homePage/'.$content->find(4)->txt6)}}" alt="" class="bg-card">--}} {{--<div class="card-content">--}} {{--<div class="icon-card">--}} {{--<img src="/img/card-icons_bg/2.png" alt="">--}} {{--</div>--}} {{--<h3 class="card-title">{{$content->find(4)->txt7}}</h3>--}} {{--<p>{{$content->find(4)->txt8}}</p>--}} {{--<a title="link" href="#" class="action-card"><span>Mai multe</span><img src="/img/arrow-white.png" alt=""></a>--}} {{--</div>--}} {{--</div>--}} {{--</div>--}} {{--<div class="col">--}} {{--<div class="card-icon-bg">--}} {{--<img src="{{asset('images/homePage/'.$content->find(4)->txt9)}}" alt="" class="bg-card">--}} {{--<div class="card-content">--}} {{--<div class="icon-card">--}} {{--<img src="/img/card-icons_bg/3.png" alt="">--}} {{--</div>--}} {{--<h3 class="card-title">{{$content->find(4)->txt10}}</h3>--}} {{--<p>{{$content->find(4)->txt11}}</p>--}} {{--<a title="link" href="#" class="action-card"><span>Mai multe</span><img src="/img/arrow-white.png" alt=""></a>--}} {{--</div>--}} {{--</div>--}} {{--</div>--}} {{--<div class="col">--}} {{--<div class="card-icon-bg">--}} {{--<img src="{{asset('images/homePage/'.$content->find(4)->txt12)}}" alt="" class="bg-card">--}} {{--<div class="card-content">--}} {{--<div class="icon-card">--}} {{--<img src="/img/card-icons_bg/4.png" alt="">--}} {{--</div>--}} {{--<h3 class="card-title">{{$content->find(4)->txt13}}</h3>--}} {{--<p>{{$content->find(4)->txt14}}</p>--}} {{--<a title="link" href="#" class="action-card"><span>Mai multe</span><img src="/img/arrow-white.png" alt=""></a>--}} {{--</div>--}} {{--</div>--}} {{--</div>--}} {{--<div class="col">--}} {{--<div class="card-icon-bg">--}} {{--<img src="{{asset('images/homePage/'.$content->find(4)->txt15)}}" alt="" class="bg-card">--}} {{--<div class="card-content">--}} {{--<div class="icon-card">--}} {{--<img src="/img/card-icons_bg/5.png" alt="">--}} {{--</div>--}} {{--<h3 class="card-title">{{$content->find(4)->txt16}}</h3>--}} {{--<p>{{$content->find(4)->txt17}}</p>--}} {{--<a title="link" href="#" class="action-card"><span>Mai multe</span><img src="/img/arrow-white.png" alt=""></a>--}} {{--</div>--}} {{--</div>--}} {{--</div>--}} {{--<div class="col">--}} {{--<div class="card-icon-bg">--}} {{--<img src="{{asset('images/homePage/'.$content->find(4)->txt18)}}" alt="" class="bg-card">--}} {{--<div class="card-content">--}} {{--<div class="icon-card">--}} {{--<img src="/img/card-icons_bg/6.png" alt="">--}} {{--</div>--}} {{--<h3 class="card-title">{{$content->find(4)->txt19}}</h3>--}} {{--<p>{{$content->find(4)->txt20}}</p>--}} {{--<a title="link" href="#" class="action-card"><span>Mai multe</span><img src="/img/arrow-white.png" alt=""></a>--}} {{--</div>--}} {{--</div>--}} {{--</div>--}} {{--</div>--}}{{--</section>--}}
    <section class="open_sistem">
        <div class="section-name">
            <h1 class="section-sub-title">{!! $content->find(4)->txt1 !!}</h1>
            <p>{{$content->find(4)->txt2}}</p>
        </div>
        <div class="container section-content">

            <div class="col">
                <div class="sistem-card2">
                    <div class="sistem-img">
                        <a title="link" href="{{'/'.Session::get('city_id') . '/' .$content->find(4)->txt21}}">
                            <img src="{{asset('images/homePage/'.$content->find(4)->txt3)}}" alt="home">
                        </a>
                        <h3 class="card-title">{!! $content->find(4)->txt4 !!}</h3>
                    </div>
                    <div class="card-content">
                        <div>
                            <h3 class="card-title">{!! $content->find(4)->txt4 !!}</h3>
                            <p>{{$content->find(4)->txt5}}</p>
                        </div>
                        <a title="link" href="{{'/'.Session::get('city_id') . '/' .$content->find(4)->txt21}}">Toate modelele</a>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="sistem-card2">
                    <div class="sistem-img">
                        <a title="link" href="{{'/'.Session::get('city_id') . '/' .$content->find(4)->txt22}}">
                            <img src="{{asset('images/homePage/'.$content->find(4)->txt6)}}" alt="home">
                        </a>
                        <h3 class="card-title">{!! $content->find(4)->txt7 !!}</h3>
                    </div>
                    <div class="card-content">
                        <div>
                            <h3 class="card-title">{!! $content->find(4)->txt7 !!}</h3>
                            <p>{{$content->find(4)->txt8}}</p>
                        </div>
                        <a title="link" href="{{'/'.Session::get('city_id') . '/' .$content->find(4)->txt22}}">Toate modelele</a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="sistem-card2">
                    <div class="sistem-img">
                        <a title="link" href="{{'/'.Session::get('city_id') . '/' .$content->find(4)->txt23}}">
                            <img src="{{asset('images/homePage/'.$content->find(4)->txt9)}}" alt="home">
                        </a>
                        <h3 class="card-title">{!! $content->find(4)->txt10 !!}</h3>
                    </div>
                    <div class="card-content">
                        <div>
                            <h3 class="card-title">{!! $content->find(4)->txt10 !!}</h3>
                            <p>{{$content->find(4)->txt11}}</p>
                        </div>
                        <a title="link" href="{{'/'.Session::get('city_id') . '/' .$content->find(4)->txt23}}">Toate modelele</a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="sistem-card2">
                    <div class="sistem-img">
                        <a title="link" href="{{'/'.Session::get('city_id') . '/' .$content->find(4)->txt24}}">
                            <img src="{{asset('images/homePage/'.$content->find(4)->txt12)}}" alt="">
                        </a>
                        <h3 class="card-title">{!! $content->find(4)->txt13 !!}</h3>
                    </div>
                    <div class="card-content">
                        <div>
                            <h3 class="card-title">{!! $content->find(4)->txt13 !!}</h3>
                            <p>{{$content->find(4)->txt14}}</p>
                        </div>
                        <a title="link" href="{{'/'.Session::get('city_id') . '/' .$content->find(4)->txt24}}">Toate modelele</a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="sistem-card2">
                    <div class="sistem-img">
                        <a title="link" href="{{'/'.Session::get('city_id') . '/' .$content->find(4)->txt25}}">
                            <img src="{{asset('images/homePage/'.$content->find(4)->txt15)}}" alt="home">
                        </a>
                        <h3 class="card-title">{!! $content->find(4)->txt16 !!}</h3>
                    </div>
                    <div class="card-content">
                        <div>
                            <h3 class="card-title">{!! $content->find(4)->txt16 !!}</h3>
                            <p>{{$content->find(4)->txt17}}</p>
                        </div>
                        <a title="link" href="{{'/'.Session::get('city_id') . '/' .$content->find(4)->txt25}}">Toate modelele</a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="sistem-card2">
                    <div class="sistem-img">
                        <a title="link" href="{{'/'.Session::get('city_id') . '/' .$content->find(4)->txt26}}">
                            <img src="{{asset('images/homePage/'.$content->find(4)->txt18)}}" alt="home">
                        </a>
                        <h3 class="card-title">{!! $content->find(4)->txt19 !!}</h3>
                    </div>
                    <div class="card-content">
                        <div>
                            <h3 class="card-title">{!! $content->find(4)->txt19 !!}</h3>
                            <p>{{$content->find(4)->txt20}}</p>
                        </div>
                        <a title="link" href="{{'/'.Session::get('city_id') . '/' .$content->find(4)->txt26}}">Toate modelele</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--END Uși Premium de interior -->

    <!--END DESPRE ETALON --><!--FACILITATI -->
    <section id="facility">
        <div class="container facility">
            <div class="col">
                <div class="card-icon">
                    <div class="icon"><img src="/img/card-icons/1.png" alt="card-icons"></div>
                    <h3 class="title-card">{{$content->find(3)->txt1}}</h3> {{--<p>{{$content->find(3)->txt2}}</p>--}}
                </div>
            </div>
            <div class="col">
                <div class="card-icon">
                    <div class="icon"><img src="/img/card-icons/2.png" alt="card-icons"></div>
                    <h3 class="title-card">{{$content->find(3)->txt3}}</h3> {{--<p>{{$content->find(3)->txt4}}</p>--}}
                </div>
            </div>
            <div class="col">
                <div class="card-icon">
                    <div class="icon"><img src="/img/card-icons/3.png" alt="card-icons"></div>
                    <h3 class="title-card">{{$content->find(3)->txt5}}</h3> {{--<p>{{$content->find(3)->txt6}}</p>--}}
                </div>
            </div>
            <div class="col b-none">
                <div class="card-icon">
                    <div class="icon"><img src="/img/card-icons/4.png" alt="card-icons"></div>
                    <h3 class="title-card">{{$content->find(3)->txt7}}</h3>
                    <p> {{--{{$content->find(3)->txt1}} Testează-ne acum: <br>--}} <a title="link" href="">+40 (755) 476
                            598</a></p></div>
            </div>
        </div>
    </section>
    <!--END FACILITATI -->

    <!--Cum să ne contactezi -->
    <section id="contact">
        <div class="container section-content">
            <div class="col">
                <div class="form_card">
                    <h3 class="form-title">Colaborăm cu designeri și arhitecți</h3>
                    <p>Completează formularul iar noi te vom contacta pentru a stabili detaliile colaborării
                        noastre</p>
                    {{ Form::open(array('method' => 'put','action' => 'FrontController@getColaboration')) }}
                    {{ csrf_field() }}
                    <div class="columns">
                        <div class="column"><input class="input" type="text" placeholder="Nume" name="nume" required>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column pr-9"><input class="input" type="tel" placeholder="Telefon" name="telefon"
                                                        required></div>
                        <div class="column pl-9"><input class="input" type="email" placeholder="Email" name="email"
                                                        required></div>
                    </div>
                    <div class="columns">
                        <div class="column"><input class="input" type="text" placeholder="Subiect" name="subiect"
                                                   required></div>
                    </div>
                    <div class="columns">
                        <div class="column"><textarea class="textarea" placeholder="Mesaj" name="mesaj"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="button is-primary btn-primary">Trimite</button>
                    {{Form::close()}}
                </div>
            </div>
            <div class="col">
                <div class="contact-content">
                    <div class="section-name section-name-50 d-flex-vertical-align-center">
                        <h1 class="section-sub-title">Cum să ne <span>contactezi</span></h1>
                        <p>Pentru detalii despre produse, oferte sau alte întrebări, vă invităm să ne vizitați pe adresa
                            indicată mai jos sau să folosești datele de contact:</p>
                    </div>

                    <ul class="contact-list">
                        <li>
                            <div class="contact-icon">
                                <img src="/img/contact_icons/1.png" alt="contact">
                            </div>
                            <div>
                                <span class="contact-name">Adresa</span>
                                <span class="contact-sub">{{$infos->address}}</span>
                                <span class="contact-sub">Iași, Strada nr. 39 Bl. 508</span>
                                <a title="link" href="{{$infos->go}}">Găsește-ne pe hartă</a>
                            </div>
                        </li>
                        <li>
                            <div class="contact-icon">
                                <img src="/img/contact_icons/2.png" alt="contact">
                            </div>
                            <div>
                                <span class="contact-name">Numere de telefon</span>
                                <a title="link" href="tel:{{$infos->phone}}">București: {{$infos->phone}}</a><br>
                                <a title="link" href="tel:{{$infos->phone1}}">{{$infos->phone1}}</a>
                                <a title="link" href="tel:+40 (752) 660 224">Iași: +40 (752) 660 224</a>
                            </div>
                        </li>
                        <li>
                            <div class="contact-icon">
                                <img src="/img/contact_icons/3.png" alt="contact">
                            </div>
                            <div>
                                <span class="contact-name">E-mail</span>
                                <a title="link" href="email:{{$infos->email}}">{{$infos->email}}</a><br>
                                <a title="link" href="email:iasi@etalon.ro">iasi@


                                    .ro</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section><!--END Cum să ne contactezi -->

    <!--END Colecții uși de interior -->

    <!--Sisteme de deschidere-->
    <section class="open_sistem">
        <div class="section-name">
            <h1 class="section-sub-title"><span>Sisteme de deschidere </span> moderne</h1>
        </div>
        <div class="container section-content">
            @foreach($openSystem as $value)
                <div class="col">
                    <div class="sistem-card">
                        <div class="sistem-img">
                            <a title="link"
                               href="{{url('/'.Session::get('city_id') .'/sisteme-de-deschidere/afisare/'.$value->slug. '/' .$value->id)}}">
                                <img src="{{asset('images/systems/'.$value->image)}}" alt="systems">
                            </a>
                        </div>
                        <div class="card-content">
                            <h3 class="card-title">{{$value->name}}</h3>
                            <a title="link"
                               href="{{url('/'.Session::get('city_id') .'/sisteme-de-deschidere/afisare/'.$value->slug. '/' .$value->id)}}">
                                CITEȘTE MAI MULT
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <!--END Sisteme de deschidere-->

    <!--Suntem gata -->
{{--    <section id="are_ready">--}}
{{--        <div class="container section-content">--}}
{{--            <div class="col">--}}
{{--                <div class="section-name section-name-50 d-flex-vertical-align-center h-100">--}}
{{--                    <h1 class="section-sub-title">{!! $content->find(7)->txt1 !!}</h1>--}}
{{--                    <p class="mb-48">{{$content->find(7)->txt2}}</p>--}}

{{--                    <h3 class="contact-me-now">Contactează-ne pentru a beneficia <br> de o ofertă personalizată</h3>--}}
{{--                    <div class="text-right">--}}
{{--                        <a itle="link" href="#contact" class="btn-line">Cere o Ofertă</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col">--}}
{{--                <img src="{{asset('images/homePage/' .$content->find(7)->txt4)}}" alt="home">--}}
{{--            </div>--}}
{{--        </div>--}}

{{--    </section>--}}
    <!--END Suntem gata -->

    <!--END first content row --><!--DESPRE ETALON -->
    <div class="container" id="about_etalon">
        <div class="col">
            <h3 class="section-title">DESPRE ETALON DOORS</h3>
            <h1 class="section-sub-title">{!! $content->find(2)->txt1 !!}</h1>
            <ul>
                <li>
                    <span>{{$content->find(2)->txt3}}</span>
                    <p>{{$content->find(2)->txt4}}</p>
                </li>
                <li>
                    <span>{{$content->find(2)->txt5}}</span>
                    <p>{{$content->find(2)->txt6}}</p>
                </li>
                <li>
                    <span>{{$content->find(2)->txt7}}</span>
                    <p>{{$content->find(2)->txt8}}</p>
                </li>
            </ul>
            <a title="link" class="button is-primary btn-primary">{{$content->find(2)->txt9}}</a>
        </div>
        <div class="col dors_about">
            <img src="/img/dors_about/3.png" alt="about" class="img_about_dors">
            <a title="link" href="#" class="rounted-button pulse" id="showVideo"
               data-video-id="{{$content->find(2)->txt2}}">
                <img src="/img/triangle.png" alt="triangle">
            </a>
        </div>
    </div>


    <div class="container" id="articles_last">

        <div class="section-subtitle">
            <h1>Ultimele <span>Articole din Blog</span></h1>
            <a title="link" href="{{url('/'.Session::get('city_id') . '/blog')}}" class="button is-primary btn-primary">Mai Multe</a>
        </div>

        <div class="last-articles">
            <div class="sw-btn-prev" slot="button-prev"></div>
            <swiper :options="swiperOptionBlog">
                @foreach($blog as $item)
                    <swiper-slide>
                        <div class="article-card2">
                            <div class="article-img">
                                <img src="{{asset('/images/article/'. $item->image)}}" alt="article">
                            </div>
                            <div class="content-card">
                                <div class="data-article">
                                    <span>Mariana Melnic</span>
                                    <span>{{$item->created_at->format('d.m.Y')}}</span>
                                </div>
                                <a title="link"
                                   href="{{url('/'.Session::get('city_id') .'/blog/article/'.$item->lang->slug .'/'.$item->id)}}">{{$item->lang->name}}</a>
                            </div>
                        </div>
                    </swiper-slide>
                @endforeach
            </swiper>
            <div class="sw-btn-next" slot="button-next"></div>
        </div>
    </div>
    <!--END Ultimele Articole -->

@endsection
