@extends('front.layouts.new-white-nav')
@section('content')
    @if(!empty($getProduct))
        @if($getProduct->category_id == 3 || $getProduct->category_id == 4)

            <section id="parket_page">
                <div class="container section-content">
                    <div class="parket-section-description">
                        <h1 class="title">{{$getProduct->name}}</h1>
                        <div class="price-buy">
{{--                            <span>Preț de la: <strong>{{$getProduct->price}}</strong> Lei</span>--}}
                            <span>Cod SKU: {{$getProduct->name}}</span>
                        </div>
                        <p>{!! $getProduct->desc !!}</p>
                        <div class="price-buy parchet-me">
                            <button data-target="simpleModal_3" data-toggle="modal"
                                    class="button is-primary btn-primary">
                                Solictă oferta
                            </button>
                        </div>
                        <div id="simpleModal_3" class="modal">
                            <div class="modal-window small me-modal">
                                <span>Solicită oferta</span>
                                {{ Form::open(array('method' => 'put','action' => 'FrontController@getOffer')) }}
                                {{ csrf_field() }}
                                <input type="hidden" name="door_name" value="{{$getProduct->name}}">
                                <input name="name" type="text" placeholder="Nume..." class="futer-input" required>
                                <input name="phone" type="phone" placeholder="Nr. de telefon..." class="futer-input"
                                       required>
                                <input name="door_numbers" type="text" placeholder="Numărul de parchet..."
                                       class="futer-input" required>
                                <button type="submit" class="button is-primary is-rounded me-button-mult">Transmite
                                    solicitarea
                                </button>
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                    <div class="parket-section-img">
                        <div class="product_img">
                            @if (!empty($getProduct->image))
                                <img src="{{asset('images/catalog/'. $getProduct->image)}}" alt="Door">
                            @else
                                <img src="{{asset('images/catalog/')}}" alt="Door">
                            @endif
                        </div>
                    </div>
                    <div class="parket-section-aditional">
                        <div class="sidenav border-none">
                            <h3 class="sidenav-section-title border-right">INFORMAȚII ADIȚIONALE</h3>
                            {!! $getProduct->description !!}
                        </div>
                    </div>
                </div>
            </section>

            <!--Colecții uși de interior -->
            <section id="colection_intern" style="background: #ffffff;">
                <div class="section-name">
                    <h1 class="section-sub-title"><span>produse similare</span></h1>
                </div>
                <div class="container slider-colection">
                    <div class="sw-button-prev" slot="button-prev"></div>
                    <swiper :options="swiperOption">
                        @foreach($showProductSeries as $product1)
                            <swiper-slide>
                                <a title="link"
                                   href="{{url('/'.Session::get('city_id') . '/product/'. str_slug($product1->name) . '/' . $product1->id)}}">
                                    <div class="slider_image">
                                        <img src="{{asset('images/catalog/'.$product1->image)}}" alt="catalog">
                                        <div class="slide-price">{{$product1->name}} </div>
{{--                                        <div class="price" style="text-align: -webkit-center !important;">--}}
{{--                                            Pret: {{$product1->price}} Lei--}}
{{--                                        </div>--}}
                                    </div>
                                </a>
                            </swiper-slide>
                        @endforeach
                    </swiper>
                    <div class="sw-button-next" slot="button-next"></div>
                </div>
            </section>
            <!--END Colecții uși de interior -->

        @endif
    @endif
    @if(empty($getProduct))
        <product-page-new :city_name="{{json_encode(Session::get('city_id'))}}" :model_id="{{json_encode((int)$modelId)}}"></product-page-new>
    @endif
@endsection
