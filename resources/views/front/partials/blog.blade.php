@extends('front.layouts.new-white-nav')
@section('content')
    <!--Ultimele Articole -->
    <section id="blog_page">
        <div class="section-name page-title">
            <h1 class="section-sub-title"><span>NOUTĂȚI</span></h1>
        </div>
        <div class="container section-content">
            @foreach($blogs as $item)
                <div class="col">
                    <div class="article-card2">
                        <div class="article-img">
                            <img src="{{asset('/images/article/'. $item->image)}}" alt="article">
                        </div>
                        <div class="content-card">
                            <div class="data-article">
                                <span>Mariana Melnic</span>
                                <span>{{$item->created_at->format('d.m.Y')}}</span>
                            </div>
                            <a title="link" href="{{url(Session::get('city_id') . '/blog/article/'.$item->lang->slug .'/'.$item->id)}}">{{$item->lang->name}}</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <!--END Ultimele Articole -->
@endsection
