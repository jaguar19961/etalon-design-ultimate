@extends('front.layouts.new-white-nav')
@section('content')
    <section id="articles" style="margin-top: 50px; margin-bottom: 50px; padding: 20px;">
        <div class="mb4 has-text-centered" style="margin-bottom: 50px;">
            <h2 class="title is-2">Îți mulțumim pentru interesul manifestat.</h2>
            <span>In scurt timp un angajat etalon.ro va lua legatura cu dmv.</span>
        </div>
        <div class="container" >
            <div class="columns" style="border: 2px solid #d62f2f; padding: 7px;">
{{--                <iframe style="height: 600px!important;" width="100%" height="600" src="https://www.youtube.com/embed/1nDJERhStjo?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--}}

                <video width="80%" height="500" autoplay>
                    <source src="{{asset('/videos/alex1.mp4')}}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            </div>

        </div>
    </section>

    @endsection