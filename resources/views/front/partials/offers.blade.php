@extends('front.layouts.new-white-nav')
@section('content')
    <!--Ultimele Articole -->
    <section id="offers_page">
        <div class="section-name page-title">
            <h1 class="section-sub-title"><span>OFERTE</span></h1>
        </div>
        <div class="container section-content">
            @if($offers->count() > 0)
                @foreach($offers as $offer)
                <div class="col">
                    <div class="sistem-card">
                        <div class="sistem-img">
                            <a title="link" href="{{url(Session::get('city_id') . '/offers/article/'.$offer->lang->slug .'/'.$offer->id)}}">
                                <img src="{{asset('/images/article/'.$offer->image)}}" alt="systems">
                            </a>
                        </div>
                        <div class="card-content">
                            <h3 class="card-title">{{$offer->lang->name}}</h3>
                            <a title="link" href="{{url(Session::get('city_id') . '/offers/article/'.$offer->lang->slug .'/'.$offer->id)}}">
                                CITEȘTE MAI MULT
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            @else
                <div class="col">
                    <h2>La moment nu avem nici o ofertă</h2>
                </div>
            @endif
        </div>
    </section>
    <!--END Ultimele Articole -->
@endsection
