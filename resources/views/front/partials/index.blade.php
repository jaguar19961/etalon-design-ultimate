@extends('front.layouts.app')

@section('front')



    <div class="swiper-container s1">

        <div class="swiper-wrapper">

            @foreach($banners as $banner)



                    <div

                            class="swiper-slide"

                            style="background-image: url('{{asset('images/scroll/'.$banner->image)}}'); background-size: cover; background-size: 100%; background-repeat: no-repeat;">

                        <div class="sor">
                            <h1> {{$banner->name}}</h1>
                        </div>


                        <div class="control-play">

                            <div class="control-text">

                                <p><a href="{{$banner->video_link}}" style="color: white">Află detalii <i class="fa fa-angle-right"></i></a></p>

                            </div>

                        </div>



                    </div>



            @endforeach



        </div>


        <style>
            .desc1{
                display: flex;
                text-align: -webkit-center;
                margin: 20px;
                margin-top: 67px;
            }

            .desc1 p{
                color: #000;
            }
            .sor{

                position: absolute;
                left: 5%;
                top: 0px;
                width: 487px;
                height: 100%;
                background-color: #00000094;
            }

            .sor h1{

                padding: 111px 5px 5px 31px;
                position: absolute;
                color: #ffffff;
                font-size: 38px;
                font-family: "Gotham-Pro-Medium", sans-serif;
                line-height: 50px;
                letter-spacing: 1.2px;
                top: 104px;
            }

            @media only screen and (max-width: 600px) {

                .desc1{
                    display: block;
                    text-align: -webkit-center;
                }

                .desc1 p{
                    color: #000;
                    margin: 20px;
                }

                .sor{

                    position: absolute;
                    left: 11%;
                    top: 64px;
                    width: 290px;
                    height: 44%;
                    background-color: #000000b8;
                }

                .sor h1{

                    padding: 111px 5px 5px 31px;
                    position: absolute;
                    color: #ffffff;
                    font-size: 15px;
                    font-family: "Gotham-Pro-Medium", sans-serif;
                    line-height: 27px;
                    letter-spacing: 1.2px;
                    top: -102px;
                    text-align: -webkit-center;
                }

                /*.control-play {*/
                    /*display: flex;*/
                    /*z-index: 1;*/
                    /*height: 55px;*/
                    /*width: 268px;*/
                    /*background: rgba(137, 109, 72, 0.84);*/
                    /*border: 2px solid #fff;*/
                    /*border-top-right-radius: 85px;*/
                    /*border-bottom-right-radius: 85px;*/
                    /*position: absolute;*/
                    /*left: 73%;*/
                    /*top: -23%;*/
                    /*transform: translate(50%, 150%);*/
                /*}*/

                .control-play{
                    display: none;
                }

                .swiper-button-next{
                    display: none;
                }

                .swiper-button-prev{
                    display: none;
                }

                .swiper-wrapper{
                    height: 300% !important;
                }
            }
        </style>
        <!-- Add Pagination -->

        <div class="swiper-pagination s1"></div>

        <!-- Add Arrows -->

        <div class="swiper-button-next s1"></div>

        <div class="swiper-button-prev s1"></div>





    </div>

    </header>



















    <script>

        $(document).ready(function () {



            $('#flip-countdown .countdown-dot').css({

                'background-color': sBackColor

            });



            $('#flip-countdown .countdown-number-top').css({

                'background-color': sBackColor

            });



            $('#flip-countdown .countdown-number-bottom').css({

                'background-color': sBackColor

            });



            $('#flip-countdown .countdown-number-next').css({

                'background-color': sBackColor

            });



            $('#flip-countdown .countdown-number-next').css({

                'color': sTextColor

            });



            $('#flip-countdown .countdown-number-top').css({

                'color': sTextColor

            });



            $('#flip-countdown .countdown-number-bottom').css({

                'color': sTextColor

            });



            $('#flip-countdown .countdown-dot').css({

                'border-color': sBorderColor

            });



            $('#flip-countdown .countdown-number-top').css({

                'border-color': sBorderColor

            });



            $('#flip-countdown .countdown-number-bottom').css({

                'border-color': sBorderColor

            });



            $('#flip-countdown .countdown-number-next').css({

                'border-color': sBorderColor

            });



            $('#flip-countdown .countdown-label-container').css({

                'color': sLabelColor

            });



            var days = 24 * 60 * 60,

                hours = 60 * 60,

                minutes = 60;



            var left, d, h, m, s, positions;



            toDate = new Date(sToDate);



            (function tick() {



                // Time left

                left = Math.floor((toDate - (new Date())) / 1000);



                if (left < 0) {

                    left = 0;

                }



                // days left

                d = Math.floor(left / days);

                updateNumbers(1, 2, d, 0);

                left -= d * days;



                // hours left

                h = Math.floor(left / hours);

                updateNumbers(3, 4, h, 999);

                left -= h * hours;



                // minutes left

                m = Math.floor(left / minutes);

                updateNumbers(5, 6, m, 999);

                left -= m * minutes;



                // seconds left

                s = left;

                updateNumbers(7, 8, s, 999);





                // Scheduling another call of this function in 1s

                setTimeout(tick, 1000);

            })();



            function updateNumbers(minor, major, value, forDays) {



                if (forDays == 0) {

                    var forDaysClass = '.position-' + parseInt(forDays);

                    switchDigit(forDaysClass, Math.floor(value / 100));

                }



                var minorClass = '.position-' + parseInt(minor);

                var majorClass = '.position-' + parseInt(major);



                switchDigit(minorClass, Math.floor(value / 10) % 10);

                switchDigit(majorClass, value % 10);



            }



            function switchDigit(sPosition, iNumber) {



                var oDigit = $(sPosition);

                var oTarget1 = oDigit.parents('.countdown-number-top');

                var iNextNumber = iNumber - 1;

                var sNextPosition = sPosition + '-next';



                if (oDigit.is(':animated') || $(oDigit).html() == iNumber || oTarget1.is(':animated')) {

                    return false;

                }



                if (((sPosition == '.position-0' || sPosition == '.position-1' || sPosition == '.position-2' || sPosition == '.position-4' || sPosition == '.position-6' || sPosition == '.position-8') && iNextNumber < 0)) {

                    iNextNumber = 9;

                } else if ((sPosition == '.position-3' && iNextNumber < 0)) {

                    iNextNumber = 2;

                } else if ((sPosition == '.position-5' || sPosition == '.position-7') && iNextNumber < 0) {

                    iNextNumber = 5;

                }



                $(oTarget1).animate({borderSpacing: -90}, {

                    step: function (now, fx) {

                        $(this).css('-webkit-transform', 'rotateX(' + now + 'deg)');

                        $(this).css('-moz-transform', 'rotateX(' + now + 'deg)');

                        $(this).css('transform', 'rotateX(' + now + 'deg)');

                    },

                    duration: 750,

                    complete: function () {

                        $(sPosition).each(function () {

                            $(this).html(iNumber);

                        });

                        $(sNextPosition).each(function () {

                            $(this).html(iNextNumber);

                        });



                        $(this).css('-webkit-transform', '');

                        $(this).css('-moz-transform', '');

                        $(this).css('transform', '');

                    }

                });



                $('.countdown-number-top .countdown-number-inner ' + sPosition).animate({borderSpacing: -90}, {

                    step: function (now, fx) {

                        $(this).css('-webkit-transform', 'rotateX(' + now + 'deg)');

                        $(this).css('-moz-transform', 'rotateX(' + now + 'deg)');

                        $(this).css('transform', 'rotateX(' + now + 'deg)');

                    },

                    duration: 750,

                    complete: function () {

                        $(this).css('-webkit-transform', 'rotateX(180deg)');

                        $(this).css('-moz-transform', 'rotateX(180deg)');

                        $(this).css('transform', 'rotateX(180deg)');

                    }

                });

            }



            resizeClockFonts();



            $(window).resize(function () {

                resizeClockFonts();

            });



            // resize fonts based on container width

            function resizeClockFonts() {



                var numContainerHeight = $('#flip-countdown .countdown-number-container').height();

                var labelContainerHeight = $('#flip-countdown .countdown-label-container').height();



                var numFontSize = parseInt(numContainerHeight * .9) + 'px';

                var labelFontsize = parseInt(labelContainerHeight * .9) + 'px';



                $("#flip-countdown .countdown-number-inner").css({

                    'font-size': numFontSize,

                    'line-height': parseInt(numContainerHeight) + 'px'

                });



                $('#flip-countdown .countdown-number-next').css({

                    'font-size': numFontSize,

                    'line-height': parseInt(numContainerHeight) + 'px'

                });



                $("#flip-countdown .countdown-label-container").css({

                    'font-size': labelFontsize,

                    'line-height': parseInt(labelContainerHeight) + 'px'

                });



            }

        });



    </script>



    @if($getOffers->activity == 1)

        <section id="offers-timer">

            <div class="container">

                <h2 class="title is-2">{{$getOffers->name}}</h2>

                <p>{!! $getOffers->description !!}

                </p>

                <div style="width: 800px; margin:auto;">

                    <!-- #flip-countdown is always 100% responsive to it's container-->



                    <div id="flip-countdown">

                        <div class="countdown-wrapper">

                            <div class="countdown-main">

                                <div class="countdown-section-days days">

                                    <div class="countdown-number-container">

                                        <div class="countdown-number-days">

                                            <div class="countdown-number-next position-0-next">0</div>

                                            <div class="countdown-number-top">

                                                <div class="shadow">

                                                    <div class="countdown-number-inner position-0">0</div>

                                                </div>

                                            </div>

                                            <div class="countdown-number-bottom">

                                                <div class="countdown-number-inner position-0">0</div>

                                            </div>

                                        </div>

                                        <div class="countdown-number-days">

                                            <div class="countdown-number-next position-1-next">0</div>

                                            <div class="countdown-number-top">

                                                <div class="shadow">

                                                    <div class="countdown-number-inner position-1">0</div>

                                                </div>

                                            </div>

                                            <div class="countdown-number-bottom">

                                                <div class="countdown-number-inner position-1">0</div>

                                            </div>

                                        </div>

                                        <div class="countdown-number-days">

                                            <div class="countdown-number-next position-2-next">0</div>

                                            <div class="countdown-number-top">

                                                <div class="shadow">

                                                    <div class="countdown-number-inner position-2">0</div>

                                                </div>

                                            </div>

                                            <div class="countdown-number-bottom">

                                                <div class="countdown-number-inner position-2">0</div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="countdown-label-container">Zile</div>

                                </div>

                                <div class="countdown-separator">

                                    <div class="countdown-separator-top">

                                        <div class="countdown-dot"></div>

                                        <div class="countdown-dot"></div>

                                    </div>

                                </div>

                                <div class="countdown-section-other hours">

                                    <div class="countdown-number-container">

                                        <div class="countdown-number-other">

                                            <div class="countdown-number-next position-3-next">0</div>

                                            <div class="countdown-number-top">

                                                <div class="shadow">

                                                    <div class="countdown-number-inner position-3">0</div>

                                                </div>

                                            </div>

                                            <div class="countdown-number-bottom">

                                                <div class="countdown-number-inner position-3">0</div>

                                            </div>

                                        </div>

                                        <div class="countdown-number-other">

                                            <div class="countdown-number-next position-4-next">0</div>

                                            <div class="countdown-number-top">

                                                <div class="shadow">

                                                    <div class="countdown-number-inner position-4">0</div>

                                                </div>

                                            </div>

                                            <div class="countdown-number-bottom">

                                                <div class="countdown-number-inner position-4">0</div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="countdown-label-container">Ore</div>

                                </div>

                                <div class="countdown-separator">

                                    <div class="countdown-separator-top">

                                        <div class="countdown-dot"></div>

                                        <div class="countdown-dot"></div>

                                    </div>

                                </div>

                                <div class="countdown-section-other minutes">

                                    <div class="countdown-number-container">

                                        <div class="countdown-number-other">

                                            <div class="countdown-number-next position-5-next">0</div>

                                            <div class="countdown-number-top">

                                                <div class="shadow">

                                                    <div class="countdown-number-inner position-5">0</div>

                                                </div>

                                            </div>

                                            <div class="countdown-number-bottom">

                                                <div class="countdown-number-inner position-5">0</div>

                                            </div>

                                        </div>

                                        <div class="countdown-number-other">

                                            <div class="countdown-number-next position-6-next">0</div>

                                            <div class="countdown-number-top">

                                                <div class="shadow">

                                                    <div class="countdown-number-inner position-6">0</div>

                                                </div>

                                            </div>

                                            <div class="countdown-number-bottom">

                                                <div class="countdown-number-inner position-6">0</div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="countdown-label-container">Minute</div>

                                </div>

                                <div class="countdown-separator">

                                    <div class="countdown-separator-top">

                                        <div class="countdown-dot"></div>

                                        <div class="countdown-dot"></div>

                                    </div>

                                </div>

                                <div class="countdown-section-other seconds">

                                    <div class="countdown-number-container">

                                        <div class="countdown-number-other">

                                            <div class="countdown-number-next position-7-next">0</div>

                                            <div class="countdown-number-top">

                                                <div class="shadow">

                                                    <div class="countdown-number-inner position-7">0</div>

                                                </div>

                                            </div>

                                            <div class="countdown-number-bottom">

                                                <div class="countdown-number-inner position-7">0</div>

                                            </div>

                                        </div>

                                        <div class="countdown-number-other">

                                            <div class="countdown-number-next position-8-next">0</div>

                                            <div class="countdown-number-top">

                                                <div class="shadow">

                                                    <div class="countdown-number-inner position-8">0</div>

                                                </div>

                                            </div>

                                            <div class="countdown-number-bottom">

                                                <div class="countdown-number-inner position-8">0</div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="countdown-label-container">Secunde</div>

                                </div>

                            </div>

                        </div>

                    </div>



                </div>



                <script>

                    var sTextColor = '#FFFFFF';

                    var sBackColor = '#404040';

                    var sLabelColor = '#000000';

                    var sToDate = '{{$getOffers->valability}}';

                    var sBorderColor = '#000000';

                </script>

                <a href="{{$getOffers->link}}" class="button btn-yellow is-rounded">Vezi oferta</a>

            </div>

        </section>

    @endif

    <div class="container">
        <div class="desc1">
            <p><i class="fas fa-check"></i>Usi de interior, cea mai vasta paleta - peste 2500 de modele de usi. </p>
            <p><i class="fas fa-check"></i>Usi culisante, usi care se rotesc, usi pliante sau glisante.  </p>
            <p><i class="fas fa-check"></i>Usi de dimensiuni atipice, cu balamale ascunse sau deschideri alternative.</p>
            <p><i class="fas fa-check"></i>Usi de interior pentru pretentiosi. </p>
        </div>
    </div>

    <section id="benefits">

        <div class="container">

            <div class="columns is-multiline is-mobile">

                <div class="column benefit-item is-5-mobile">

                    <img src="{{asset('img/benefits-icons/favorites.png')}}" alt="favorites">

                    <p>Garantăm calitatea</p>

                </div>

                <div class="column benefit-item is-5-mobile">

                    <img src="{{asset('img/benefits-icons/sortiment.png')}}" alt="sortiment bogat">

                    <p>Sortiment bogat</p>

                </div>

                <div class="column benefit-item is-5-mobile">

                    <img src="{{asset('img/benefits-icons/livrare.png')}}" alt="livrare">

                    <p>Livrare gratuită în Bucuresti</p>

                </div>

                <div class="column benefit-item is-5-mobile">

                    <img src="{{asset('img/benefits-icons/autorizati.png')}}" alt="autorizati">

                    <p>Montatori autorizați</p>

                </div>

            </div>

        </div>

    </section>



    <section class="gr-background" id="catalog-categorii">

        <div class="container">

            <h3 class="title is-3">Catalog produse</h3>

            <!-- <div class="columns is-multiline">

                @foreach($catalogOffer as $item)

                <div class="column is-one-second catalog-item">

                    <a href="{{$item->link}}">

                    <img src="{{asset('images/catalog-offer/'.$item->image)}}" alt="example">

                    </a>

                    <span>{{$item->name}}</span>

                </div>

                @endforeach



            </div> -->
            <div class="columns">
                <div class="column is-two-thirds">
                    <div class="catalog-item fist-item">

                        <a href="{{$cat1->link}}">

                        <img @if(!empty($cat1->image)) class="catalog-img" @endif src="{{asset('images/catalog-offer/'.$cat1->image)}}" alt="example">

                        <span>{{$cat1->name}}</span>

                        </a>
                    </div>

                    {{--<div class="swiper-container s4">--}}

                        {{--<div class="swiper-wrapper">--}}

                            {{--@foreach($products as $product1)--}}

                                {{--<div class="swiper-slide">--}}

                                    {{--<a href="{{url('/category/serie/'. $product1->name . '/' . $product1->id)}}">--}}



                                        {{--<img class="catalog-img" src="{{asset('images/usi-de-interior/'.$product1->images)}}" alt="door">--}}

                                    {{--</a>--}}

                                    {{--<div class="slide-title">--}}

                                    {{--{{$product->name}}{{$product->serie->name}}--}}

                                    {{--</div>--}}

                                    {{--<div class="slide-price">--}}

                                        {{--Seria {{$product1->name}}--}}

                                    {{--</div>--}}

                                {{--</div>--}}

                            {{--@endforeach--}}

                        {{--</div>--}}

                        {{--<div class="swiper-button-next s4"></div>--}}

                        {{--<div class="swiper-button-prev s4"></div>--}}

                    {{--</div>--}}
                </div>
                <div class="column  is-one-third ">
                    <div class="catalog-item me">
                        <a href="{{$cat2->link}}">
                        
                        <img @if(!empty($cat2->image)) class="catalog-img" @endif src="{{asset('images/catalog-offer/'.$cat2->image)}}" alt="example">
                        
                        <span>{{$cat2->name}}</span>
                        
                        </a>
                    </div>
                    <div class="catalog-item last">
                        <a href="{{$cat3->link}}">
                        
                        <img @if(!empty($cat3->image)) class="catalog-img" @endif src="{{asset('images/catalog-offer/'.$cat3->image)}}" alt="example">
                        
                        <span>{{$cat3->name}}</span>
                        
                        </a>
                    </div>
                </div>
            </div>
            <div class="columns">

                <div class="column is-one-thirds">
                    <div class="catalog-item">

                        <a href="{{$cat4->link}}">

                            <img @if(!empty($cat4->image)) class="catalog-img" @endif src="{{asset('images/catalog-offer/'.$cat4->image)}}" alt="example" style="height: 200px;">

                            <span>{{$cat4->name}}</span>

                        </a>
                    </div>
                </div>
                <div class="column is-one-thirds">
                    <div class="catalog-item">

                        <a href="{{$cat5->link}}">

                            <img @if(!empty($cat5->image)) class="catalog-img" @endif src="{{asset('images/catalog-offer/'.$cat5->image)}}" alt="example" style="height: 200px;">

                            <span>{{$cat5->name}}</span>

                        </a>
                    </div>
                </div>
                <div class="column is-one-thirds">
                    <div class="catalog-item">

                        <a href="{{$cat6->link}}">

                            <img @if(!empty($cat6->image)) class="catalog-img" @endif src="{{asset('images/catalog-offer/'.$cat6->image)}}" alt="example" style="height: 200px;">

                            <span>{{$cat6->name}}</span>

                        </a>
                    </div>
                </div>
            </div>

        </div>

    </section>



    <section id="offers-produse">

        <div class="container">

            {{--<div class="mb4">--}}

                {{--<h2 class="title is-2">Type a title here</h2>--}}

                {{--<p>In ut mauris in nulla pharetra auctor ac eget risus. Duis iaculis risus--}}

                    {{--ut odio bibendum porttitor. Proin blandit est vitae luctus pulvinar.--}}

                    {{--Donec vestibulum lorem gravida dictum elementum. Praesent sem ex,--}}

                    {{--accumsan non rutrum in, molestie eu dolor. Etiam risus purus,--}}

                    {{--scelerisque quis neque sit amet, hendrerit laoreet risus. Fusce et--}}

                    {{--gravida neque. Pellentesque fringilla volutpat faucibus.--}}

                {{--</p>--}}

            {{--</div>--}}



            <div class="mb5">

                <h3 class="title is-3">Cele mai populare usi</h3>

                <div class="swiper-container s2">

                    <div class="swiper-wrapper">

                        @foreach($getProducts as $product1)

                            <div class="swiper-slide">

                                <a href="{{url('/product/'. $product1->getPro->name . '/' . $product1->product_id)}}">



                                    <img  class="catalog-img" src="@if(!empty($product1->getPro->color->image)) {{asset('images/catalog/'.$product1->getPro->colors->first()->image)}} @endif" alt="door">

                                </a>

                                {{--<div class="slide-title">--}}

                                {{--{{$product->name}}{{$product->serie->name}}--}}

                                {{--</div>--}}

                                <div class="slide-price">

                                    {{$product1->getPro->name}}

                                </div>

                            </div>

                        @endforeach

                    </div>

                    <div class="swiper-button-next s2"></div>

                    <div class="swiper-button-prev s2"></div>

                </div>

            </div>



            <div class="">

                <h3 class="title is-3">Ce mai noi modele de usi pentru interior</h3>

                <div class="swiper-container s3">

                    <div class="swiper-wrapper">

                        @foreach($newProduct as $product)

                            <div class="swiper-slide">

                                <a href="{{url('/product/'. $product->getPro->name . '/' . $product->product_id)}}">

                                    <img class="catalog-img" src="@if(!empty($product->getPro->color->image)) {{asset('images/catalog/'.$product->getPro->colors->first()->image)}} @endif" alt="door">

                                </a>

                                {{--<div class="slide-title">--}}

                                {{--Slide 1--}}

                                {{--</div>--}}

                                <div class="slide-price">

                                    {{$product->getPro->name}}

                                </div>

                            </div>

                        @endforeach

                    </div>

                    <div class="swiper-button-next s3"></div>

                    <div class="swiper-button-prev s3"></div>

                </div>

            </div>

        </div>

    </section>



    <section class="gr-background" id="articles">

        <div class="container">

            <h2 class="title is-2">Prinde ofertele acestui sezon</h2>

            <div class="columns is-multiline">

                @foreach($offers as $offer)

                <div class="column is-one-third">

                    <img src="{{asset('images/article/'.$offer->image)}}" alt="example">

                    <div class="blog-item">

                        <h4 class="title is-4">{{$offer->lang->name}}</h4>

                        <p>{!!  str_limit($offer->lang->description, 50) !!}</p>

                    </div>

                </div>

                @endforeach

            </div>

        </div>

    </section>



@endsection