@extends('front.layouts.new-white-nav')
@section('content')
    @if($thisCategory->id == 1 || $thisCategory->id ==2)
    <section id="categories_page" class="@if($thisCategory->id == 1){{'category-with-mini-sidenav'}}@endif">

        @if($thisCategory->id == 1)
        <div class="serie-block-me">
            <ul class="serie-arrange">
                @foreach($showserie as $serie)
                    <li>
                        <a href="{{url(Session::get('city_id') . '/catalog?serii=' . $serie->id)}}">
                            <span>{{$serie->name}}</span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="section-name">
            <h1 class="section-sub-title"><span>{{$thisCategory->lang->name}}</span></h1>
            @if($thisCategory->id == 1)
                <p>O uşã de interior este o alegere prin care poţi imprima personalitatea ta. Având posibilitatea de a alege dintr-o sumedenie de modele, culori şi finisaje, este foarte uşor de gãsit o variantã care sã transforme rapid interiorul ȋntr-un spaţiu surprinzãtor, frumos şi primitor.
                </p>
            @elseif($thisCategory->id == 2)
                @if(isset($showserie))
                    <h3 style="color: red">Pentru moment suplinim stocurile cu produse noi!</h3>
                    <h4 style="color: red">In curind vor apărea noi modele!</h4>
                @endif
            @elseif($thisCategory->id == 3)

            @endif
        </div>

        <catalog-series :city_name="{{json_encode(Session::get('city_id'))}}"/>

    </section>


    @elseif($thisCategory->id == 3)
       @include('front.partials.parchet')
    @endif

@endsection
