<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('images/icon.png')}}"> <!-- CSRF Token -->
    <meta name="facebook-domain-verification" content="cakyxr1on73uqr2phmwfvjs3lo7m0r"/>
    <meta name="csrf-token"
          content="{{ csrf_token() }}">
    <title>Etalon</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.css" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app" v-cloak>

    <div class="intro-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 offset-lg-2">
                    <a class="logo" href="#">
                        <img src="/img/logos/etalon-black.svg" alt="">
                    </a>
                </div>
{{--                <div class="text_explication">--}}
{{--                    <span>Alege locatia dorita</span>--}}
{{--                </div>--}}
                <div class="col-lg-6">
                    <ul>
                        <li class="location"><a style="padding: 5px 105px;" class="@if($city == 'iasi') active @endif" href="/set-city/iasi">Iași</a></li>
                        <li class="location"><a style="padding: 5px 20px;" class="@if($city == 'bucuresti') active @endif" href="/set-city/bucuresti">București</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>

