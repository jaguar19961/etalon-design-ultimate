@extends('front.layouts.new-white-nav')
@section('content')
    <products-catalog :city_name="{{json_encode(Session::get('city_id'))}}"></products-catalog>
@endsection
