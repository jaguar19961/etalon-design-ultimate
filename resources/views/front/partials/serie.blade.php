@extends('front.layouts.new-white-nav')
@section('content')
    <section id="offers-serie">

        <div class="section-name page-title">
            <h1 class="section-sub-title"><span>Produse din seria {{$thisCategory != null ? $thisCategory->name : ''}}</span></h1>
        </div>

        <div class="container">

            <div class="mb5" id="series">
                <div class="series">

                    <div class="sidenav mobfilter">
                        <div class="sidenav-reset">
                            <el-button type="primary" class="button is-primary btn-primary" @click="resetFilter()">Resetare filtre</el-button>
                        </div>
                        <div>
                            <h3 class="sidenav-section-title">serii</h3>
                            <ul class="sidenav-series">
                                <li v-for="(serie, index) in series"
                                    :key="index"
                                    :class="{'active' : serie.id === serie_id}">
                                    <a :href="'/category/serie/'+ serie.name + '/' + serie.id">
                                        <span> @{{serie.name}}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="sidenav-culori">
                            <h3 class="sidenav-section-title">Culoare</h3>
                            <div class="sidenav-section-content">
                                <div v-if="group_colors.length > 0">
                                    <ul class="sidenav-color">
                                        <li v-for="(color, i) in group_colors" :key="i" :class="(color_id != '' && color_id == color.id) ? 'active' : ''" @click="color_id = color.id; GetDors()">
                                            <img :src="'https://order.profildoors.ro/'+color.image" :alt="color.name" v-if="color.image != null" >
                                            <span v-else :style="'background-color:' + color.code +';'"></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="sidenav-window">
                            <h3 class="sidenav-section-title">Insertii</h3>

                            <div v-if="group_windows.length > 0" class="sidenav-section-content">
                                <ul class="sidenav-color">
                                    <li v-for="(wind, u) in group_windows"
                                        :key="u"
                                        :class="(window_id != '' && window_id == wind.id) ? 'active' : ''"
                                        @click="window_id = wind.id; GetDors()">
                                        <img :src="'https://order.profildoors.ro/'+wind.image" :alt="wind.name">
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <div class="sidenav-stil">
                            <h3 class="sidenav-section-title">Stil</h3>

                            <div v-if="door_styles.length > 0" class="sidenav-section-content">
                                <el-checkbox-group v-model="styles">
                                    <el-checkbox :label="style.id" v-for="(style, index) in tags" :key="index" @change="GetDors()">@{{ style.name }}</el-checkbox>
                                </el-checkbox-group>
                            </div>

                        </div>

                        <div class="sidenav-reset">
                            <el-button type="primary" class="button is-primary btn-primary" @click="resetFilter()">Resetare filtre</el-button>
                        </div>

                    </div>
                    <div class="content">
                        <pagination :data="laravelData" @pagination-change-page="getResults" :limit="2" align="center"></pagination>

                        <transition-group name=”fade” class="columns is-multiline is-mobile" tag="div" v-if="doors.length > 0">
                            <div class="column bigd is-3-desktop is-half-mobile is-half-tablet" v-for="(dor, index) in doors" :key="'index'+index" v-if="getImage(dor) != null">

                                <a :href="'/'+city_name+'/product/'+dor.name+'/'+dor.id" class="catalog-img">

                                    <img :src="'https://order.profildoors.ro/images'+getImage(dor)" alt="door"
                                         style="width: 160px;">
                                </a>
                                <div class="slide-title has-text-weight-bold">
                                    Seria @{{dor.name}}
                                </div>
{{--                                <div class="door-price">--}}
{{--                                    <span>Preț de la <strong>@{{dor.price}} Lei</strong></span>--}}
{{--                                </div>--}}
                                <div class="slide-link">
                                    <a :href="'/'+city_name+'/product/'+dor.name+'/'+dor.id">Vezi culori</a>
                                </div>
                            </div>
                        </transition-group>

                        <infinite-loading :identifier="infiniteId" @infinite="infiniteHandler"></infinite-loading>
                    </div>

                    <div class="serie-description">
                        @if(isset( $thisCategory->lang)){!! $thisCategory->lang->name !!}@endif
                        @if(isset($thisCategory->lang)){!! $thisCategory->lang->slug !!}@endif
                    </div>


                </div>

                <span class="openfilters-mobile" onclick="openNav()">&#9776; FILTRE</span>

                <div id="mySidenav" class="sidenav1">
                    <div class="sidenav mobile-sidenav-me">
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                        <div class="sidenav-reset">
                            <el-button type="primary" class="button is-primary btn-primary" @click="resetFilter()">Resetare filtre</el-button>
                        </div>
                        <div>
                            <h3 class="sidenav-section-title">serii</h3>
                            <ul class="sidenav-series">
                                <li v-for="(serie, index) in series"
                                    :key="index"
                                    :class="{'active' : serie.id === serie_id}">
                                    <a :href="'/category/serie/'+ serie.name + '/' + serie.id">
                                        <span> @{{serie.name}}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
{{--                        <div class="sidenav-pret">--}}
{{--                            <h3 class="sidenav-section-title">Preț de la</h3>--}}
{{--                            <div class="sidenav-section-content">--}}
{{--                                <label>min: @{{ price[0] }}Lei, max: @{{ price[1] }}Lei</label>--}}
{{--                                <el-slider--}}
{{--                                        v-model="price"--}}
{{--                                        range--}}
{{--                                        show-stops--}}
{{--                                        :min="min"--}}
{{--                                        :max="max"--}}
{{--                                        @change="GetDors()">--}}
{{--                                </el-slider>--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="sidenav-culori">
                            <h3 class="sidenav-section-title">Culoare</h3>
                            <div class="sidenav-section-content">
                                <div v-if="group_colors.length > 0">
                                    <ul class="sidenav-color">
                                        <li v-for="(color, i) in group_colors" :key="i" :class="(color_id != '' && color_id == color.id) ? 'active' : ''" @click="color_id = color.id; GetDors()">
                                            <img :src="'https://order.profildoors.ro/'+color.image" :alt="color.name" v-if="color.image != null" >
                                            <span v-else :style="'background-color:' + color.code +';'"></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="sidenav-window">
                            <h3 class="sidenav-section-title">Insertii</h3>

                            <div v-if="group_windows.length > 0" class="sidenav-section-content">
                                <ul class="sidenav-color">
                                    <li v-for="(wind, u) in group_windows"
                                        :key="u"
                                        :class="(window_id != '' && window_id == wind.id) ? 'active' : ''"
                                        @click="window_id = wind.id; GetDors()">
                                        <img :src="'https://order.profildoors.ro/'+wind.image" :alt="wind.name">
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <div class="sidenav-stil">
                            <h3 class="sidenav-section-title">Stil</h3>

                            <div v-if="door_styles.length > 0" class="sidenav-section-content">
                                <el-checkbox-group v-model="styles">
                                    <el-checkbox :label="style.id" v-for="(style, index) in door_styles" :key="index" @change="GetDors()">@{{ style.name }}</el-checkbox>
                                </el-checkbox-group>
                            </div>

                        </div>

                        <div class="sidenav-type-me">
                            <h3 class="sidenav-section-title">Tip</h3>
                            <div class="sidenav-tip sidenav-section-content" v-if="door_types.length > 0">
                                <el-radio-group v-model="door_type">
                                    <el-radio :label="dtype.id" v-for="(dtype, index) in door_types" :key="index" @change="GetDors()">@{{ dtype.type }}</el-radio>
                                </el-radio-group>
                            </div>

                        </div>

                        <div class="sidenav-reset">
                            <el-button type="primary" class="button is-primary btn-primary" @click="resetFilter()">Resetare filtre</el-button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
{{--{{dd(Session::get('city_id'))}}--}}

@endsection

@section('script')
    <script> window.min_value = @json($min)</script>
    <script> window.max_value = @json($max)</script>
    <script> window.serie_id = @json($thisCategory != null ? $thisCategory->id : '')</script>
    <script> window.groupcolors = @json($groupcolors)</script>
    <script> window.groupwindows = @json($groupwindows)</script>
    <script> window.doorStyles = @json($doorStyle)</script>
    <script> window.doorTypes = @json($doorType)</script>
    <script> window.door_tp = @json($door_type)</script>
    <script> window.door_st = @json($styles)</script>
    <script> window.colorid = @json($color_id)</script>
    <script> window.windowid = @json($window_id)</script>
    <script> window.city_name = @json(Session::get('city_id'))</script>
@endsection
