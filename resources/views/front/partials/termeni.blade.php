@extends('front.layouts.new')
@section('content')
    <section id="article" style="margin-top: 50px; margin-bottom: 150px; padding: 20px;">
        {{--<div class="mb4 has-text-centered">--}}
            {{--<span style="font-size: 22px; font-weight: 600;">Termeni și condiții</span>--}}
        {{--</div>--}}
        <div class="container">
            <span style="font-size: 22px; font-weight: 600;">POLITICA DE CONFIDENŢIALITATE</span>
            <p>&Icirc;ti multumim ca vizitezi site-ul nostru. Cand vizitezi site-ul Etalon.ro, cand ne contactezi sau cand apelezi la serviciile noastre, ne incredintezi informatiile tale. Scopul acestei Politici de confidentialitate este sa iti explice ce date prelucram, de ce le prelucram si ce facem cu ele. &Icirc;ti luam in serios confidentialitatea si nu vindem niciodata liste sau adrese de e-mail. Fiind pe deplin constienti de faptul ca informatiile tale personale iti apartin, facem tot posibilul sa le stocam in siguranta si sa procesam cu atentie informatiile pe care le imparti cu noi. Nu oferim informatii unor parti terte fara a te informa. Aceste informatii sunt importante. Speram ca le vei citi cu atentie.</p>
            <p>Prin navigarea pe site-ul www.etalon.ro sau prin contactarea noastra prin formular declarari ca ai fost informat in mod adecvat asupra prelucrarii datelor prin parcurgerea acestui document, denumit &bdquo;Politica de confidentialitate&rdquo;.</p>
            <p>DEFINIȚII</p>
            <p>Vom incepe sa definim cateva notiuni care sa te ajute sa intelegi prezenta Politica de confidentialitate.</p>
            <p>Etalon.ro (sau &bdquo;Site-ul&rdquo;) este un site de prezentare al S.C. ETALON INTERIORS S.R.L., oferit prin intermediul URL-ului https://www.etalon.ro/, care cuprinde si un blog pentru a oferi informatii utile cititorilor. Etalon este detinut si administrat de S.C. ETALON INTERIORS S.R.L., cu sediul profesional in București, strada Avrig nr. 29B, Sect. 2, avand CIF 40598290, denumit in continuare &bdquo; Etalon&rdquo; sau &bdquo;noi&rdquo;.</p>
            <p>Potrivit Legii nr. 677/2001 si a Regulamentului (UE) 679/2016 (aplicabil in Romania din 25 mai 2018), Etalon este operator de date cu caracter personal.</p>
            <p>Cand spunem &bdquo;GDPR&rdquo; ne referim la Regulamentul (UE) 679/2016 privind protectia persoanelor fizice in ceea ce priveste prelucrarea datelor cu caracter personal si privind libera circulatie a acestor date si de abrogare a Directivei 95/46/CE.</p>
            <p>&bdquo;Date cu caracter personal&rdquo; inseamna orice informatii privind o persoana fizica identificata sau identificabila (&bdquo;persoana vizata&rdquo;);</p>
            <p>Potrivit legislatiei, tu, in calitate de vizitator al site-ului esti o &bdquo;persoana vizata&rdquo;, adica o persoana fizica identificata sau identificabila. O persoana fizica identificabila este o persoana care poate fi identificata, direct sau indirect, in special prin referire la un element de identificare, cum ar fi un nume, un numar de identificare, date de localizare, un identificator online, sau la unul sau mai multe elemente specifice, proprii identitatii sale fizice, fiziologice, genetice, psihice, economice, culturale sau sociale.</p>
            <p>CINE SUNTEM</p>
            <p>ETALON este distribuitor al unor marci de prestigiu pentru usi de interior / exterior precum si pardoseli: * PROFIL DOORS &ndash; domeniul rezidential, hotelier, tehnic cu usi de interior, exterior * FLOOREXPERS &ndash; domeniu rezidențial, hotelier, tehnic cu pardoseli, laminat și parchet.</p>
            <p>SCHIMBĂRI</p>
            <p>Putem schimba aceasta Politica de confidentialitate in orice moment. Toate actualizarile si modificarile prezentei Politici sunt valabile imediat dupa notificare, pe care o vom realiza prin afisare pe site si/sau notificare pe e-mail.</p>
            <p>&Icirc;NTREBĂRI ŞI SOLICITĂRI</p>
            <p>Daca ai intrebari sau nelamuriri cu privire la prelucrarea datelor tale sau doresti sa iti exerciti drepturile legale in legatura cu datele&nbsp; pe care le detinem sau daca ai ingrijorari cu privire la modul in care tratam orice problema de confidentialitate, ne poti scrie la adresa de e-mail: info@etalon.ro</p>
            <p>CE FEL DE INFORMAŢII COLECTĂM</p>
            <p>a) Informatiile pe care ni le furnizezi voluntar</p>
            <p>Cand utilizezi formularele de pe site, cand ne contactezi prin telefon sau e-mail sau comunici cu noi in orice mod, ne dai in mod voluntar informatiile pe care le prelucram. Aceste informatii includ numele, prenumele, adresa, adresa de e-mail si numarul de telefon.</p>
            <p>Cand postezi un comentariu la un articol de pe blog, ne dai informatiile tale (nume si e-mail).</p>
            <p>Oferindu-ne aceste informatii, noi le pastram in conditii de securitate si confidentialitate in baza noastra de date. Nu dezvaluim sau tranferam informatii catre terti.</p>
            <p>b) Informatii pe care le colectam automat</p>
            <p>Cand navighezi pe site-ul nostru, este posibil sa colectam informatii despre vizita ta pe site. Aceste informatii pot include adresa IP, sistemul de operare, browserul, activitatea de navigare si alte informatii despre modul in care ai interactionat cu site-u. Putem colecta aceste informatii prin folosirea cookie-urilor sau a altor tehnologii de similare.</p>
            <p>CARE ESTE TEMEIUL LEGAL PENTRU PRELUCRARE?</p>
            <p>&Icirc;n privinta datelor pe care ni le oferi voluntar prin completarea si trimiterea formularelor aferente sau prin contactarea noastra in orice mod, temeiul legal este &bdquo;luarea unor masuri la cererea persoanei vizate inaintea incheierii unui contract&rdquo; (art. 5 alin. 2 lit. (a) din Legea nr. 677/2001) si &bdquo;pentru a face demersuri la cererea persoanei vizate inainte de incheierea unui contract&rdquo; (art. 6 alin. (1) lit. b din Regulamentul (UE) 679/2016).</p>
            <p>&Icirc;n privinta datelor pe care ni oferi voluntar prin postarea unui comentariu la un articol de pe blog, temeiul prelucrarii este obligatia noastra legala de a sprijini exercitarea dreptului de informare, dar si interesul nostru legitim de a oferi cititorilor blogului transparenta si informare.</p>
            <p>Atat potrivit legislatiei actuale, cat si potrivit GDPR (aplicabil din 25 mai 2018), consimtamantul tau nu este cerut in situatia in care prelucrarea este necesara pentru efectuarea de demersuri pentru incheierea unui contract, indeplinirea unei obligatii legale sau interesul legitim.</p>
            <p>&Icirc;n privinta datelor pe care le colectam automat prin folosirea cookie-urilor sau a altor tehnologii similare, temeiul pentru prelucrare este consimtamantul. Odata cu accesarea site-ului, iti dai in mod valabil consimtamantul asupra prelucrarii.</p>
            <p>&Icirc;N CE SCOPURI COLECTĂM DATELE?</p>
            <p>pentru a-ti raspunde la intrebari si solicitari;</p>
            <p>pentru a ne apara impotriva atacurilor cibernetice;</p>
            <p>pentru a oferi si imbunatatii serviciile pe care le oferim.</p>
            <p>in scop marketing, insa numai in situatia in care ti-ai dat consimtamantul in prealabil.</p>
            <p>Pentru a nu mai primi mesaje promotionale, poti urma instructiunile de dezabonare incluse in fiecare e-mail;</p>
            <p>C&Acirc;T TIMP STOCĂM DATELE?</p>
            <p>Stocam datele cu caracter personal doar pe perioada necesara indeplinirii scopurilor, dar nu mai mult de 10 ani de la ultima vizita pe site sau ultima interactiune cu noi.</p>
            <p>CUM DEZVĂLUIM INFORMAŢIILE TALE?</p>
            <p>Nu vom dezvalui informaţiile tale catre terţe parţi, pentru a fi utilizate in propriile scopuri de marketing sau comerciale ale acestora, fara consimţamantul tau. Totuşi, este posibil sa dezvaluim informaţiile tale catre urmatoarele entitaţi:</p>
            <p>Furnizori de servicii. Putem dezvalui informatiile tale altor companii care ne furnizeaza servicii si actioneaza in calitate de persoane imputernicite, cum ar fi companiile care ne ajuta pentru facturare sau care trimit e-mailuri in numele nostru. Aceste entitati sunt selectate cu o grija deosebita pentru a ne asigura ca indeplinesc cerintele specifice in materie de protectie a datelor cu caracter personal. Aceste entitaţi au o capacitate limitata de a utiliza informaţiile tale in alte scopuri decat cel de a ne furniza servicii;</p>
            <p>Instante de judecata, parchete sau alte autoritati publice pentru a ne conforma legii sau drept raspuns la un procedura legala obligatorie (cum ar fi un mandat de percheziţie sau o hotarare judecatoreasca);</p>
            <p>Altor parţi cu consimţamantul sau la instrucţiunile tale. &Icirc;n afara dezvaluirilor descrise in prezenta Politica de confidenţialitate, este posibil sa transmitem informaţiile terţilor carora consimţi sau soliciti sa efectuam o asemenea dezvaluire.</p>
            <p>TRANSFERĂM DATE CĂTRE STATE TERŢE?</p>
            <p>&Icirc;n prezent, nu transferam datele tale catre state din afara Uniunii Europene. Daca vom schimba politica, te vom informa in mod corespunzator, iti vom prezenta garantiile aferente si iti vom solicita consimtamantul.</p>
            <p>CARE SUNT DREPTURILE TALE?</p>
            <p>Dreptul de retragere a consimtamantului;</p>
            <p>&Icirc;n situatia in care prelucrarea se bazeaza pe consimtamant, iti poti retrage consimtamantul in orice moment si in mod gratuit prin transmiterea unui e-mail pe adresa info@etalon.ro cu subiectul &bdquo;retragere consimtamant&rdquo; sau prin SMS la <a href="tel:+40%20(755)%20476%20598">+40 (755) 476 598</a> cu textul &bdquo;retragere consimtamant&rdquo; (SMS-ul trebuie sa fie trimis de pe numarul inregistrat in baza noastra de date).</p>
            <p>Dreptul de a depune o plangere in fata unei autoritati de supraveghere;</p>
            <p>Dreptul de a te adresa justitiei;</p>
            <p>Dreptul de acces;</p>
            <p>Ai dreptul de a obtine din partea noastra o confirmare ca se prelucreaza sau nu date cu caracter personal care te privesc si, in caz afirmativ, ai dreptul de acces la datele respective.</p>
            <p>Dreptul la rectificare;</p>
            <p>Ai dreptul de a obtine de la noi, fara intarzieri nejustificate, rectificarea datelor cu caracter personal inexacte care te privesc. Ținandu-se seama de scopurile in care au fost prelucrate datele, ai dreptul de a obtine completarea datelor cu caracter personal care sunt incomplete, inclusiv prin furnizarea unei declaratii suplimentare.</p>
            <p>Dreptul la stergerea datelor (&bdquo;dreptul de a fi uitat&rdquo;);</p>
            <p>&Icirc;n situatiile in care (1) datele nu mai sunt necesare pentru indeplinirea scopurilor, (2) s-a retras consimtamantul si nu exista un alt temei juridic pentru prelucrare, (3) te opui prelucrarii si nu exista motive legitime care sa prevaleze in ceea ce priveste prelucrarea sau (4) datele cu caracter personal au fost prelucrate ilegal, ai dreptul de a obtine stergerea datelor care te privesc, fara intarzieri nejustificate.</p>
            <p>Dreptul la restrictionarea prelucrarii;</p>
            <p>Ai dreptul de a obtine din partea noastra restrictionarea prelucrarii in cazul in care se aplica unul din urmatoarele cazuri:</p>
            <p>(a) contesti exactitatea datelor, pentru o perioada care ne permite noua sa verificam exactitatea datelor;</p>
            <p>(b) prelucrarea este ilegala, iar tu te opui stergerii datelor cu caracter personal, solicitand in schimb restrictionarea utilizarii lor;</p>
            <p>(c) nu mai avem nevoie de datele cu caracter personal in scopul prelucrarii, dar tu ni le soliciti pentru constatarea, exercitarea sau apararea unui drept in instanta;</p>
            <p>(d) te-ai opus prelucrarii in conformitate cu articolul 21 alineatul (1) din GDPR, pentru intervalul de timp in care se verifica daca drepturile noastre legitime prevaleaza asupra drepturilor tale.</p>
            <p>Dreptul la portabilitatea datelor</p>
            <p>Ai dreptul de a primi datele cu caracter personal care te privesc si pe care ni le-ai furnizat intr-un format structurat, utilizat in mod curent si care poate fi citit automat si ai dreptul de a transmite aceste date altui operator, fara obstacole din partea noastra, in cazul in care:</p>
            <p>(a) prelucrarea se bazeaza pe consimtamant in temeiul articolului 6 alineatul (1) litera (a) sau al articolului 9 alineatul (2) litera (a) din GDPR sau pe un contract in temeiul articolului 6 alineatul (1) litera (b) din GDPR; si</p>
            <p>(b) prelucrarea este efectuata prin mijloace automate.</p>
            <p>Dreptul de a nu face obiectul unei decizii bazate exclusiv pe prelucrarea automata, inclusiv crearea de profiluri, care produce efecte juridice care te privesc sau te afecteaza in mod similar intr-o masura semnificativa</p>
            <p>Nu ai acest drept in cazul in care decizia:</p>
            <p>(a) este necesara pentru incheierea sau executarea unui contract intre tine si un operator de date;</p>
            <p>(b) este autorizata prin dreptul Uniunii sau dreptul intern care se aplica operatorului si care prevede, de asemenea, masuri corespunzatoare pentru protejarea drepturilor, libertatilor si intereselor legitime ale persoanei vizate; sau</p>
            <p>(c) are la baza consimtamantul tau explicit.</p>
            <p>TE RUGĂM SĂ REȚII URMĂTOARELE:</p>
            <p>Perioada de timp: Vom incerca sa raspundem solicitarii in termen de 30 de zile. Cu toate acestea, termenul poate fi prelungit din motive specifice legate de dreptul legal specific sau complexitatea cererii tale.</p>
            <p>Restrictionarea accesului:&nbsp;in anumite situatii, este posibil sa nu va putem oferi acces la toate sau la unele dintre datele tale personale datorita dispozitiilor legale. Daca refuzam cererea ta de acces, te vom informa despre motivul refuzului.</p>
            <p>Imposibilitatea de a te indentifica</p>
            <p>&Icirc;n unele cazuri, este posibil sa nu putem cauta datele tale personale din cauza identificatorilor pe care ii furnizezi in solicitarea ta. Un exemplu de date cu caracter personal pe care nu le putem consulta atunci cand ne furnizezi numele si adresa de e-mail sunt datele colectate prin cookie-uri de browser.</p>
            <p>&Icirc;n astfel de cazuri, in cazul in care nu te putem identifica ca persoana vizata, nu suntem in masura sa ne conformam solicitarii tale, cu exceptia cazului in care furnizezi informatii suplimentare care sa permita identificarea.</p>
            <p>CUM &Icirc;ŢI EXERCIŢI DREPTURILE?</p>
            <p>Pentru a-ti exercita drepturile legale, te rugam sa ne contactezi la adresa de e-mail info@etalon.ro<br />Felicitari! Ai ajuns la sfarsit. &Icirc;ti multumim ca ti-ai facut timp sa aflii despre Politica de confidentialitate&nbsp;Etalon.</p>
        </div>
    </section>

@endsection