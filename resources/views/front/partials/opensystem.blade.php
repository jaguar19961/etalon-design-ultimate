@extends('front.layouts.new-white-nav')
@section('content')

<!--Sisteme de deschidere-->
<section id="offers_page">
    <div class="section-name page-title">
        <h1 class="section-sub-title"><span>Sisteme de deschidere </span> moderne</h1>
    </div>
    <div class="container section-content">
        @foreach($openSystem as $value)
            <div class="col">

                <div class="sistem-card">
                    <div class="sistem-img"><a href="{{url(Session::get('city_id') . '/sisteme-de-deschidere/afisare/'.$value->slug. '/' .$value->id)}}">
                            <img src="{{asset('images/systems/'.$value->image)}}" alt="sisteme-de-deschidere"></a></div>
                    <div class="card-content">
                        <h3 class="card-title">{{$value->name}}</h3>
                        <a href="{{url(Session::get('city_id') . '/sisteme-de-deschidere/afisare/'.$value->slug. '/' .$value->id)}}">
                            <span>Toate modelele</span>
                        </a>
                    </div>
                </div>

            </div>
        @endforeach
    </div>
</section>
<!--END Sisteme de deschidere-->
    @endsection
