<section id="parchet_page">
    <div class="section-name page-title">
        <h1 class="section-sub-title"><span>APLICAȚIE</span></h1>
        @if($thisCategory->id == 3)
            @if($showserie == null)
                <h3 style="color: red">Pentru moment suplinim stocurile cu produse noi!</h3>
                <h4 style="color: red">In curind vor apărea noi modele!</h4>
            @endif
        @endif
    </div>
    <div class="section-offer container">
        <div class="img-offer">
            <img src="{{asset('/img/new-image/parchet-offer.png')}}" alt="">
        </div>
        <div class="text-content">
            <div>
                <h3 class="title">Alege culoarea parchetului din <span>3 mișcări<br> Cu aplicația online de pe etalon.ro</span></h3>
                <ul class="char">
                    <li><span>1</span> <p>Deschizi aplicația,</p></li>
                    <li><span>2</span> <p>Faci o poză camerei, sau încarci de pe telefon/computer,</p></li>
                    <li><span>3</span> <p>Rulezi de la o nuanță de parchet la alta, până dai de preferata ta.</p> </li>
                </ul><br>
                <p>Apoi îți rămâne doar să vii să-l vezi pe viu, în showroom.</p><br>
                <p class="desc-with-red-effect">Sau <span>INVITĂ</span> consultantul la tine acasă: <span>+40 743 204 25</span><br>
                    Va aduce mostre, va face măsurătorile, va calcula prețul și chiar va face și contractul.<br>
                    Și <span>VOILA!</span> Ai cu o bătaie de cap mai puțin!<br>
                    Nu pierde vremea, umblând pe zeci de drumuri, <span>BE SMART</span>, alege calea cea mai<br> simplă și cea mai eficientă.

                </p>
                <div class="btn-offer">
                    <button type="button" class="button is-primary btn-primary" onclick="javascript: roomvo.startStandalone()">ACCESEAZĂ ACUM!</button>
                </div>
            </div>
        </div>
    </div>
    <div class="section-name">
        <h1 class="section-sub-title"><span>{{$thisCategory->lang->name}}</span></h1>
    </div>


    <div class="container section-content">
        @foreach($showserie as $item)
            <div class="col">
                <div class="parket-card">
                    <a href="{{url(Session::get('city_id') . '/product/'. str_slug($item->name) . '/' . $item->id)}}" class="parket-price">DE LA<br>{{((float)$item->price / 4.5) * 1.05}}euro</a>
                    <a href="{{url(Session::get('city_id') . '/product/'. str_slug($item->name) . '/' . $item->id)}}" class="parket-img">
                        <img class="parket-img" src="{{asset('images/catalog/'. $item->image)}}" alt="parchet">
                    </a>
                    <div class="parket-tile">
                        {{$item->name}}
                    </div>
                </div>
            </div>
        @endforeach
    </div>

</section>
