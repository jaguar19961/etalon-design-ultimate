@extends('front.layouts.new-white-nav')
@section('content')
    <section id="contact_page">
        <div class="section-name page-title"><h1 class="section-sub-title"><span>Contacte</span></h1></div>
        <div class="container section-conten">
            <div class="col">
               <div>
                   <h1 class="section-name-map"><span>București</span></h1>
                   <iframe src="{{$infos->go}}"
                           width="100%" height="250" frameborder="0" allowfullscreen style="height: 200px !important;"></iframe>

               </div>
                <div>
                    <h1 class="section-name-map"><span>Iași</span></h1>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d41095.286128564534!2d27.526613756744865!3d47.173407562570304!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40cafb4e3f01b699%3A0x6e49732f3d767311!2sBl.%20508%2C%20Strada%20Canta%2039%2C%20Ia%C8%99i%2C%20Rom%C3%A2nia!5e0!3m2!1sro!2s!4v1583922073355!5m2!1sro!2s"
                            width="100%" height="250" frameborder="0" allowfullscreen style="height: 200px !important;"></iframe>
                </div>
                </div>
            <div class="col">
                <h2 class="fs-35">Contactează-ne</h2>
                <p>Te rugăm să ne contactezi dacă ai întrebări sau ai nevoie de asistență. Îți oferim cu drag toate informațiile necesare și suport.</p>
                {{ Form::open(array('method' => 'put','action' => 'FrontController@getColaboration')) }}
                {{ csrf_field() }}
                <div class="columns">
                    <div class="column"><input class="input" type="text" placeholder="Nume" name="nume"></div>
                </div>
                <div class="columns">
                    <div class="column pr-9"><input class="input" type="tel" placeholder="Telefon" name="telefon"></div>
                    <div class="column pl-9"><input class="input" type="email" placeholder="Email" name="email"></div>
                </div>
                <div class="columns">
                    <div class="column"><input class="input" type="text" placeholder="Subiect" name="subiect"></div>
                </div>
                <div class="columns">
                    <div class="column"><textarea class="textarea" placeholder="Mesaj" name="mesaj"></textarea></div>
                </div>
                <button type="submit" class="button is-primary btn-primary">Trimite</button>
                {{Form::close()}}
            </div>
    </section>

@endsection