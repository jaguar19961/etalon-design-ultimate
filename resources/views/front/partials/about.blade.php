@extends('front.layouts.new-white-nav')
@section('content')

    <section id="about-hed">
        <img src="/img/about/hed.png" alt="example" class="top_img">
        <div class="section-header-content">
            <div class="section-name section-name-50 d-flex-vertical-align-center h-100">
                <h1 class="section-sub-title"><span>DESPRE NOI</span><br>ETALON.ro - salon de uși de interior și pardoseli în București.</h1>
                <p>Salonul Etalon.ro este rezultatul a peste 5 ani de experiență a echipei noastre în domeniul de consultanță și vânzare a ușilor și pardoselilor. În acești ani de muncă am învățat tot despre uși și pardoseli. Am ales doar produse premium și am reușit să facem cele mai accesibile prețuri din gama premium. Noi știm să alegem ce este mai bun și să-ți recomandăm doar ce ți se potrivește.</p>
            </div>
        </div>
    </section>

    <section class="about-content">
        <div class="container">
            <div class="about-text d-flex-vertical-align-center h-100">
                <h3>Cel mai important serviciu al nostru este</h3>
                <h2>CONSULTANȚĂ</h2>
                <ul>
                    <li>Răspundem rapid la fiecare solicitare</li>
                    <li>Studiem fiecare proiect în particular</li>
                    <li>Oferim cel mai potrivit produs</li>
                    <li>Suntem amabili și transparenți</li>
                </ul>
            </div>
            <div class="about-img">
                <img src="/img/about/1.png" alt="">
            </div>
            <div class="about-img">
                <img src="/img/about/2.png" alt="">
            </div>
            <div class="about-text d-flex-vertical-align-center h-100">
                <h3>Avem două tipuri de produse</h3>
                <h2>UȘI DE INTERIOR ȘI PARDOSELI</h2>
                <p>Am selectat din tot ce am testat în ultimii 5 ani. Pentru noi raportul dintre preț și calitate este o prioritate. Ușile de interior le aducem de la PROFIL DOORS.</p>
                <p>Este o fabrică din Rusia care a făcut o simbioză perfectă între design, calitate și preț. Am ales acest producător din câteva motive:</p>
                <ul>
                    <li>Implică Designerii italieni pentru a face colecții de uși frumoase, în pas cu ultimele tendințe ale arhitecturii moderne.</li>
                    <li>Colaborează direct cu inginerii germani pentru a implementa cea mai bună tehnologie, învelișuri foarte rezistente la uzură, mecanisme diverse și durabile. Și face uși de interior care vor ține zeci de ani.</li>
                    <li>Folosește materie primă și forță de muncă rusească, pentru a consuma lemn de acolo de crește din belșug și costă mult mai ieftin decât în Europa. Iar calitatea forței de muncă este la nivelul standaradelor așteptate pe piața europeană.</li>
                </ul>
                <p>Astfel putem aduce UȘI DE INTERIOR de clasă PREMIUM la prețuri accesibile românilor. Am adus în paleta noastră peste 2500 de modele de uși, de la cele mai chic-clasice până la cele futuriste-industriale LOFT.</p>
            </div>
            <div class="title-about">
                <h2>uși de interior cu DESCHIDERI ALTERNATIVE și mărimi nestandard</h2>
            </div>
            <div class="about-img">
                <img src="/img/about/3.png" alt="">
            </div>
            <div class="about-text d-flex-vertical-align-center h-100">
                <h4>UȘI ROTATIVE</h4>
                <p>Uși care se rotesc pe axa verticală și se deschid în ambele direcții, ocupând doar spațiul din golul de ușă. Bune pentru a economisi spațiu, dar și pentru a uimi oaspeții și a manifesta pesonalitatea proprietarului casei/biroului.</p>
                <h4>UȘI GLISANTE</h4>
                <p>Foarte bune pentru a partaja spațiul dintre living și bucătărie sau intrările în dressing. Pot fi duble sau de mărimi nestandard - până la 3m înălțime.</p>
                <h4>UȘI GLISANTE MAGIC</h4>
                <p>Ușa care te pune pe întrebări când o vezi prima oară. Pentru că sistemul de glisare și fixare al ușii pe perete este invizibil. E doar o foaie de ușă care alunecă de-a lungul peretelui ca o fantomă. Este o ușă scumpă dar își merită banii, pentru că e pe cât de durabilă pe atât de frumoasă.</p>
            </div>

            <div class="about-text d-flex-vertical-align-center h-100">
                <h4>UȘI PLIANTE</h4>
                <p>Aceste uși se pliază ca o carte, pot fi single sau duble. Și sunt o alternativă bună- ușilor clasice, atunci când spațiul este foarte limitat, iar mobilierul foarte aproape de tocul ușii și a zonei de deschidere.</p>
                <h4>UȘI PLIANTE COMPACT</h4>
                <p>Acest tip de pliere este mai diferit decât suntem obișnuiți să vedem. Pentru că se pliază în exterior și se lipesc de perete, astfel eliberând maxim golul de ușă și economisește spațiul de deschidere. Sunt bune pentru spații foarte mici și goluri de ușă foarte înguste, dar dacă faci o ușă dublă cu așa deschidere va părea foarte High Tech, poate fi la fix pentru un dressing mare, pentru o sală de restaurant sau living.</p>
                <h4>UȘI INVIZIBILE</h4>
                <p>Acest tip de uși se acoperă cu tapet, sau se vopsesc ori se pot acoperi cu piatră sau lemn ca și peretele pe care se instalează. Rolul lor este de a se integra ușor cu peretele și a rămâne invizibile. Bune pentru a masca intrările în camere care nu trebuie să iasă în evidență sau dacă sunt prea multe uși și nu vrem ca toate să iasă în evidență. Dacă te uiți atent la poza de mai sus o să observi o ușă invizibilă acoperită cu lemn în stilul peretelui.</p>
            </div>
            <div class="about-img">
                <img src="/img/about/4.png" alt="">
            </div>
            <div class="about-img">
                <img src="/img/about/5.png" alt="">
            </div>
            <div class="about-text d-flex-vertical-align-center h-100">
                <h2>PEREȚI DESPĂRȚITORI DIN STICLĂ CU UȘI GLISANTE</h2>
                <p>Putem face și PEREȚI DESPĂRȚITORI DIN STICLĂ CU UȘI GLISANTE. Acest tip de partiție a spațiului este fantastic pentru că păstrează lumina și senzația de open space.</p>
            </div>
            <div class="about-text d-flex-vertical-align-center h-100">
                <h2>pardoseli de la Floor Expert Slovenia</h2>
                <p>Pe partea de pardoseli am ales o colecție vastă importată de la Floor Expert Slovenia. I-am ales pe ei pentru că sunt experți și pentru că ne dau o varietate foarte mare de LAMINAT și PARCHET, atât pentru locuințe cât și industrial.</p>
            </div>
            <div class="about-img">
                <img src="/img/about/6.png" alt="">
            </div>
            <div class="title-about"><h2>Dacă renovezi casa, îți construiești una nouă sau dacă ești designer de interior vino la noi să vezi cu ce îți putem fi de folos!</h2></div>
        </div>
    </section>

@endsection