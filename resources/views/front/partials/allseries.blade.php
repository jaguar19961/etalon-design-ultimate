@extends('front.layouts.new')
@section('content')
    <section id="offers-serie" style="margin-top: 50px; margin-bottom: 50px; padding: 20px;">
        <div class="serie-block-me">
            <div class="container" >
                <ul class="serie-arrange">
                    @foreach($series as $serie)
                        <li>
                            <a href="{{url(Session::get('city_id') . '/catalog?serii=' . $serie->id)}}">
                                <span>{{$serie->name}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="container">
            <div class="mb4 has-text-centered ">
                <h2 class="title is-2">Uși din colecția etalon.ro</h2>
                {{--<div class="content">--}}
                    {{--@if(isset($thisCategory->lang)){!! $thisCategory->lang->name !!}@endif--}}
                {{--</div>--}}
                {{--<transition name="fade">--}}
                    {{--<div class="content" v-if="show_more">--}}
                        {{--@if(isset($thisCategory->lang)){!! $thisCategory->lang->slug !!}@endif--}}
                    {{--</div>--}}
                {{--</transition>--}}
                {{--<div class="show-more">--}}
                    {{--<button class="button is-rounded is-primary is-outlined" @click="show_more = !show_more">@{{textShow}}</button>--}}
                {{--</div>--}}
            </div>

            <div class="mb5" id="series">
                <h3 class="title is-3">Toate colecțiile de uși</h3>
                <div class="series">

                    <div class="sidenav mobfilter">
                        <div>
                            <el-button type="primary" class="action_button" @click="resetFilter()">Resetare filtre</el-button>
                        </div>
                        <div>
                            <h3 class="sidenav-section-title">serii</h3>
                            <ul class="sidenav-series">
                                @foreach($series as $serie)
                                    <li><a
                                                href="{{url(Session::get('city_id') . '/category/serie/'. $serie->name . '/' . $serie->id)}}"><span>{{$serie->name}}</span></a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="sidenav-pret">
                            <h3 class="sidenav-section-title">Preț de la</h3>

                            <label>min: @{{ price[0] }}Lei, max: @{{ price[1] }}Lei</label>
                            <el-slider
                                    v-model="price"
                                    range
                                    show-stops
                                    :min="min"
                                    :max="max"
                                    @change="GetDors()">
                            </el-slider>
                        </div>

                        <div class="sidenav-culori">
                            <h3 class="sidenav-section-title">Culoare</h3>

                            <div v-if="group_colors.length > 0" v-for="(group, index) in group_colors" :key="index">
                                <span v-if="index == 0">Alte culori</span>
                                <span v-else-if="index == 1">Albe</span>
                                <span v-else-if="index == 2">Structura lemn</span>
                                <ul class="sidenav-color" v-if="group.length > 0">
                                    <li v-for="(color, i) in group" :key="i" :class="(color_id != '' && color_id == color.id) ? 'active' : ''" @click="color_id = color.id; GetDors()">
                                        <img :src="'/images/catalog/'+color.image" :alt="color.name" v-if="color.image != null" >
                                        <span v-else :style="'background-color:' + color.code +';'"></span>
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <div class="sidenav-window">
                            <h3 class="sidenav-section-title">Insertii</h3>

                            <div v-if="group_windows.length > 0">
                                <ul class="sidenav-color">
                                    <li v-for="(wind, u) in group_windows" :key="u" :class="(window_id != '' && window_id == wind.id) ? 'active' : ''" @click="window_id = wind.id; GetDors()">
                                        <img :src="'/images/catalog/'+wind.image" :alt="wind.name">
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <div class="sidenav-stil">
                            <h3 class="sidenav-section-title">Stil</h3>

                            <div v-if="door_styles.length > 0">
                                <el-checkbox-group v-model="styles">
                                    <el-checkbox :label="style.id" v-for="(style, index) in door_styles" :key="index" @change="GetDors()">@{{ style.name }}</el-checkbox>
                                </el-checkbox-group>
                            </div>

                        </div>

                        <div class="sidenav-type-me">
                            <h3 class="sidenav-section-title">Tip</h3>
                            <div class="sidenav-tip" v-if="door_types.length > 0">
                                <el-radio-group v-model="door_type">
                                    <el-radio :label="dtype.id" v-for="(dtype, index) in door_types" :key="index" @change="GetDors()">@{{ dtype.type }}</el-radio>
                                </el-radio-group>
                            </div>

                        </div>

                        <div>
                            <el-button type="primary" class="action_button" @click="resetFilter()">Resetare filtre</el-button>
                        </div>

                    </div>

                    <div class="content">
                        <pagination :data="laravelData" @pagination-change-page="getResults" :limit="2" align="center"></pagination>

                        <transition-group name=”fade” class="columns is-multiline is-mobile" tag="div" style="margin-top: 16px;" v-if="doors.length > 0">
                            <div class="column bigd is-3-desktop is-half-mobile is-half-tablet" v-for="(dor, index) in doors" :key="'index'+index" v-if="getImage(dor) != null">

                                <a :href="{{Session::get('city_id')}}'/product/'+dor.name+'/'+dor.id" class="catalog-img">

                                    <img :src="'/images/catalog/'+getImage(dor)" alt="door" style="width: 160px;">
                                </a>
                                <div class="slide-title has-text-weight-bold">
                                    Seria @{{dor.name}}
                                </div>
                                <div class="door-price">
                                    <span>Preț de la <strong>@{{dor.price}}</strong>Lei</span>
                                </div>
                                <div class="slide-link">
                                    <a :href="{{Session::get('city_id')}}'/product/'+dor.name+'/'+dor.id">Vezi culori</a>
                                </div>
                            </div>
                        </transition-group>

                        <infinite-loading :identifier="infiniteId" @infinite="infiniteHandler"></infinite-loading>
                    </div>
                </div>
                <span class="openfilters-mobile" onclick="openNav()">&#9776; FILTRE</span>
                <div id="mySidenav1" class="sidenav12">

                </div>
                <div id="mySidenav" class="sidenav1">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <div class="sidenav mobile-sidenav-me">
                        <div>
                            <el-button type="primary" class="action_button" @click="resetFilter()">Resetare filtre</el-button>
                        </div>
                        <div>
                            <h3 class="sidenav-section-title">serii</h3>
                            <ul class="sidenav-series">
                                @foreach($series as $serie)
                                    <li><a
                                                href="{{url(Session::get('city_id') . '/category/serie/'. $serie->name . '/' . $serie->id)}}"><span>{{$serie->name}}</span></a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="sidenav-pret">
                            <h3 class="sidenav-section-title">Preț de la</h3>

                            <label>min: @{{ price[0] }}Lei, max: @{{ price[1] }}Lei</label>
                            <el-slider
                                    v-model="price"
                                    range
                                    show-stops
                                    :min="min"
                                    :max="max"
                                    @change="GetDors()">
                            </el-slider>
                        </div>

                        <div class="sidenav-culori">
                            <h3 class="sidenav-section-title">Culoare</h3>

                            <div v-if="group_colors.length > 0" v-for="(group, index) in group_colors" :key="index">
                                <span v-if="index == 0">Alte culori</span>
                                <span v-else-if="index == 1">Albe</span>
                                <span v-else-if="index == 2">Structura lemn</span>
                                <ul class="sidenav-color" v-if="group.length > 0">
                                    <li v-for="(color, i) in group" :key="i" :class="(color_id != '' && color_id == color.id) ? 'active' : ''" @click="color_id = color.id; GetDors()">
                                        <img :src="'/images/catalog/'+color.image" :alt="color.name" v-if="color.image != null" >
                                        <span v-else :style="'background-color:' + color.code +';'"></span>
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <div class="sidenav-window">
                            <h3 class="sidenav-section-title">Insertii</h3>

                            <div v-if="group_windows.length > 0">
                                <ul class="sidenav-color">
                                    <li v-for="(wind, u) in group_windows" :key="u" :class="(window_id != '' && window_id == wind.id) ? 'active' : ''" @click="window_id = wind.id; GetDors()">
                                        <img :src="'/images/catalog/'+wind.image" :alt="wind.name">
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <div class="sidenav-stil">
                            <h3 class="sidenav-section-title">Stil</h3>

                            <div v-if="door_styles.length > 0">
                                <el-checkbox-group v-model="styles">
                                    <el-checkbox :label="style.id" v-for="(style, index) in door_styles" :key="index" @change="GetDors()">@{{ style.name }}</el-checkbox>
                                </el-checkbox-group>
                            </div>

                        </div>

                        <div class="sidenav-type-me">
                            <h3 class="sidenav-section-title">Tip</h3>
                            <div class="sidenav-tip" v-if="door_types.length > 0">
                                <el-radio-group v-model="door_type">
                                    <el-radio :label="dtype.id" v-for="(dtype, index) in door_types" :key="index" @change="GetDors()">@{{ dtype.type }}</el-radio>
                                </el-radio-group>
                            </div>

                        </div>

                        <div>
                            <el-button type="primary" class="action_button" @click="resetFilter()">Resetare filtre</el-button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@section('script')
    <script> window.min_value = @json($min)</script>
    <script> window.max_value = @json($max)</script>
    <script> window.serie_id = @json(1)</script>
    <script> window.groupcolors = @json($groupcolors)</script>
    <script> window.groupwindows = @json($groupwindows)</script>
    <script> window.doorStyles = @json($doorStyle)</script>
    <script> window.doorTypes = @json($doorType)</script>
@endsection
