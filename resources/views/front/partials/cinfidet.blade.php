@extends('front.layouts.new')
@section('content')
    <section id="article" style="margin-top: 50px; margin-bottom: 50px; padding: 20px;">
        {{--<div class="mb4 has-text-centered">--}}
            {{--<span >Politica de confidențialitate</span>--}}
        {{--</div>--}}
        <div class="container">
            <p style="font-size: 22px; font-weight: 600;">POLITICA DE UTILIZARE COOKIE-URI</p>
            <p>CE SUNT COOKIE-URILE?</p>
            <p>Un cookie este un fisier-text de mici dimensiuni pe care un site il salveaza pe calculatorul sau dispozitivul dumneavoastra mobil atunci cand vizitati site-ul. Cookie-urile sunt larg utilizate pentru a face site-urile functionale sau pentru ca acestea sa functioneze mai eficient, precum si pentru a furniza informatii detinatorilor site-ului.</p>
            <p>CUM UTILIZĂM COOKIE-URILE?</p>
            <p>Site-ul Etalon utilizeaza Google Analytics, un serviciu de analiza web furnizat de Google, Inc. (&bdquo;Google&rdquo;) care contribuie la analizarea utilizarii site-ului. &Icirc;n acest scop, Google Analytics utilizeaza &bdquo;cookie-uri&rdquo;, care sunt fisiere-text plasate in calculatorul dumneavoastra.</p>
            <p>Informatiile generate de cookie-uri cu privire la modul in care este utilizat site-ul &ndash; informatii standard de jurnal de utilizare a internetului (inclusiv adresa dumneavoastra IP) si informatii privind comportamentul vizitatorului intr-o forma anonima &ndash; sunt transmise catre Google si stocate de aceasta, inclusiv pe servere din Statele Unite. &Icirc;nainte de a fi transmisa catre Google, adresa dvs. IP este anonimizata.</p>
            <p>&Icirc;n conformitate cu certificarea sa legata de &bdquo;scutul de confidentialitate&rdquo; (Privacy Shield), Google declara ca respecta cadrul UE-SUA privind scutul de confidentialitate. Google poate transfera informatiile colectate de Google Analytics catre o terta parte atunci cand legislatia impune acest lucru sau atunci cand respectiva terta parte prelucreaza informatiile in numele Google.</p>
            <p>&Icirc;n conformitate cu conditiile de utilizare ale Google Analytics, Google nu va asocia adresa IP a utilizatorilor cu niciun fel de alte date detinute de Google.</p>
            <p>Puteti refuza utilizarea cookie-urilor Google Analytics descarcand si instaland Google Analytics Opt-out Browser Add-on.</p>
            <p>Google Analytics Opt-out Browser Add-on</p>
            <p>CUM POT FI CONTROLATE COOKIE-URILE</p>
            <p>Puteti controla si/sau sterge cookie-uri dupa cum doriti. Puteti sterge toate cookie-urile care sunt deja pe calculatorul dumneavoastra si puteti configura majoritatea browserelor sa impiedice plasarea acestora.</p>
            <p><a title="Totul despre cookies" href="http://www.consilium.europa.eu/ro/about-site/cookies/">Totul despre cookies</a></p>
            <p>GESTIONAREA COOKIE-URILOR &Icirc;N BROWSERUL DUMNEAVOASTRĂ</p>
            <p>Majoritatea browserelor va permit:</p>
            <p>sa vedeti ce cookie-uri aveti si sa le stergeti in mod individual</p>
            <p>sa blocati cookie-uri ale unor terte parti</p>
            <p>sa blocati cookie-uri ale anumitor site-uri</p>
            <p>sa blocati setarea tuturor cookie-urilor</p>
            <p>sa stergeti toate cookie-urile atunci cand inchideti browserul</p>
            <p>Daca optati pentru stergerea cookie-urilor, trebuie sa aveti in vedere ca eventualele preferinte se vor pierde. De asemenea, daca blocati cookie-urile complet, numeroase site-uri (inclusiv al nostru) nu vor functiona in mod adecvat, iar transmisiunile pe internet nu vor functiona deloc. Din aceste motive, nu va recomandam sa blocati complet cookie-urile atunci cand utilizati serviciile noastre de transmisiuni pe internet.</p>

        </div>
    </section>

@endsection