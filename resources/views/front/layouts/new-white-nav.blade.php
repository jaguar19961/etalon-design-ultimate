<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('images/icon.png')}}"> <!-- CSRF Token -->
    <meta name="facebook-domain-verification" content="cakyxr1on73uqr2phmwfvjs3lo7m0r" />
    <meta name="csrf-token"
          content="{{ csrf_token() }}"> {{--daca pagina statica--}}
    @if(\Request::route()->getName() == 'blogArticle')
        <title>{{$seo->lang->name}}</title> {{--meta taguri--}}
        <meta name="description" content="{{$seo->lang->descr}}"/>
        <meta name="keywords" content="{{$seo->lang->keywords}}"/>
        <meta name="author" content="etalon.md"/> <!-- Facebook and Twitter integration -->
        <meta property="og:title" content="{{$seo->lang->name}}"/>
        <meta property="og:image" content="{{asset('images/article/'.$seo->image)}}"/>
        <meta property="og:url" content="{{url()->current()}}"/>
        <meta property="og:site_name" content="{{$seo->lang->name}}"/>
        <meta property="og:description" content="{{$seo->lang->descr}}"/>
        <meta name="twitter:title" content="{{$seo->lang->name}}"/>
        <meta name="twitter:image" content="{{asset('images/article/'.$seo->image)}}"/>
        <meta name="twitter:url" content="{{url()->current()}}"/>
        <meta name="twitter:card"
              content="{{url()->current()}}"/>
    @elseif(\Request::route()->getName() == 'oferteArticol')
        <title>{{$seo->lang->name}}</title> {{--meta taguri--}}
        <meta name="description" content="{{$seo->lang->descr}}"/>
        <meta name="keywords" content="{{$seo->lang->keywords}}"/>
        <meta name="author" content="etalon.md"/> <!-- Facebook and Twitter integration -->
        <meta property="og:title" content="{{$seo->lang->name}}"/>
        <meta property="og:image" content="{{asset('images/article/'.$seo->images)}}"/>
        <meta property="og:url" content="{{url()->current()}}"/>
        <meta property="og:site_name" content="{{$seo->lang->name}}"/>
        <meta property="og:description" content="{{$seo->lang->descr}}"/>
        <meta name="twitter:title" content="{{$seo->lang->name}}"/>
        <meta name="twitter:image" content="{{asset('images/article/'.$seo->images)}}"/>
        <meta name="twitter:url" content="{{url()->current()}}"/>
        <meta name="twitter:card" content="{{url()->current()}}"/>
        @elseif(\Request::route()->getName() == 'produs' && !empty($getProduct))
        <title>{{$seo->lang->title}} Usa - {{$getProduct->name}}</title> {{--meta taguri--}}
        <meta name="description" content="{{$getProduct->descr}}"/>
        <meta name="keywords" content="{{$getProduct->keywords}}"/>
        <meta name="author" content="etalon.md"/> <!-- Facebook and Twitter integration -->
        <meta property="og:title" content="{{$seo->lang->title}} Usa - {{$getProduct->name}}"/>
        <meta property="og:image"
              content="@if($getProduct->category_id == 1 || $getProduct->category_id == 2) {{asset('images/catalog/'.$getProduct->colors->first()->image)}} @elseif($getProduct->category_id == 3 || $getProduct->category_id == 4) {{asset('images/catalog/'.$getProduct->image)}} @endif"/>
        <meta property="og:url" content="{{url()->current()}}"/>
        <meta property="og:site_name" content="{{$seo->lang->title}} Usa - {{$getProduct->name}}"/>
        <meta property="og:description" content="{{$getProduct->descr}}"/>
        <meta name="twitter:title" content="{{$seo->lang->title}} Usa - {{$getProduct->name}}"/>
        <meta name="twitter:image"
              content="@if($getProduct->category_id == 1 || $getProduct->category_id == 2) {{asset('images/catalog/'.$getProduct->colors->first()->image)}} @elseif($getProduct->category_id == 3 || $getProduct->category_id == 4) {{asset('images/catalog/'.$getProduct->image)}} @endif"/>
        <meta name="twitter:url" content="{{url()->current()}}"/>
        <meta name="twitter:card" content="{{url()->current()}}"/> @else
        <title>{{$seo->lang->title}}</title> {{--meta taguri--}}
        <meta name="description" content="{{$seo->lang->description}}"/>
        <meta name="keywords" content="{{$seo->lang->keywords}}"/>
        <meta name="author" content="etalon.md"/> <!-- Facebook and Twitter integration -->
        <meta property="og:title" content="{{$seo->lang->title}}"/>
        <meta property="og:image" content="{{asset('images/seo/'.$infos->logo)}}"/>
        <meta property="og:url" content="{{url()->current()}}"/>
        <meta property="og:site_name" content="{{$seo->lang->title}}"/>
        <meta property="og:description" content="{{$seo->lang->description}}"/>
        <meta name="twitter:title" content="{{$seo->lang->title}}"/>
        <meta name="twitter:image" content="{{asset('images/seo/'.$infos->logo)}}"/>
        <meta name="twitter:url" content="{{url()->current()}}"/>
        <meta name="twitter:card" content="{{url()->current()}}"/>
@endif
<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.css" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        #preloader {
            position: fixed;
            left: 0;
            top: 0;
            z-index: 999;
            width: 100%;
            height: 100%;
            overflow: visible;
            background: #fbfbfb url('//cdnjs.cloudflare.com/ajax/libs/file-uploader/3.7.0/processing.gif') no-repeat center center;
        }

        .visible {
            visibility: visible;
            opacity: 1;
            transition: opacity 2s linear;
        }

        .hidden {
            visibility: hidden;
            opacity: 0;
            transition: visibility 0s 2s, opacity 2s linear;
        }
        .el-dropdown .el-dropdown__caret-button::before{
            display: none;
        }
    </style>
</head>
<body>


<div id="app" v-cloak>
{{--    <div id="preloader" class="visible"></div>--}}

    <header>
        <div class="navtop light">
            <div class="container is-flex">
                <div class="navbar-start">
                    <a class="navbar-item" href="tel:{{$infos->phone}}">
                        <i class="fas fa-phone"></i> {{$infos->phone}}
                    </a>
                    <a class="navbar-item" href="https://goo.gl/maps/{{$infos->address}}" target="_blank">
                        <i class="fas fa-map-marker-alt"></i> {{$infos->address}}
                    </a>
                    <a class="navbar-item" href="mailto:{{$infos->email}}">
                        <i class="fas fa-envelope"></i> {{$infos->email}}
                    </a>
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        @if($localeCode == LaravelLocalization::getCurrentLocale())
                        @elseif($url = LaravelLocalization::getLocalizedURL($localeCode))
                            <a class="navbar-item" hreflang="{{$localeCode}}"
                               href="{{$url}}"> {{LaravelLocalization::getCurrentLocale()}}
                                / {{$localeCode}} </a>
                        @endif
                    @endforeach
                </div>
                <div class="navbar-end">
{{--                    <a class="navbar-item share" href="#"> <i class="fab fa-facebook-f"></i> </a>--}}
{{--                    <a class="navbar-item share" href="#"> <i class="fab fa-twitter"></i> </a>--}}
{{--                    <a class="navbar-item share" href="#"> <i class="fab fa-instagram"></i> </a>--}}
                    <a class="navbar-item" href="tel:+40 (752) 660 224">
                        <i class="fas fa-phone"></i> +40 (752) 660 224
                    </a>
                    <a class="navbar-item" href="https://goo.gl/maps/Strada Canta nr. 39 Bl. 508 Iași, Romania" target="_blank">
                        <i class="fas fa-map-marker-alt"></i> Strada Canta nr. 39 Bl. 508 Iași, Romania
                    </a>
                    <a class="navbar-item" href="mailto:iasi@etalon.ro">
                        <i class="fas fa-envelope"></i> iasi@etalon.ro
                    </a>
                </div>
            </div>
        </div>
        <nav class="navbar is-white" id="navbar2">
            <div class="container ">
                <div class="navbar-brand">
                    <a href="{{ url(Session::get('city_id') . '/') }}" class="navbar-item">
                        <img class="logo-me logo-transparent" style="max-height: 2.75rem;" src="{{asset('img/logos/etalon-white.svg')}}" alt="{{ config('app.name', 'Laravel') }}">
                        <img class="logo-me logo-fill-white" style="max-height: 2.75rem;" src="{{asset('img/logos/etalon-black.svg')}}" alt="{{ config('app.name', 'Laravel') }}">
                    </a>
                    <a href="tel:{{$infos->phone}}" class="navbar-item phone-mob"> {{$infos->phone}} </a>
                    <div class="navbar-burger burger" data-target="navMenu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="navbar-menu" id="navMenu">
                    <div class="navbar-start"></div>
                    <div class="navbar-end">
                        <a href="{{ url(Session::get('city_id') . '/') }}" class="logo-mobile">
                            <img src="/img/logo-mob.png" alt="{{ config('app.name', 'Laravel') }}"></a>
                        @foreach($categories->where('activity', 1) as $category)
                            @if($category->dropdown == 1)
                                <el-dropdown split-button class="navbar-item nav-item">
                                    <a href="{{$category->is_url == 1 ? '/' . Session::get('city_id') . $category->link : $category->link}}">{{$category->name}}</a>
                                    <el-dropdown-menu>
                                        @foreach($category->dropdowns as $item)
                                            <el-dropdown-item>
                                                <a href="/{{Session::get('city_id') . $item->link}}">{{$item->name}}</a>
                                            </el-dropdown-item>
                                        @endforeach
                                    </el-dropdown-menu>
                                </el-dropdown>
                                <el-menu default-active="1"
                                         class="el-menu-vertical-demo"
                                         background-color="#2b2b2b"
                                         text-color="#959595"
                                         active-text-color="#ffd04b">
                                    <el-submenu index="1">
                                        <template slot="title">
                                            <span>{{$category->name}}</span>
                                        </template>
                                        @foreach($category->dropdowns as $key => $item)
                                            <el-menu-item index="1-{{$key}}">
                                                <a href="/{{Session::get('city_id') . $item->link}}">{{$item->name}}</a>
                                            </el-menu-item>
                                        @endforeach
                                    </el-submenu>
                                </el-menu> @endif
                            {{--@if($category->dropdown == 1 && $category->dropdowns != null) <div class="dropdown is-hoverable dropdown-mobile-me"> @endif--}} {{--<a style="color: black"--}} {{--class="navbar-item nav-item {{ request()->is(app()->getLocale() .'/category/'. $category->lang->slug . '/' . $category->id) ? 'is-active b-top' : '' }}"--}} {{--href="@if($category->dropdown == 1 && $category->dropdowns != null) # @else {{url('/category/'. $category->lang->slug . '/' . $category->id)}} @endif"--}} {{--@if($category->dropdown == 1 && $category->dropdowns != null)--}} {{--aria-haspopup="true"--}} {{--aria-controls="dropdown-menu4{{$category->id}}"--}} {{--:style="'width: 90%'"--}} {{--@endif--}} {{--v-if="!serch">--}} {{--{{$category->lang->name}}--}} {{--</a>--}} {{--@if($category->dropdown == 1 && $category->dropdowns != null)--}} {{--<div class="dropdown-menu" id="dropdown-menu4{{$category->id}}" role="menu">--}} {{--<div class="dropdown-content">--}} {{--@if($category->dropdowns != null)--}} {{--@foreach($category->dropdowns as $item)--}} {{--<a href="@if($item->category_id == 5) {{url('pereti-despartitori/' . $item->slug. '/'. $item->id)}} @else {{$item->link}} @endif" class="dropdown-item">{{$item->name}}</a>--}} {{--@if($item->divider == 1)--}} {{--<hr class="dropdown-divider">--}} {{--@endif--}} {{--@endforeach--}} {{--@else--}} {{--Nu sunt postari!--}} {{--@endif--}} {{--</div>--}} {{--</div>--}} {{--@endif--}} {{--@if($category->dropdown == 1 && $category->dropdowns != null) </div> @endif--}}
                        @endforeach {{--<div class="dropdown is-hoverable dropdown-mobile-me">--}} {{--<a style="color: black"--}} {{--class="navbar-item nav-item"--}} {{--href=""--}} {{--aria-haspopup="true"--}} {{--aria-controls="dropdown-menu45"--}} {{--:style="'width: 90%'"--}} {{--v-if="!serch">--}} {{--Pereți despărțitori--}} {{--</a>--}} {{--<div class="dropdown-menu" id="dropdown-menu45" role="menu">--}} {{--<div class="dropdown-content">--}} {{--@foreach($toMenu as $menu)--}} {{--<a href="{{url('/blog/article/'.$menu->lang->slug .'/'.$menu->id)}}" class="dropdown-item">{{$menu->lang->name}}</a>--}} {{--@endforeach--}} {{--</div>--}} {{--</div>--}} {{--</div>--}}
                        {{--                        <a style="color: white"--}}
                        {{--                           class="navbar-item nav-item {{ request()->is(app()->getLocale() .'/category/parchet/3') ? 'is-active b-top' : '' }}"--}}
                        {{--                           href="{{url('/category/parchet/3')}}" v-if="!serch">Parchet</a>--}}
                        <a style="color: white"
                           class="navbar-item nav-item {{ request()->is(app()->getLocale() .'sisteme-de-deschidere/afisare/pereti-glisanti/13') ? 'is-active b-top' : '' }}"
                           href="{{url(Session::get('city_id') . '/sisteme-de-deschidere/afisare/pereti-glisanti/13')}}" v-if="!serch">
                            Pereți despărțitori
                        </a>
                        <a style="color: white"
                           class="navbar-item nav-item {{ request()->is(app()->getLocale() .'about') ? 'is-active b-top' : '' }}"
                           href="{{url(Session::get('city_id') . '/about')}}" v-if="!serch">
                            Despre noi
                        </a>
                        <a style="color: white"
                           class="navbar-item nav-item {{ request()->is(app()->getLocale() .'blog') ? 'is-active b-top' : '' }}"
                           href="{{url(Session::get('city_id') . '/blog')}}" v-if="!serch">
                            Blog
                        </a>
                        <a style="color: white"
                           class="navbar-item nav-item {{ request()->is(app()->getLocale() .'offers') ? 'is-active b-top' : '' }}"
                           href="{{url(Session::get('city_id') . '/offers')}}"
                           v-if="!serch">
                            Oferte
                        </a>
                        <a style="color: white"
                           class="navbar-item nav-item {{ request()->is(app()->getLocale() .'contact') ? 'is-active b-top' : '' }}"
                           href="{{url(Session::get('city_id') . '/contact')}}" v-if="!serch">
                            Contact
                        </a>
                        <div class="navbar-center show_mob" style="margin-top: 20px;">
                            @if(Session::get('city_id') == 'bucuresti')
                                <a href="/intro" class="navbar-center-location_name"><i class="fa fa-arrow-left"></i>
                                    București <i class="fa fa-map-marker-alt"></i></a>
                            @endif
                            @if(Session::get('city_id') == 'iasi')
                                <a href="/intro" class="navbar-center-location_name"><i
                                            class="fa fa-map-marker-alt"></i> Iași <i class="fa fa-arrow-right"></i></a>
                            @endif
                        </div>
                        {{--                        <div class="navbar-item mob-show serch-input"><input class="input" type="text"--}}
                        {{--                                                                             placeholder="Căutare..."></div>--}}
                        {{--                        <div class="navbar-item w-100 desc-show" v-if="serch"><input class="input" type="text"--}}
                        {{--                                                                                     placeholder="Căutare..."></div>--}}
                        {{--                        <a class="navbar-item desc-show" href="#" @click.prevent="serch = !serch"><img--}}
                        {{--                                    :src="serch ? '/img/close.png' : '/img/serch.png' " alt="serch dor"></a>--}}
                        <div class="navbar-item desc-show">
                            <a href="tel:{{$infos->phone}}" class="button is-primary btn-primary">SUNĂ ACUM</a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    @yield('content')

    <footer id="footer">
        <section class="primary-footer">
            <div class="container section-content">
                <div class="col">
                    <a href="{{Session::get('city_id')}}">
                        <img src="{{asset('img/logos/etalon-black.svg')}}" alt="logo-mob">
                    </a>
                    <p class="footer-description">Magazin de uși de interior, exterior și pardoseli în București. Uși de
                        tip filomuro, uși glisante și culisante, uși invizibile, uși pliante sau tip carte, uși calitate
                        premium design modern și ultramodern.</p>
                    <a href="#" class="share-btn">
                        <img src="/img/social/1.png" alt="social">
                    </a>
                    <a href="#" class="share-btn">
                        <img src="/img/social/2.png" alt="social">
                    </a>
                    <a href="#" class="share-btn">
                        <img src="/img/social/3.png" alt="social">
                    </a>
                </div>
                <div class="col">
                    <h3 class="footer-title">Link-uri utile</h3>
                    <a href="" class="footer-linck">Uși de interior</a>
                    <a href="" class="footer-linck">Uși de exterior</a>
                    <a href="" class="footer-linck">Parchet</a> {{--<a href="" class="footer-linck">Laminat</a>--}}
                    <a href="" class="footer-linck">Despre noi</a>
                    <a href="" class="footer-linck">Blog</a>
                    <a href="" class="footer-linck">Oferte</a>
                    <a href="" class="footer-linck">Contacte</a>
                </div>
                <div class="col">
                    <h3 class="footer-title">Contacte</h3>
                    <div class="columns">
                        <div class="column">
                            <a href="" class="footer-linck mb-17">
                                <img src="/img/social/4.png" alt="social">
                                <span>{!! $infos->work !!}</span>
                            </a>
                            <a href="" class="footer-linck mb-17">
                                <img src="/img/social/5.png" alt="social"><span>{{$infos->address}}</span>
                            </a>
                            <a href="" class="footer-linck mb-17">
                                <img src="/img/social/6.png" alt="social"><span>{{$infos->phone}}</span>
                            </a>

                            <a href="" class="footer-linck mb-17">
                                <img src="/img/social/5.png" alt="social"><span>Iași, Strada Canta nr. 39 Bl. 508</span>
                            </a>
                            <a href="" class="footer-linck mb-17">
                                <img src="/img/social/6.png" alt="social"><span>+40 (752) 660 224</span>
                            </a>
                            <a href="" class="footer-linck mb-17">
                                <img src="/img/social/7.png" alt="social"><span>{{$infos->email}}</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h3 class="footer-title">Newsletter</h3>
                    {{ Form::open(array('method' => 'put','action' => 'FrontController@getNewsletter')) }}
                    {{ csrf_field() }}
                    <input type="email" class="futer-input" placeholder="E-mail..." name="email">
                    <button type="submit" class="btn-line">Înscrie-te</button>
                    {{Form::close()}}
                </div>
            </div>
        </section>
        <section class="copiright">
            <div class="container section-content">
                <div class="col">
                    <p>Copyright ©2019 Etalon Doors. All rights Reserved. Elaborat de <a href="http://midavco.com">MIDAVCO</a></p>
                </div>
                <div class="col copiright-lincks">
                    <a href="{{Session::get('city_id')}}/termeni&conditii">Termeni și condiții</a>
                    <a href="{{Session::get('city_id')}}/politica-de-confidentialitate">Politica de confidențialitate</a>
                </div>
            </div>
        </section>
        <cookie-law>
            <div slot="message"> {{--Dacă accesați acest web-site, automat acceptați <a href="{{url('/politica-de-confidentialitate')}}">politica de confidențialitate </a>(Cookies).--}}
                We use cookies to ensure you get the best experience on our website.
                <a href="{{url(Session::get('city_id') . '/politica-de-confidentialitate')}}">Learn More...</a>
            </div>
        </cookie-law>
    </footer>

    <div id="to_top" @click="topFunction()"><img src="/img/arrow-top.png" alt="arrow"></div>

    <div id="fixed-butons">
        <div class="buttons has-addons">
            <span class="button is-dark">Suna acum</span>
            <span class="button is-primary" data-target="simpleModal_4" data-toggle="modal">Cere oferta</span>
        </div>
    </div>

    <div id="simpleModal_4" class="modal">
        <div class="modal-window small me-modal">
            <span class="close" data-dismiss="modal" onclick="closeModal1()">×</span>
            <span>Solicită oferta</span>
            {{ Form::open(array('method' => 'put','action' => 'FrontController@getOffer1')) }}
            {{ csrf_field() }}

            <input name="name" type="text" placeholder="Nume..." class="futer-input" required>

            <input name="phone" type="phone" placeholder="Nr. de telefon..." class="futer-input" required>

            <textarea name="mesaj" id="" cols="30" rows="10" placeholder="Mesajul dmv." class="futer-input" required></textarea>

            <button type="submit" class="button is-primary is-rounded me-button-mult">Transmite solicitarea</button>

            {{Form::close()}}
        </div>
    </div>
</div>


@yield('script')
@if(\Request::route()->getName() == 'series')
    <script src="{{ asset('js/series.js') }}"></script>
@elseif(\Request::route()->getName() == 'produs')
{{--    <script src="{{ asset('js/prodjq.js') }}"></script>--}}
    <script src="{{ asset('js/product.js') }}"></script>
@else
    <script src="{{ asset('js/app.js') }}"></script>
@endif

<script type="text/javascript" src="{{asset('js/roomdecor.js')}}"></script>

<script type="application/javascript" src="{{asset('modal/modal.js')}}"></script>
<script>
    // side nav
    function openNav() {
        document.getElementById("mySidenav").style.width = "85%";

    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }

    function closeModal() {
        document.getElementById("simpleModal_3").element.classList.add("modal");
    }

    function closeModal1() {
        document.getElementById("simpleModal_4").element.classList.add("modal");
    }

</script>

{{--facebook chat--}}
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script> window.fbAsyncInit = function () {
        FB.init({xfbml: true, version: 'v4.0'});
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/ro_RO/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script><!-- Your customer chat code -->
<div class="fb-customerchat" attribution=setup_tool page_id="622812921555418" theme_color="#fa3c4c"
     greeting_dialog_display="hide"></div><!--End of facebook chat Script-->
<link href="{{asset('modal/modal.css')}}" rel="stylesheet"/>{{--<!-- Google Tag Manager -->--}}
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-MBHJ5B7');</script><!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MBHJ5B7" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript><!-- End Google Tag Manager (noscript) --></body>
</html>
