<!DOCTYPE html>

<html lang="en" dir="ltr">

<head>

    <meta charset="utf-8">
    <meta name="facebook-domain-verification" content="cakyxr1on73uqr2phmwfvjs3lo7m0r" />
    <title>{{$seo->lang->title}}</title>

{{--meta taguri--}}

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="{{$seo->lang->description}}" />

    <meta name="keywords" content="{{$seo->lang->keywords}}" />

    <meta name="author" content="Natur.md" />



    <!-- Facebook and Twitter integration -->

    <meta property="og:title" content="{{$seo->lang->title}}"/>

    <meta property="og:image" content="{{asset('images/seo/'.$infos->image)}}"/>

    <meta property="og:url" content="{{url()->current()}}"/>

    <meta property="og:site_name" content="{{$seo->lang->title}}"/>

    <meta property="og:description" content="{{$seo->lang->description}}"/>

    <meta name="twitter:title" content="{{$seo->lang->title}}" />

    <meta name="twitter:image" content="{{asset('images/seo/'.$infos->image)}}" />

    <meta name="twitter:url" content="{{url()->current()}}" />

    <meta name="twitter:card" content="{{url()->current()}}" />









    {{--sfirsit metataguri--}}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">

    <link rel="stylesheet" href="{{asset('css/bulma.css')}}">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">



    <link rel="stylesheet" href="{{asset('css/main.css')}}">

    <link rel="stylesheet" href="{{asset('css/header.css')}}">

    <link rel="stylesheet" href="{{asset('css/footer.css')}}">

    <link rel="stylesheet" href="{{asset('css/swiperstyle.css')}}">

    {{--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">--}}

    <script

            src="https://code.jquery.com/jquery-3.4.1.min.js"

            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="

            crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>

    <script src="{{asset('js/main.js')}}"></script>
</head>



<body>



<header>

    <div class="navbar-fix">

        <div class="navtop">

            <div class="container navbar-end">

                <a class="navbar-item" href="tel:{{$infos->phone}}">

                    <i class="fas fa-phone"></i>

                    {{$infos->phone}}

                </a>

                <a class="navbar-item" href="https://goo.gl/maps/{{$infos->address}}" target="_blank">

                    <i class="fas fa-map-marker-alt"></i>

                    {{$infos->address}}

                </a>

                <a class="navbar-item" href="mailto:{{$infos->email}}">

                    <i class="fas fa-envelope"></i>

                    {{$infos->email}}

                </a>

                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)

                    @if($localeCode == LaravelLocalization::getCurrentLocale())

                    @elseif($url = LaravelLocalization::getLocalizedURL($localeCode))



                        <a class="navbar-item" hreflang="{{$localeCode}}" href="{{$url}}">

                            {{LaravelLocalization::getCurrentLocale()}} / {{$localeCode}}

                        </a>



                    @endif

                @endforeach



            </div>

        </div>

        <nav class="navbar">

            <div class="container">

                <div class="navbar-brand">

                    <a class="navbar-item" href="/">

                        {{--<div class="img-logo">--}}



                        {{--</div>--}}

                         <img src="{{asset('images/seo/'.$infos->logo)}}" alt="Natur Doors Logo" style="max-height: 50px">

                    </a>

                    <div class="navbar-burger burger" data-target="navbar">

                        <span></span>

                        <span></span>

                        <span></span>

                    </div>

                </div>



                <div id="navbar" class="navbar-menu">

                    <div class="navbar-start">



                    </div>

                    <div class="navbar-end">

                        @foreach($categories as $category)

                        <a class="navbar-item" href="{{url('/category/'. $category->lang->slug . '/' . $category->id)}}">

                            {{$category->lang->name}}

                        </a>

                        @endforeach

                        <a class="navbar-item" href="{{url('/about')}}">

                            Despre noi

                        </a>

                        <a class="navbar-item" href="{{url('/blog')}}">

                            Blog

                        </a>

                        <a class="navbar-item" href="{{url('/offers')}}">

                            Oferte

                        </a>

                        <a class="navbar-item" href="{{url('/contact')}}">

                            Contact

                        </a>

                    </div>

                </div>

            </div>

        </nav>

    </div>









@yield('front')
    <style>

        @media screen and (min-width: 990px) {
            .sunaAcumBtn, .btnCerereOferta.mobil {
                display: none;
            }
        }
        @media only screen and (max-width: 600px) {
            .sunaAcumBtn{
                display: block;
            }
            .footer-action {
                position: fixed;
                bottom: 8px;
                z-index: 99999;
            }

            .btn1{
                background-color: #ffe729;
                padding: 10px 10px 10px 10px;
                color: black;
            }

            .btn2{
                background-color: #ffe729;
                padding: 10px 10px 10px 10px;
            }

            .sunaAcumBtn a {
                position: fixed;
                bottom: 0px;
                width: 50%;
                background: #958063;
                text-align: center;
                font-size: 20px;
                padding: 20px 20px;
                color: white;
                text-transform: uppercase;
                font-weight: 700;
                z-index: 999;
            }

            .btnCerereOferta.mobil {
                display: block;
                width: 50%;
                position: fixed;
                bottom: 0px;
                right: 0px;
                text-align: center;
                margin: 0px;
                font-size: 20px;
                padding: 20px 20px;
                border-radius: 0px;
                color: white;
                background: black;
                z-index: 999;
                text-transform: uppercase;
            }
        }
    </style>
    <div class="sunaAcumBtn">
        <a id="btn-suna-acum" href="tel:{{$infos->phone}}">Sună acum</a>
        <span class="btnCerereOferta mobil">Cere ofertă</span>
    </div>


<footer>

    <div class="container">

        <div class="columns">

            <div class="column footer-plugin">

                <iframe src="https://www.facebook.com/plugins/page.php?href={{$infos->fb}}" width="500" height="130" style="border:none;overflow:hidden;height: 130px;" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

            </div>

            <div class="column footer-plugin">

                <iframe src="{{$infos->go}}" width="500" height="130" frameborder="0" style="border:0; height:130px;" allowfullscreen></iframe>

            </div>

        </div>

        <div class="">

            {{$infos->address}}

        </div>

        <div class="">

            {{$infos->email}}

        </div>

        <div class="">

            {{$infos->phone}}

        </div>

        <hr>

        <p>Copyright © 2019 ETALON. All rights reserved.<span>Developed by <a href="http://midavco.com">midavco.com</a></span></p>

    </div>

</footer>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-K3389XV');</script>
    <!-- End Google Tag Manager -->


    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K3389XV"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->




        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5d07892b36eab9721117d251/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

</body>



<script>

    var swiper1 = new Swiper('.swiper-container.s1', {

        loop: true,

        autoplay: {
            delay: 4000,
            disableOnInteraction: false,
            waitForTransition: false,
        },

        pagination: {

            el: '.swiper-pagination.s1',

        },

        navigation: {

            nextEl: '.swiper-button-next.s1',

            prevEl: '.swiper-button-prev.s1',

        },

    });



    var swiper2 = new Swiper('.swiper-container.s2', {

        loop: true,

        autoplay: {
            delay: 4000,
            disableOnInteraction: false,
            waitForTransition: false,
        },

        slidesPerView: 6,

        spaceBetween: 20,

        pagination: {

            el: '.swiper-pagination.s2',

            clickable: true,

        },

        navigation: {

            nextEl: '.swiper-button-next.s2',

            prevEl: '.swiper-button-prev.s2',

        },

        breakpoints: {

            1024: {

                slidesPerView: 4,

                spaceBetween: 40,

            },

            768: {

                slidesPerView: 3,

                spaceBetween: 30,

            },

            640: {

                slidesPerView: 2,

                spaceBetween: 20,

            },

            320: {

                slidesPerView: 1,

                spaceBetween: 10,

            }

        }

    });

    var swiper3 = new Swiper('.swiper-container.s3', {

        loop: true,

        autoplay: {
            delay: 4000,
            disableOnInteraction: false,
            waitForTransition: false,
        },

        slidesPerView: 6,

        spaceBetween: 20,

        pagination: {

            el: '.swiper-pagination.s3',

            clickable: true,

        },

        navigation: {

            nextEl: '.swiper-button-next.s3',

            prevEl: '.swiper-button-prev.s3',

        },

        breakpoints: {

            1024: {

                slidesPerView: 4,

                spaceBetween: 40,

            },

            768: {

                slidesPerView: 3,

                spaceBetween: 30,

            },

            640: {

                slidesPerView: 2,

                spaceBetween: 20,

            },

            320: {

                slidesPerView: 1,

                spaceBetween: 10,

            }

        }

    });

    var swiper4 = new Swiper('.swiper-container.s4', {

        loop: true,

        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
            waitForTransition: false,
        },


        slidesPerView: 5,

        spaceBetween: 20,

        pagination: {

            el: '.swiper-pagination.s4',

            clickable: true,

        },

        navigation: {

            nextEl: '.swiper-button-next.s4',

            prevEl: '.swiper-button-prev.s4',

        },

        breakpoints: {

            1024: {

                slidesPerView: 4,

                spaceBetween: 40,

            },

            768: {

                slidesPerView: 3,

                spaceBetween: 30,

            },

            640: {

                slidesPerView: 2,

                spaceBetween: 20,

            },

            320: {

                slidesPerView: 1,

                spaceBetween: 10,

            }

        }

    });
    $(".swiper-container").mouseenter(function () {
        swiper.autoplay.pause();
    });
    $(".swiper-container").mouseleave(function () {
        swiper.autoplay.start();
    });


</script>

</html>

